<?php

use Illuminate\Support\Facades\Route;
use Spatie\GoogleCalendar\Event;
use App\Http\Controllers\TripController;
use App\Http\Controllers\AdduserController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\PassengerController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\AdddriverController;
use App\Http\Controllers\AddvehicleController;
// use App\Http\Controllers\ViewvehicleController;
// use App\Http\Controllers\EditvehicleController;
// use App\Http\Controllers\AddfareController;
use App\Http\Controllers\FareController;
// use App\Http\Controllers\EditdriverController;
use App\Http\Controllers\PaymenteditController;
// use App\Http\Controllers\EditpassengerController;
// use App\Http\Controllers\EditfareController;
use App\Http\Controllers\BookedtripController;
use App\Http\Controllers\RevenueController;
use App\Http\Controllers\AccountController;
// use App\Http\Controllers\ViewaccountController;
use App\Http\Controllers\PaypalController;










/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Artisan commands
Route::get('clear-cache', function () {

    \Artisan::call('cache:clear');

    dd("Cache is cleared");

});

Route::get('view-clear', function () {

    \Artisan::call('view:clear');

    dd("View is cleared");

});

Route::get('config-clear', function () {

    \Artisan::call('config:clear');

    dd("Config is cleared");

});

Route::get('config-cache', function () {

    \Artisan::call('config:cache');

    dd("Config cache is cleared");

});

Route::get('route-clear', function () {

    \Artisan::call('route:clear');

    dd("Route is cleared");

});

Route::get('route-cache', function () {

    \Artisan::call('route:cache');

    dd("Route is cached");

});

Route::get('optimize', function () {

    \Artisan::call('optimize');

    dd("Cache optimized");

});

Route::get('migrate', function () {

    \Artisan::call('migrate');

    dd("Database migrated");

});

Route::get('db-seed', function () {

    \Artisan::call('db:seed');

    dd("Database seeded");

});

Route::get('/test', function () {
    $event = new Event;
    $event->name = 'Todays Meeting for Example';
    $event->startDateTime = Carbon\Carbon::now();
    $event->endDateTime = Carbon\Carbon::now()->addHour();
    $event->save();
    $e = Event::get();

    dd($e);
});
Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/contact', function () {
    return view('contact');
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile');
Route::get('/addmeeting', [App\Http\Controllers\AddmeetingController::class, 'index'])->name('addmeeting');
Route::get('/allmeeting/{category?}/{startingdate?}/{endingdate?}/{user_id?}', [App\Http\Controllers\AllMeetingController::class, 'index'])->name('allmeeting');
Route::get('/editmeeting/{id}', [App\Http\Controllers\AllMeetingController::class, 'show'])->name('editmeeting/{id}');
Route::get('/meetingNotification', [App\Http\Controllers\MeetingNotificationController::class, 'index'])->name('meetingNotification');
Route::post('/add', [App\Http\Controllers\AdduserController::class, 'create'])->name('add');
Route::post('/', [App\Http\Controllers\AddmeetingController::class, 'create'])->name('/');
Route::post('/update', [App\Http\Controllers\AllMeetingController::class, 'edit'])->name('update');
Route::delete('/delmeeting/{id}', [App\Http\Controllers\AllMeetingController::class, 'destroy'])->name('Meeting.destroy');
Route::post('/submit', [App\Http\Controllers\ContactController::class, 'create'])->name('submit');
Route::get('/event', [App\Http\Controllers\AddEventController::class, 'index'])->name('event');
Route::get('/calendar', [App\Http\Controllers\CalendarController::class, 'index'])->name('calendar');
Route::get('/contacts', [App\Http\Controllers\ContactController::class, 'index'])->name('contacts');
//ALL PASSENGER ROUTE
Route::get('/passenger', [App\Http\Controllers\PassengerController::class, 'index'])->name('passenger');
// Route::get('/analyze', [App\Http\Controllers\AnalyzeController::class, 'index'])->name('analyze');
// PAYMENT ROUTE
Route::get('/payment', [App\Http\Controllers\PaymentController::class, 'index'])->name('payment');
//ADD DRIVER ROUTE
Route::group(['prefix' => 'adddriver', 'as' => 'adddriver.'], function () {
      Route::get('/create', [AdddriverController::class, 'index'])->name('create');
      Route::get('/alldriver', [AdddriverController::class, 'view'])->name('alldriver');
      Route::post('/store', [AdddriverController::class, 'store'])->name('store');
      //EDIT DRIVER ROUTE
      Route::get('/editdriver/{id}', [AdddriverController::class, 'edit'])->name('editdriver');
      Route::post('update-data', [AdddriverController::class, 'update'])->name('update-data');
      Route::get('delete-data/{id}', [AdddriverController::class, 'delete'])->name('delete-data');
});
//ADD VEHICLE ROUTE
Route::group(['prefix' => 'addvehicle', 'as' => 'addvehicle.'], function () {
      Route::get('/create', [AddvehicleController::class, 'index'])->name('create');
      Route::get('/viewvehicle', [AddvehicleController::class, 'view'])->name('viewvehicle');
      Route::post('/store', [AddvehicleController::class, 'store'])->name('store');
      Route::get('/editvehicle/{id}', [AddvehicleController::class, 'edit'])->name('editvehicle');
      Route::post('update-data', [AddvehicleController::class, 'update'])->name('update-data');
      Route::get('delete-data/{id}', [AddvehicleController::class, 'delete'])->name('delete-data');



});
// VIEW VEHICLE ROUTE




//ADD FARE ROUTE
Route::group(['prefix' => 'fare', 'as' => 'fare.'], function () {
    Route::get('/create', [FareController::class, 'index'])->name('create');
    Route::get('/all', [FareController::class, 'view'])->name('all');
    Route::post('/store', [FareController::class, 'store'])->name('store');
    Route::get('edit/{id}', [FareController::class, 'edit'])->name('edit');
    Route::post('update-data', [FareController::class, 'update'])->name('update-data');
    Route::get('delete-data/{id}', [FareController::class, 'delete'])->name('delete-data');

});



//PAYMENT EDIT ROUTE
Route::get('/paymentedit', [App\Http\Controllers\PaymenteditController::class, 'index'])->name('paymentedit');
// EDIT PASSENGER ROUTE
Route::group(['prefix' => 'passenger', 'as' => 'passenger.'], function () {
    Route::get('/passenger', [PassengerController::class, 'index'])->name('passenger');
    Route::get('delete-data/{id}', [PassengerController::class, 'delete'])->name('delete-data');

    // Route::get('/editpassenger', [PassengerController::class, 'edit'])->name('editpassenger');
    // Route::post('update-data', [PassengerController::class, 'update'])->name('update-data');
});

// BOOKED TRIPS
Route::get('/bookedtrip', [App\Http\Controllers\BookedtripController::class, 'index'])->name('bookedtrip');
//REVENUE
Route::get('/revenue', [App\Http\Controllers\RevenueController::class, 'index'])->name('revenue');
// ADD DRIVER ACCOUNT DETAILS
Route::group(['prefix' => 'account', 'as' => 'account.'], function () {
    Route::get('/create', [AccountController::class, 'index'])->name('create');
    Route::get('/viewaccount', [AccountController::class, 'view'])->name('viewaccount');
    Route::post('/store', [AccountController::class, 'store'])->name('store');
    Route::get('accountedit/{id}', [AccountController::class, 'edit'])->name('accountedit');
    Route::post('update-data', [AccountController::class, 'update'])->name('update-data');
    Route::get('delete-data/{id}', [AccountController::class, 'delete'])->name('delete-data');
    });
//VIEW DRIVERS ACCOUNT
// Route::get('/viewaccount', [App\Http\Controllers\ViewaccountController::class, 'index'])->name('viewaccount');
Route::get('/paypal', [App\Http\Controllers\PaypalController::class, 'index'])->name('paypal');



Route::get('/report/{category?}/{startingdate?}/{endingdate?}/{user_id?}', [App\Http\Controllers\ReportController::class, 'index'])->name('report');
Route::get('/report-status/{meeting_id}/{meeting_status}', [App\Http\Controllers\ReportController::class, 'meetingStatus']);

//Role
Route::group(['prefix' => 'trip', 'as' => 'role.'], function () {
    Route::get('/create', [TripController::class, 'create'])->name('create');
    Route::post('/store', [TripController::class, 'store'])->name('store');
    Route::get('/all', [TripController::class, 'all'])->name('all');
    Route::post('/update', [TripController::class, 'update'])->name('update');
});
//User
Route::group(['prefix' => 'driver', 'as' => 'user.'], function () {
    Route::get('/addUser', [DriverController::class, 'index'])->name('addUser');
    Route::get('/alluser/{startingdate?}/{endingdate?}/{user_id?}', [DriverController::class, 'index'])->name('alluser');
    Route::get('/edit-user/{id}', [DriverController::class, 'editUser'])->name('edit-user');
    Route::post('/update-user', [DriverController::class, 'updateUser'])->name('update-user');
    Route::delete('/del/{id}', [DriverController::class, 'destroy'])->name('users.destroy');
});
//End User
//Meeting
Route::group(['prefix' => 'meeting', 'as' => 'meeting.'], function () {
});
//End Meeting
//Meeting Reporting
Route::group(['prefix' => 'meeting-report', 'as' => 'meeting-report.'], function () {
});






//ADD FARE ROUTE
Route::group(['prefix' => 'fare', 'as' => 'fare.'], function () {
    Route::get('/create', [FareController::class, 'index'])->name('create');
    Route::get('/viewfare', [FareController::class, 'view'])->name('viewfare');
    Route::post('/store', [FareController::class, 'store'])->name('store');
});
//End Meeting Reporting
