$(function () {

    $("#get-selected").click(function () {
        var open = [];
        var userId = [];
        $.each($("input:checkbox[type='checkbox']:checked"), function () {
            open.push($(this).val());
            userId.push($(this).data('userId'));
        });
        $('#participants').val(open.toString());
        $('#participants_id').val(userId.toString());
    })
    $(".hh").blur(function () {
        if ($(this).val() >= 24)
            $(this).val($(this).val() % 24);

        if ($(this).val() == "")
            $(this).val("");
        else
            if ($(this).val() < 10)
                $(this).val("0" + parseInt($(this).val()));
        validateTime(x);
    });
    $(".mm").blur(function () {
        if ($(this).val() >= 60)
            $(this).val($(this).val() % 60);

        if ($(this).val() == "")
            $(this).val("");
        else
            if ($(this).val() < 10)
                $(this).val("0" + parseInt($(this).val()));

        var x = $(this).parent().attr("class").split(" ")[1];
        validateTime(x);
    });

    $(".hh").on("input", function () {
        $(this).parent().removeClass("invalid").removeClass("valid");
        if ($(this).val().length == 2)
            $(this).siblings(".mm").focus().select();
    });
    $(".mm").on("input", function () {
        $(this).parent().removeClass("invalid").removeClass("valid");
        if ($(this).val().length == 2)
            $(this).blur();
    });
    $(".hh").on("focus", function () {
        $(this).parent().removeClass("invalid").removeClass("valid");
    });
    $(".mm").on("focus", function () {
        $(this).parent().removeClass("invalid").removeClass("valid");
    });



    function validateTime(x) {
        var t = $(".timepicker." + x).find(".hh").val() + ":" + $(".timepicker." + x).find(".mm").val();
        var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(t);
        if (isValid) {
            $(".timepicker." + x).removeClass("invalid").addClass("valid");
        } else {
            $(".timepicker." + x).removeClass("valid").addClass("invalid");
        }

    }
    $("html").on('input', ".N", function () {
        $(this).val($(this).val().replace(/[^0-9.]/g, ""));
    });
});