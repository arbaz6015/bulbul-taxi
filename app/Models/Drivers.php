<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Drivers extends Model
{
    use HasFactory;
    protected $table = "drivers";
    protected $fillable = [
        	'id', 	'photo',			'name', 'street_address	','city', 'country', 'zip_code','gender', 'driver_government_id' , 'email', 'driver_license_no', 'phone_number',  'date_of_birth', 'deleted_at',	'created_at',	'updated_at',
    ];
}
