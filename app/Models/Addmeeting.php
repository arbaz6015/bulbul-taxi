<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Addmeeting extends Model
{
    use HasFactory;
    protected $table = "addmeeting";
    protected $fillable = [
        'meeting_name',
        'description',
        'participant',
        'meeting_id',
        'host_name',
        'meeting_date',
        'meeting_time',
        'category',
        'duration',
        'meeting_place',
        'host_id',
        'created_at',
        'updated',
        'status',
    ];
    // public function meeting()
    // {
    //     return $this->hasMany(UserMeeting::class, 'user_id', 'meeting_id');
    // }

    public function users()
    {
        return $this->belongsTo(User::class, 'host_id', 'id');
    }
    public function meeting()
    {
        return $this->belongsToMany(User::class, 'usermeetings', 'meeting_id', 'user_id');
    }
}
