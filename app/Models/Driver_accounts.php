<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver_accounts extends Model
{
    use HasFactory;
    protected $table = "driver_accounts";
    protected $fillable = [
        	'id', 'driver_id',	'bank_name',	'account_number	',	'routing_number',	'deleted_at',	'created_at',	'updated_at',

    ];
}
