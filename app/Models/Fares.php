<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fares extends Model
{
    use HasFactory;
    protected $table = "fares";
    protected $fillable = [
        	'id', 'vehicle_type',	'fare_per_km',	'fare_per_minute',	'minimun_fare',	'minimun_distance',	'waiting_fare',	'deleted_at',	'created_at',	'updated_at',

    ];
}
