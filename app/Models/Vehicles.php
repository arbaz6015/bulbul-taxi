<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicles extends Model
{
    use HasFactory;
    protected $table = "vehicles";
    protected $fillable = [
        	'id', 'name',	'driver_id	',	'vehicle_license_plate_no',	'engine_number',	'chasis_number', 'rc_photo	','puc_photo', 'insurance_photo', 'vehicle_photos','vehicle_type',   'vehicle_model', 'seating_capacity	', 'tax_submission_date',  'insurance_renewal_date', 'deleted_at',	'created_at',	'updated_at',
    ];
}
