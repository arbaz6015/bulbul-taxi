<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Passengers extends Model
{
    use HasFactory;
    protected $table = "passengers";
    protected $fillable = [
        	'id', 'passenger_id',	'name',	'email',	'number',	'wallet_balance',	'trip', 'status',	'deleted_at',	'created_at',	'updated_at',

    ];
}
