<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

use App\Models\Addmeeting;
use App\Models\UserMeeting as UM;
use Spatie\GoogleCalendar\Event;
use Illuminate\Support\Facades\Auth;


class AddmeetingController extends Controller
{
    protected function create(Request $req)
    {
        $req->validate(
            [
                'meeting_name' => 'required',
                'meeting_id' => 'required|unique:addmeeting',
                'participants' => 'required',
                'category' => 'required',
                'meeting_date' => 'required',
                'meeting_time' => 'required'
            ],
            [
                'meeting_name.required' => 'Meeting name is required',
                'meeting_date.required' => 'Meeting date is required',
                'meeting_time.required' => 'Metting time is required'
            ]
        );
        if ($req->hours == 0 && $req->minute == 0) {
            toastr()->warning('Duration is required');
            return redirect()->back();
        }

        $duration = $req->hours . ":" . $req->minute;
        $userId = array_filter(explode(',', $req->participants));
        try {
            $meeting = Addmeeting::create([
                'meeting_name' => $req->meeting_name,
                'description' => isset($req->description) ? $req->description : NULL,
                'participant' => json_encode($userId, JSON_FORCE_OBJECT),
                'meeting_id' => $req->meeting_id,
                'host_name' => $req->host_name,
                'meeting_date' => $req->meeting_date,
                'meeting_time' => $req->meeting_time,
                'category' => $req->category,
                'duration' => $duration,
                'host_id' => isset(Auth::user()->id) ? Auth::user()->id : 0,
                'meeting_place' => isset($req->meeting_place) ? $req->meeting_place : NULL,
            ]);
            $meetingId = $meeting->id;
            if (!empty($userId)) {
                foreach ($userId as $user) {
                    $meetingUser = UM::create([
                        'user_id' => $user,
                        'meeting_id' => $meetingId,
                    ]);
                    if (empty($meetingUser)) {
                        toastr()->error('Oops! Something went wrong.');
                        return redirect()->back();
                    }
                }
            }
            if (!empty($meeting)) {
                toastr()->success('Add Meeting Successfully');
                return redirect()->back();
            } else {
                toastr()->error('Oops! Something went wrong.');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            toastr()->error($e->getMessage());
        }
        return redirect()->back();
    }
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $collectionOfRole = array("Admin");
        $currentRole = Auth::user()->getRoleNames()[0];
        if (!in_array($currentRole, $collectionOfRole)) {
            array_push($collectionOfRole, $currentRole);
        }
        
        $users = User::whereHas("roles", function ($q) use ($collectionOfRole) {
            $q->whereNotIn('name', $collectionOfRole);
        })->get();
        return view('AfterLogin.ScheduleMeeting.addmeeting', compact('users'));
    }
}
