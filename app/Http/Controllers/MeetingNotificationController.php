<?php

namespace App\Http\Controllers;

use App\Models\Addmeeting;
use App\Models\User;
use Illuminate\Http\Request;

class MeetingNotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $req)
    {
        $addmeeting = Addmeeting::get();
        $currentDate = date('Y-m-d');
        $expiry = Addmeeting::where('meeting_date', '<', $currentDate)->get();
        $up_date = Addmeeting::where('meeting_date', '>', $currentDate)->get();
        $today = Addmeeting::where('meeting_date', '=', $currentDate)->get();
        return view('AfterLogin.meetingNotification', compact('expiry', 'up_date', 'today'));
    }
}
