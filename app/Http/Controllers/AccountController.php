<?php

namespace App\Http\Controllers;

use App\Models\Driver_accounts;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('AfterLogin.create');
    }
    public function Pweb(Request $request)
    {

        $driver_account = new Driver_accounts;
        $driver_account->driver_id = $request->driver_id;
        $driver_account->bank_name = $request->bank_name;
        $driver_account->account_number = $request->account_number;
        $driver_account->routing_number = $request->routing_number;
        $driver_account->save();

    }
    public function store(Request $request)
    {
        $driver_accounts = new Driver_accounts;
        $driver_accounts->driver_id = $request->driver_id;
        $driver_accounts->bank_name = $request->bank_name;
        $driver_accounts->account_number = $request->account_number;
        $driver_accounts->routing_number = $request->routing_number;
        $driver_accounts->save();
        return redirect()->route('account.viewaccount');

    }
    public function view()
    {
        $driver_accounts = Driver_accounts::all();
        // echo "<pre>";
        // print_r($fares->toArray());
        // echo "</pre>";
        // die;
        // $data = compact('driver_accounts');
        return view('AfterLogin.viewaccount', compact('driver_accounts'));
    }
    
    public function edit($id)
    {
        $driver_accounts = Driver_accounts::find($id);
        return view('AfterLogin.accontedit', compact('driver_accounts'));
    }
    public function update(Request $request)
    {
        $driver_accounts = Driver_accounts::find($request->edit);
        $driver_accounts->bank_name = $request->input('bank_name');
        $driver_accounts->account_number = $request->input('account_number');
        $driver_accounts->routing_number = $request->input('routing_number');
        $driver_accounts->update();
        return redirect()->route('account.viewaccount')->with('all', 'Account Updated Successfully');

    }
    public function delete($id)
    {
        $driver_accounts = Driver_accounts::find($id);
        $driver_accounts->delete();
        return redirect()->route('account.viewaccount')->with('all','Account Delete Successfully');
       
    }

}
