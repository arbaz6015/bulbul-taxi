<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;

class TripController extends Controller
{
    public function index()
    {
        $roles = Role::get();
        return view('auth.register');
    }
    public function create()
    {
        $roles = Role::where("name", "!=", "Admin")->get();
        return view('AfterLogin.roles.create-role', compact('roles'));
    }
    public function all()
    {
        $roles = Role::where("name", "!=", "Admin")->get();
        return view('AfterLogin.roles.index', compact('roles'));
    }
    public function store(Request $req)
    {
        $req->validate(
            [
                'name' => 'required|unique:roles',
            ]
        );
        $role_id = (isset($req->role_name) && !empty($req->role_name)) ? (int)($req->role_name) : NULL;
        try {
            $role = Role::create([
                'name' => $req->name,
                'role_id' => $role_id,
                'guard_name'=> 'web'

            ]);
            if (!empty($role)) {
                toastr()->success('Added role Successfully');
                return redirect()->back();
            } else {
                toastr()->error('Oops! Something went wrong.');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
