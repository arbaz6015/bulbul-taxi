<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicles;
use Image;
use Illuminate\Support\Facades\Storage;


class AddvehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('AfterLogin.addvehicle');
    }
    public function Pweb(Request $request)
    {
        
        $vehicles = new Vehicles;
        // $drivers->users_id = $request->users_id;
        $vehicles->driver_id = $request->driver_id;
        $vehicles->vehicle_license_plate_no = $request->vehicle_license_plate_no;
        $vehicles->engine_number = $request->engine_number	; 
        $vehicles->chasis_number = $request->chasis_number	; 
        $vehicles->rc_photo = $request->rc_photo	;        
        $vehicles->puc_photo = $request->puc_photo		;        
        $vehicles->insurance_photo = $request->insurance_photo	;        
        $vehicles->vehicle_photos = $request->vehicle_photos	;        
        $vehicles->vehicle_type = $request->vehicle_type	;        
        // $vehicles->drivehicle_manufacturerver_government_id = $request->vehicle_manufacturer	;        
        $vehicles->vehicle_model = $request->vehicle_model	;        
        $vehicles->seating_capacity = $request->seating_capacity	;        
        $vehicles->tax_submission_date = $request->tax_submission_date	;        
        $vehicles->insurance_renewal_date = $request->insurance_renewal_date	;              
        $vehicles->save();

       


    }
    public function store(Request $request){
        $vehicles = new Vehicles;
        // vehicle photo 
        if ($request->hasFile('vehicle_photos')) {
            $vehicle_image      = $request->file('vehicle_photos');
            $fileName = time() . '.' . $vehicle_image->getClientOriginalExtension();
            
            $img = Image::make($vehicle_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $vehicles->vehicle_photos = $fileName;       
        }
        // rc photo 
        if ($request->hasFile('rc_photo')) {
            $vehicle_image      = $request->file('rc_photo');
            $fileName = time() . '.' . $vehicle_image->getClientOriginalExtension();
            
            $img = Image::make($vehicle_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $vehicles->rc_photo = $fileName;       
        }
        // puc photo 
        if ($request->hasFile('puc_photo')) {
            $vehicle_image      = $request->file('puc_photo');
            $fileName = time() . '.' . $vehicle_image->getClientOriginalExtension();
            
            $img = Image::make($vehicle_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $vehicles->puc_photo = $fileName;       
        }
        //Insurance photo
        if ($request->hasFile('insurance_photo')) {
            $vehicle_image      = $request->file('insurance_photo');
            $fileName = time() . '.' . $vehicle_image->getClientOriginalExtension();
            
            $img = Image::make($vehicle_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $vehicles->insurance_photo = $fileName;       
        }
        $vehicles->driver_id = $request->driver_id;
        $vehicles->vehicle_license_plate_no = $request->vehicle_license_plate_no;
        $vehicles->engine_number = $request->engine_number	; 
        $vehicles->chasis_number = $request->chasis_number	; 
        // $vehicles->rc_photo = $request->rc_photo	;        
        // $vehicles->puc_photo	 = $request->puc_photo		;        
        // $vehicles->insurance_photo = $request->insurance_photo	;        
        $vehicles->name = $request->name	;        
        $vehicles->vehicle_type = $request->vehicle_type	;        
        // $vehicles->drivehicle_manufacturerver_government_id = $request->vehicle_manufacturer	;        
        $vehicles->vehicle_model = $request->vehicle_model	;        
        $vehicles->seating_capacity = $request->seating_capacity	;        
        $vehicles->tax_submission_date = $request->tax_submission_date	;        
        $vehicles->insurance_renewal_date = $request->insurance_renewal_date	;              
        $vehicles->save();
        return redirect()->route('addvehicle.viewvehicle')->with('success', 'Driver created successfully');
    }

  
    public function view()
    {
        $vehicles = Vehicles ::all();
        // echo "<pre>";
        // print_r($vehicles->toArray());
        // echo "</pre>";
        // die;
       
        return view('AfterLogin.viewvehicle', compact('vehicles'));
    }
    public function edit($id)
    {
        $vehicles = Vehicles::find($id);
        return view('AfterLogin.editvehicle', compact('vehicles'));
    }
    public function update(Request $request)
    {
        $vehicles = Vehicles::find($request->edit);
        $vehicles->name = $request->input('name');
        $vehicles->vehicle_license_plate_no = $request->input('vehicle_license_plate_no');
        $vehicles->engine_number = $request->input('engine_number');
        $vehicles->chasis_number = $request->input('chasis_number');
        $vehicles->vehicle_type = $request->input('vehicle_type');
        $vehicles->vehicle_model = $request->input('vehicle_model');
        $vehicles->seating_capacity = $request->input('seating_capacity');
        $vehicles->tax_submission_date = $request->input('tax_submission_date');
        $vehicles->insurance_renewal_date = $request->input('insurance_renewal_date');
        // $vehicles->vehicle_photos = $request->input('vehicle_photos');
         // vehicle photo 
         if ($request->hasFile('vehicle_photos')) {
            $vehicle_image      = $request->file('vehicle_photos');
            $fileName = time() . '.' . $vehicle_image->getClientOriginalExtension();
            
            $img = Image::make($vehicle_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $vehicles->vehicle_photos = $fileName;       
        }
        // rc photo 
        if ($request->hasFile('rc_photo')) {
            $vehicle_image      = $request->file('rc_photo');
            $fileName = time() . '.' . $vehicle_image->getClientOriginalExtension();
            
            $img = Image::make($vehicle_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $vehicles->rc_photo = $fileName;       
        }
        // puc photo 
        if ($request->hasFile('puc_photo')) {
            $vehicle_image      = $request->file('puc_photo');
            $fileName = time() . '.' . $vehicle_image->getClientOriginalExtension();
            
            $img = Image::make($vehicle_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $vehicles->puc_photo = $fileName;       
        }
        //Insurance photo
        if ($request->hasFile('insurance_photo')) {
            $vehicle_image      = $request->file('insurance_photo');
            $fileName = time() . '.' . $vehicle_image->getClientOriginalExtension();
            
            $img = Image::make($vehicle_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $vehicles->insurance_photo = $fileName;       
        }
        $vehicles->update();
        return redirect()->route('addvehicle.viewvehicle')->with('all','Vehicle Updated Successfully');
       
    }
    public function delete($id)
    {
        $vehicles = Vehicles::find($id);
        $vehicles->delete();
        return redirect()->route('addvehicle.viewvehicle')->with('all','Vehicle Delete Successfully');
       
    }
}
