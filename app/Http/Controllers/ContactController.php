<?php

namespace App\Http\Controllers;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    protected function create(Request $req)
    {
        Contact::create([
            'name' => $req->name,
            'email' => $req->email,
            'subject' => $req->subject,
            'message' => $req->message,
        ]);
        return redirect()->back();
    }
    
    public function index()
    { 
        $contact = Contact::get();
        return view('AfterLogin.ScheduleMeeting.contactspage',compact('contact'));
    }
    
}
