<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Addmeeting;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;


class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($category = null, $startingdate = null, $endingdate = null, $user_id = null)
    {
        $startingDate = date('Y-m-d', strtotime($startingdate));
        $endingDate = date('Y-m-d', strtotime($endingdate));
        $category = $category;
        $meeting = 0;
        $addmeeting = '';
        $user = Auth::user();
        $currentDate = date('Y-m-d');
        $currentUserRoleId = isset($user->roles->first()->id) ? $user->roles->first()->id : 0;

        $users = User::whereHas("roles", function ($q) use ($currentUserRoleId) {
            $q->where('id', '<=', $currentUserRoleId);
        })->get();

        if (($user_id != $user->id) && $user_id) {
            $user = User::where('id', $user_id)->first();
            if ($category != 'no') {
                $meeting = Addmeeting::where('category', $category)->where('host_id', $user_id)->whereBetween('meeting_date', [$startingDate, $endingDate])->get();
                $addmeeting = Addmeeting::where('category', $category)->where('host_id', $user_id)->orderBy('id', 'DESC')->whereBetween('meeting_date', [$startingDate, $endingDate])->orderBy('id', 'DESC')->paginate(10);
            } else {
                $meeting = Addmeeting::where('host_id', $user_id)->whereBetween('meeting_date', [$startingDate, $endingDate])->get();
                $addmeeting = Addmeeting::where('host_id', $user_id)->whereBetween('meeting_date', [$startingDate, $endingDate])->orderBy('id', 'DESC')->orderBy('id', 'DESC')->paginate(10);
            }
        } else {
            $host_id = isset($user->id) ? $user->id : '';
            if (!$category) {
                $meeting = Addmeeting::orwhere('host_id', $host_id)->orWhereHas('meeting', function ($q) use ($host_id) {
                    $q->where('user_id', $host_id);
                })->distinct()->get();
                $addmeeting = Addmeeting::orwhere('host_id', $host_id)->orWhereHas('meeting', function ($q) use ($host_id) {
                    $q->where('user_id', $host_id);
                })->distinct()->orderBy('id', 'DESC')->orderBy('id', 'DESC')->paginate(10);
            } else {
                if ($category == 'QuickCheck') {
                    $category = "Quick Check";
                }
                $meeting = Addmeeting::where('category', $category)->orwhere('host_id', $host_id)->orWhereHas('meeting', function ($q) use ($host_id) {
                    $q->where('user_id', $host_id);
                })->distinct()->get();
                $addmeeting = Addmeeting::where('category', $category)->where('host_id', $host_id)->orWhereHas('meeting', function ($q) use ($host_id) {
                    $q->where('user_id', $host_id);
                })->distinct()->orderBy('id', 'DESC')->paginate(10);
            }
        }

        return view('AfterLogin.report', compact('addmeeting', 'meeting', 'user', 'category', 'startingdate', 'endingdate', 'users'));
    }
    public function meetingStatus($meetingId, $meetingStatus)
    {
        $meeting = '';
        try {
            $meeting = Addmeeting::where('id', $meetingId)->update(
                array("status" => $meetingStatus)
            );
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['status' => 500, 'msg'=>$e->getMessage()], 500);
            exit;
        }
        if ($meeting) {
            return response()->json(['status' => 200], 200);
            exit;
        } else {
            return response()->json(['status' => 500, 'msg'=>"Oops! Something went wrong."], 500);
            exit;
        }
    }
}
