<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($startingdate = null, $endingdate = null, $role_id = null)
    {
        $startingDate = date('Y-m-d', strtotime($startingdate));
        $endingDate = date('Y-m-d', strtotime($endingdate));
        $user = Auth::user();
        $currentDate = date('Y-m-d');
        $currentUserRoleId = isset($user->roles->first()->id) ? $user->roles->first()->id : 0;
        $role = Role::where('id', '<=', $currentUserRoleId)->where("name", "!=", "Admin")->get();
        $alluser = 0;
        $users = 0;
        if (!$role_id) {
            $alluser = User::whereHas("roles", function ($q) {
                $q->where("name", "!=", "Admin");
            })->get();

            $users = User::whereHas("roles", function ($q) {
                $q->where("name", "!=", "Admin");
            })->orderBy('id', 'DESC')->paginate(10);

        }
        else{
            $roleName = Role::where('id', $role_id)->first()->name;
            $alluser = User::role($roleName)->whereHas("roles", function ($q) {
                $q->where("name", "!=", "Admin");
            })->whereBetween('created_at', [$startingDate, $endingDate])->get();
    
            $users = User::role($roleName)->whereHas("roles", function ($q) {
                $q->where("name", "!=", "Admin");
            })->whereBetween('created_at', [$startingDate, $endingDate])->orderBy('id', 'DESC')->paginate(10);

        }

        return view('AfterLogin.alluser', compact('users', 'alluser', 'role', 'startingdate', 'endingdate','role_id'));
    }
    public function destroy($id)
    {
        $users = User::where('id', $id)->firstorfail()->delete();
        return redirect()->back();
    }
    public function editUser($id)
    {
        $user = User::where('id', $id)->first();
        return view('AfterLogin.edit-user', compact('user'));
    }
    public function updateUser(Request $req)
    {
        $req->validate(
            [
                'name' => 'required',
                'password' => 'required|min:5',
                'email' => 'required|unique:users,email,'. $req->user_id,
                'employee_id' => 'required|unique:users,employee_id'.$req->user_id,
                'role_name' => 'required'
            ],
            [
                'name.required' => 'Name is required',
            ]
        );
        // try {
        //     $user = User::create([
        //         'name' => $req->name,
        //         'email' => $req->email,
        //         'number' => $req->number,
        //         'employee_id' => $req->employee_id,
        //     ])->assignRole($req->role_name);
        //     if (!empty($user)) {
        //         toastr()->success('Add Member Successfully');
        //         return redirect()->back();
        //     } else {
        //         toastr()->error('Oops! Something went wrong.');
        //         return redirect()->back();
        //     }
        // } catch (\Exception $e) {
        //     return $e->getMessage();
        // }
        // $user = User::where('id', $id)->first();
        // return view('AfterLogin.edit-user', compact('user'));
    }
}
