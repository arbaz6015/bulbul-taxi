<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Addmeeting;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    { 
        $addmeeting = Addmeeting::get();
        return view('AfterLogin.ScheduleMeeting.calander',compact('addmeeting'));
    }
}
