<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Addmeeting;
use App\Models\User;
use App\Models\UserMeeting;
use Illuminate\Support\Facades\Auth;


class AllMeetingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($category = null, $startingdate = null, $endingdate = null, $user_id = null)
    {
        $startingDate = date('Y-m-d', strtotime($startingdate));
        $endingDate = date('Y-m-d', strtotime($endingdate));
        $category = $category;
        $meeting = 0;
        $addmeeting = '';
        $user = Auth::user();
        $currentDate = date('Y-m-d');
        $currentUserRoleId = isset($user->roles->first()->id) ? $user->roles->first()->id : 0;

        $users = User::whereHas("roles", function ($q) use ($currentUserRoleId) {
            $q->where('id', '<=', $currentUserRoleId);
        })->get();

        if ($user_id) {
            $user = User::where('id', $user_id)->first();

            if ($category != 'no') {
                $meeting = Addmeeting::where('category', $category)->where('host_id', $user_id)->whereBetween('meeting_date', [$startingDate, $endingDate])->get();
                $addmeeting = Addmeeting::where('category', $category)->where('host_id', $user_id)->orderBy('id', 'DESC')->whereBetween('meeting_date', [$startingDate, $endingDate])->orderBy('id', 'DESC')->paginate(10);
            } else {
                $meeting = Addmeeting::where('host_id', $user_id)->whereBetween('meeting_date', [$startingDate, $endingDate])->get();
                $addmeeting = Addmeeting::where('host_id', $user_id)->whereBetween('meeting_date', [$startingDate, $endingDate])->orderBy('id', 'DESC')->paginate(10);
            }
        } else {
            $host_id = isset($user->id) ? $user->id : '';
            if (!$category) {
                $meeting = Addmeeting::orwhere('host_id', $host_id)->orWhereHas('meeting', function ($q) use ($host_id) {
                    $q->where('user_id', $host_id);
                })->distinct()->get();
                $addmeeting = Addmeeting::orwhere('host_id', $host_id)->orWhereHas('meeting', function ($q) use ($host_id) {
                    $q->where('user_id', $host_id);
                })->distinct()->orderBy('id', 'DESC')->paginate(10);
            } else {
                if ($category == 'QuickCheck') {
                    $category = "Quick Check";
                }
                $meeting = Addmeeting::where('category', $category)->orwhere('host_id', $host_id)->orWhereHas('meeting', function ($q) use ($host_id) {
                    $q->where('user_id', $host_id);
                })->distinct()->get();
                $addmeeting = Addmeeting::where('category', $category)->where('host_id', $host_id)->orWhereHas('meeting', function ($q) use ($host_id) {
                    $q->where('user_id', $host_id);
                })->distinct()->orderBy('id', 'DESC')->paginate(10);
            }
        }

        // $users = User::get();
        return view('AfterLogin.ScheduleMeeting.allMeeting', compact('addmeeting', 'meeting', 'user', 'category', 'startingdate', 'endingdate', 'users'));
    }
    public function show($id)
    {
        $collectionOfRole = array("Admin");
        $currentRole = Auth::user()->getRoleNames()[0];
        if (!in_array($currentRole, $collectionOfRole)) {
            array_push($collectionOfRole, $currentRole);
        }

        $users = User::whereHas("roles", function ($q) use ($collectionOfRole) {
            $q->whereNotIn('name', $collectionOfRole);
        })->get();
        // $users = User::get();
        $addmeeting = Addmeeting::where('id', $id)->first();
        // echo"<pre>";
        // print_r($addmeeting);
        return view('AfterLogin.ScheduleMeeting.editMeeting', compact('addmeeting', 'users'));
    }
    public function edit(Request $req)
    {

        $req->validate(
            [
                'meeting_name' => 'required',
                'participants' => 'required',
                'category' => 'required',
                'meeting_date' => 'required',
                'meeting_time' => 'required'
            ],
            [
                'meeting_name.required' => 'Meeting name is required',
                'meeting_date.required' => 'Meeting date is required',
                'meeting_time.required' => 'Metting time is required'
            ]
        );
        if ($req->hours == 0 && $req->minute == 0) {
            toastr()->warning('Duration is required');
            return redirect()->back();
        }
        $duration = $req->hours . ":" . $req->minute;
        $userId = array_filter(explode(',', $req->participants));
        try {
            $addmeeting = Addmeeting::where('id', $req->meet_id)->update([
                'meeting_name' => $req->meeting_name,
                'description' => $req->description,
                'participant' => json_encode($userId, JSON_FORCE_OBJECT),
                'meeting_id' => $req->meeting_id,
                'meeting_date' => $req->meeting_date,
                'meeting_time' => $duration,
                'category' => $req->category,
            ]);
            if (!empty($userId)) {
                foreach ($userId as $user) {
                    $meetingUser = UserMeeting::updateOrCreate([
                        'user_id' => $user,
                        'meeting_id' => $req->meet_id,
                    ], [
                        'user_id' => $user,
                        'meeting_id' => $req->meet_id,
                    ]);
                    if (empty($meetingUser)) {
                        toastr()->error('Oops! Something went wrong.');
                        return redirect()->back();
                    }
                }
            }
            $deleteMeeting = UserMeeting::where('meeting_id', $req->meet_id)->whereNotIn('user_id', $userId)->delete();
            if (empty($addmeeting)) {
                toastr()->error('Oops! Something went wrong.');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            toastr()->error($e->getMessage());
        }
        toastr()->success('Update Meeting Successfully');
        return redirect()->back();
        // return redirect()->back();
    }
    public function destroy(Request $req)
    {
        try {
            UserMeeting::where('meeting_id', $req->id)->delete();
            Addmeeting::where('id', $req->id)->delete();
            toastr()->success('Delete meeting Successfully');
            return redirect()->back();
        } catch (\Exception $e) {
            toastr()->error($e->getMessage());
            return redirect()->back();
        }
    }
}
