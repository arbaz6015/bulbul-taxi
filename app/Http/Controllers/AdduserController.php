<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Providers\RouteServiceProvider;

class AdduserController extends Controller
{

    protected function create(Request $req)
    {
        $req->validate(
            [
                'name' => 'required',
                'password' => 'required|min:5',
                'email' => 'required|email|unique:users',
                'employee_id' => 'required|unique:users',
                'role_name' => 'required'
            ],
            [
                'name.required' => 'Name is required',
                'password.required' => 'Password is required',
                'password.role_name' => 'Role_name is required'
            ]
        );
        try {
            $user = User::create([
                'name' => $req->name,
                'email' => $req->email,
                'password' => Hash::make($req->password),
                'number' => $req->number,
                'employee_id' => $req->employee_id,
            ])->assignRole($req->role_name);
            if (!empty($user)) {
                toastr()->success('Add Member Successfully');
                return redirect()->back();
            } else {
                toastr()->error('Oops! Something went wrong.');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('AfterLogin.addUser');
    }
}
