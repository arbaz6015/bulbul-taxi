<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Addmeeting;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $req)
    {
        $user = Auth::user();
        $userId = $user->id;
        $users = User::get();

        $addmeeting = Addmeeting::orwhere('host_id', $user->id)->orWhereHas('meeting', function ($q) use ($userId) {
            $q->where('user_id', $userId);
        })->distinct()->get();

        $currentDate = date('Y-m-d');
        $add = Addmeeting::orwhere('host_id', $user->id)->orWhereHas('meeting', function ($q) use ($userId) {
            $q->where('user_id', $userId);
        })->where('meeting_date', '<', $currentDate)->get();
        $up_date = Addmeeting::orwhere('host_id', $user->id)->orWhereHas('meeting', function ($q) use ($userId) {
            $q->where('user_id', $userId);
        })->where('meeting_date', '>', $currentDate)->get();
        $today = Addmeeting::orwhere('host_id', $user->id)->orWhereHas('meeting', function ($q) use ($userId) {
            $q->where('user_id', $userId);
        })->where('meeting_date', '=', $currentDate)->get();
        return view('home', compact('addmeeting', 'users', 'add', 'up_date', 'today'));
    }
}
