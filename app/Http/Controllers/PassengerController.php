<?php

namespace App\Http\Controllers;
use App\Models\Passengers;

use Illuminate\Http\Request;

class PassengerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('AfterLogin.passenger');
    }
    public function delete($id)
    {
        $passengers = Passengers::find($id);
        $passengers->delete();
        return redirect()->route('passenger.passenger')->with('all','Passengers Delete Successfully');
       
    }
}
