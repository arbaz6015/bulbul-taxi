<?php

namespace App\Http\Controllers;

use App\Models\Fares;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FareController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('AfterLogin.addfare');
    }

    public function store(Request $request)
    {

        $fares = new Fares;
        $fares->vehicle_type = $request->vehicle_type;
        $fares->fare_per_km = $request->fare_per_km;
        $fares->fare_per_minute = $request->fare_per_minute;
        $fares->minimun_fare = $request->minimun_fare;
        $fares->minimun_distance = $request->minimun_distance;
        $fares->waiting_fare = $request->waiting_fare;
        $fares->save();
        // return redirect()->route('fares.all');
        return redirect('all');

    }
    // FARE TABLE 
    public function view()
    {
        $fares = Fares::all();
        // echo "<pre>";
        // print_r($fares->toArray());
        // echo "</pre>";
        // die;
        $data = compact('fares');
        return view('AfterLogin.all')->with($data);
    }
   
    // EDIT DATA 
    public function edit($id)
    {
        $fares = Fares::find($id);
        return view('AfterLogin.edit', compact('fares'));
    }
    public function update(Request $request)
    {
        $fares = Fares::find($request->edit);
        $fares->vehicle_type = $request->input('vehicle_type');
        $fares->fare_per_km = $request->input('fare_per_km');
        $fares->fare_per_minute = $request->input('fare_per_minute');
        $fares->minimun_fare = $request->input('minimun_fare');
        $fares->minimun_distance = $request->input('minimun_distance');
        $fares->waiting_fare = $request->input('waiting_fare');
        $fares->update();
        return redirect()->route('fare.all')->with('all','Fare Updated Successfully');
       
    }
    public function delete($id)
    {
        $fares = Fares::find($id);
        $fares->delete();
        return redirect()->route('fare.all')->with('all','Fare Delete Successfully');       
    }
}

