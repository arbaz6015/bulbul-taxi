<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Drivers;
use Image;
use Illuminate\Support\Facades\Storage;

class AdddriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('AfterLogin.adddriver');

    }
    public function Pweb(Request $request)
    {
        
        $drivers = new Drivers;
        // $drivers->users_id = $request->users_id;
        $drivers->photo = $request->photo;
        // $drivers->government_id_photo = $request->government_id_photo	; 
        $drivers->name = $request->name	; 
        $drivers->street_address = $request->street_address	;        
        $drivers->city = $request->city	;        
        $drivers->country = $request->country	;        
        $drivers->zip_code = $request->zip_code	;        
        $drivers->gender = $request->grnder	;        
        $drivers->driver_government_id = $request->driver_government_id	;        
        $drivers->email = $request->email	;        
        $drivers->driver_license_no = $request->driver_license_no	;        
        $drivers->phone_number = $request->phone_number	;        
        $drivers->date_of_birth = $request->date_of_birth	;               
        $drivers->save();

       


    }
    public function store(Request $request){
        // echo '<pre>';
        // print_r($request);
        // exit;
        $drivers = new Drivers;
        if ($request->hasFile('photo')) {
            $driver_image      = $request->file('photo');
            $fileName = time() . '.' . $driver_image->getClientOriginalExtension();
            
            $img = Image::make($driver_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $drivers->photo = $fileName;       
        }
       
        $drivers->name = $request->name	; 
        $drivers->street_address = $request->street_address	;        
        $drivers->city = $request->city	;        
        $drivers->country = $request->country	;        
        $drivers->zip_code = $request->zip_code	;        
        $drivers->gender = $request->grnder	;        
        $drivers->driver_government_id = $request->driver_government_id	;        
        $drivers->email = $request->email	;        
        $drivers->driver_license_no = $request->driver_license_no	;        
        $drivers->phone_number = $request->phone_number	;        
        $drivers->date_of_birth = $request->date_of_birth;
        $drivers->save();
        
        return redirect()->route('adddriver.alldriver')->with('success', 'Driver created successfully');
    }
    public function view()
    {
        $drivers = Drivers ::all();
        // echo "<pre>";
        // print_r($fares->toArray());
        // echo "</pre>";
        // die;
        $data = compact('drivers');
        return view('AfterLogin.alldriver')->with($data);
    }
    public function edit($id)
    {
        $drivers = Drivers::find($id);
        return view('AfterLogin.editdriver', compact('drivers'));
    }
    public function update(Request $request)
    {
        $drivers = Drivers::find($request->edit);
        $drivers->name = $request->input('name');
        if ($request->hasFile('photo')) {
            $driver_image      = $request->file('photo');
            $fileName = time() . '.' . $driver_image->getClientOriginalExtension();
            
            $img = Image::make($driver_image->getRealPath());
            $img->resize(512, 512, function ($constraint) {
                $constraint->aspectRatio();                 
            });
            
            $img->stream(); // <-- Key point
            
            //dd();
            Storage::disk('local')->put('public/images/driver'.'/'.$fileName, $img, 'public');
            $drivers->photo = $fileName;       
        }
        // $drivers->photo = $request->input('photo');
        $drivers->street_address = $request->input('street_address');
        $drivers->city = $request->input('city');
        $drivers->country = $request->input('country');
        $drivers->zip_code = $request->input('zip_code');
        $drivers->driver_government_id = $request->input('driver_government_id');
        $drivers->email = $request->input('email');
        $drivers->driver_license_no = $request->input('driver_license_no');
        $drivers->phone_number = $request->input('phone_number');
        $drivers->update();
        return redirect()->route('adddriver.alldriver')->with('all','Driver Updated Successfully');
       
    }
    public function delete($id)
    {
        $drivers = Drivers::find($id);
        $drivers->delete();
        return redirect()->route('adddriver.alldriver')->with('all','Driver Delete Successfully');
       
    }


}