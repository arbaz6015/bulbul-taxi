<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAddmeetingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addmeeting', function($table) {
            $table->string('category')->nullable();
            $table->string('duration')->nullable();
            $table->string('meeting_place')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addmeeting', function($table) {
            $table->dropColumn('category');
            $table->dropColumn('duration');
            $table->dropColumn('meeting_place');
        });
    }
}
