<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevenues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenues', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('trip_id');
            $table->unsignedBigInteger('driver_id');
            $table->string('base_amount')->nullable();
            $table->string('trip')->nullable();
            $table->string('road_tax')->nullable();
            $table->string('tax')->nullable();
            $table->string('tatal_amount')->nullable();
            $table->string('driver_amount')->nullable();
            $table->string('admin_amount')->nullable();
            $table->string('status')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();

            $table->foreign('trip_id')
            ->references('id')
            ->on('trips')->onDelete('cascade');
            $table->foreign('driver_id')
             ->references('id')
             ->on('drivers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenues');
    }
}
