<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
// $table->unsignedBigInteger('users_id')->nullable();
$table->string('photo')->nullable();     
$table->string('rc_photo')->nullable();
$table->string('government_id_photo')->nullable();      

$table->string('street_address')->nullable();
$table->string('city')->nullable();
$table->string('country')->nullable();
$table->string('zip_code')->nullable();
$table->enum('gender',['male','femal','other'])->nullable();
$table->string('driver_government_id')->nullable();
$table->string('email')->nullable();
$table->string('driver_license_no')->nullable();
$table->string('phone_number')->nullable();
$table->date('date_of_birth')->nullable();
$table->timestamp('deleted_at')->nullable();
$table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->nullable();
// $table->foreign('users_id')
// ->references('id')
// ->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
