<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fares', function (Blueprint $table) {
            $table->id();
            $table->enum('vehicle_type',['Sedan','Large SUV','Mid Size SUV','Mini SUV', 'Hatchback', 'Convertible', 'Coupe', 'Sports Car', 'Mini Van', 'Station Wagon'])->nullable();
            $table->string('fare_per_km')->nullable();
            $table->string('fare_per_minute')->nullable();
            $table->string('minimun_fare')->nullable();
            $table->string('minimun_distance')->nullable();
            $table->string('waiting_fare')->nullable();
           
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fares');
    }
}
