<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddmeetingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addmeeting', function (Blueprint $table) {
            $table->id();
            $table->string('meeting_name',20);
            $table->string('description',100)->nullable();
            $table->text('participant',100);
            $table->string('meeting_id',50);
            $table->string('host_name',20);
            $table->timestamp('meeting_date')->nullable();
            $table->string('meeting_time');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addmeeting');
    }
}
