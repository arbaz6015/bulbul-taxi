<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTimeDataTypeAddMeetings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addmeeting', function (Blueprint $table) {

            $table->time('meeting_time')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */ 
    public function down()
    {
        Schema::table('addmeeting', function (Blueprint $table) {
            $table->dropColumn('meeting_time');
        });
    }
}
