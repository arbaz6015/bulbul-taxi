<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAddmeetingTableToAddedHostIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addmeeting', function (Blueprint $table) {
            $table->tinyInteger('status')->comment('0=>Happen, 1=>Delay, 2=>Cancel')->default(0);
            $table->bigInteger('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('users')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addmeeting', function (Blueprint $table) {
            $table->dropColumn('host_id');
            $table->dropColumn('status');
        });
    }
}
