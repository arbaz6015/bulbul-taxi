<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->id();
             $table->unsignedBigInteger('trip_id');
             $table->unsignedBigInteger('driver_id');
             $table->unsignedBigInteger('passenger_id');
             $table->string('trip_form')->nullable();
             $table->string('trip_to')->nullable();
             $table->string('start_time')->nullable();
             $table->string('end_time')->nullable();
             $table->string('distance')->nullable();
             $table->string('fare')->nullable();
             $table->string('status')->default('active');
             $table->timestamp('deleted_at')->nullable();
             $table->timestamp('created_at')->useCurrent();
             $table->timestamp('updated_at')->nullable();
             $table->foreign('driver_id')
              ->references('id')
              ->on('drivers')->onDelete('cascade');
              $table->foreign('passenger_id')
              ->references('id')
              ->on('passengers')->onDelete('cascade');

             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
