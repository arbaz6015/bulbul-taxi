<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->string('vehicle_license_plate_no')->nullable(); 
            $table->string('engine_number')->nullable();
            $table->string('chasis_number')->nullable();
            $table->string('rc_photo')->nullable();
            $table->string('puc_photo')->nullable();
            $table->string('insurance_photo')->nullable();
            $table->string('vehicle_photos')->nullable();
            $table->enum('vehicle_type',['Sedan','Large SUV','Mid Size SUV','Mini SUV', 'Hatchback', 'Convertible', 'Coupe', 'Sports Car', 'Mini Van', 'Station Wagon'])->nullable();
            // $table->string('vehicle_manufacturer')->nullable();
            $table->string('vehicle_model')->nullable();
            $table->tinyInteger('seating_capacity',false, true)->length(2)->nullable();
            $table->date('tax_submission_date')->nullable();
            $table->date('insurance_renewal_date')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('driver_id')
            ->references('id')
            ->on('drivers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
