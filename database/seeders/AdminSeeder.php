<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User as UserModel;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserModel::create([
            'name' => 'Admin',
            'email' => ("admin@bulbultaxi.com"),
            'password' => Hash::make("hachi#123"),
            'phone' => (45374564875),
        ])->assignRole('Admin');
    }
}

