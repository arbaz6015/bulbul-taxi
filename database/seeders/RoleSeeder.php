<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'Admin']);
        $role = Role::create(['name' => 'Business Assitant I']);
        $role = Role::create(['name' => 'Business Assistant II']);
        $role = Role::create(['name' => 'Team Manager']);
        $role = Role::create(['name' => 'Junior Business Manager']);
        $role = Role::create(['name' => 'Business Manager']);
        $role = Role::create(['name' => 'Senior Business Manager']);
        $role = Role::create(['name' => 'Business Director']);
        $role = Role::create(['name' => 'Senior Business Director']);
    }
}
