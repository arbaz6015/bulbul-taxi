@extends('layouts.section.dashboards')
@section('content')
<div class="content-wrapper mt-5 py-4">

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Driver Payment Edit </h1>
                     </div>
                       <form>
                         <div class="row px-3 my-4">
                          <!-- <div class="name">

                          <h3 class="m-b-20 p-b-5 b-b-default f-w-600 px-3 my-4">Driver Information</h3>
                       <hr class="bg-info">
                      </div> -->
                                <div class="col-sm-6 mb-2 name2 my-4">
                                   <p class="mb-2 f-w-600">Transaction i.d</p>
                                   <input class="form-control in1"  type="text" name="number" id="number" placeholder="Enter Transaction i.d">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>

                                <div class="col-sm-6 mb-2 my-4 ">
                                   <p class="mb-2 f-w-600">Name</p>
                                   <input class="form-control in1" type="text" name="name" id="name" placeholder="Enter Name">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Date</p>
                                   <input class="form-control in1" type="date" name="date" id="date">
                                   <!-- <input class="form-control in1" type="text" name="name" id="name" placeholder="Enter your City"> -->

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Amount</p>
                                   <input class="form-control in1" type="text" name="amount" id="amount" placeholder="Enter Amount">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>                                                                         
                                <div class="col-sm-6 mb-2 my-4">
                                    <p class="mb-2 f-w-600">Status</p>
                                    <input class="form-control in1" type="text" name="status" id="status" placeholder="Enter Status">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->name }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Commission</p>
                                   <input class="form-control in1" type="text" name="commission" id="commission" placeholder="Enter Commission">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>  
                                <div class="col-sm-6 mb-2 my-4 ">
                                    <p class="mb-2 f-w-600">Upload Driver Photo</p>
                                    <input class="form-control" type="file" name="document" id="document">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
          
                            </div>
                           <br>
                        <div class="col-lg-12  text-center">
										<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-10 my-1 btn btn-info" data-upgraded=",MaterialButton,MaterialRipple">Update<span class="mdl-button__ripple-container"><span class="mdl-ripple is-animating"></span></span></button>

									</div>
                   </form>
            </div>
        </div>
    </div>
</section>
</div>










<!-- css add  -->




  <style>
.name h3 {
  font-family: "Trirong", serif;
}

.name2 p {
  font-family: "Trirong", serif;
}
.in1 {
  border: none;
  border-bottom: 1px solid green;
}

.container{
  margin-left: 20%;

}

</style>
@endsection
