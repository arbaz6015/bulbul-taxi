@extends('layouts.section.dashboards')
@section('content')
<div class="content-wrapper mt-5 py-4">

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Add New Drivers </h1>
                     </div>
                       <form action="{!! route('adddriver.store') !!}" method="POST" enctype="multipart/form-data">
                       @csrf

                         <div class="row px-3 my-4">
                          <div class="name">

                          <h3 class="m-b-20 p-b-5 b-b-default f-w-600 px-3 my-4">Driver Information</h3>
                       <hr class="bg-info">
                      </div>
                                <div class="col-sm-6 mb-2 name2 my-4">
                                   <p class="mb-2 f-w-600">Name</p>
                                   <input class="form-control in1"  type="text" name="name" id="name" placeholder="Enter Driver Name"  required>

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>

                                <div class="col-sm-6 mb-2 my-4 ">
                                   <p class="mb-2 f-w-600">Street Address</p>
                                   <input class="form-control in1" type="text" name="street_address" id="street_address" placeholder="Enter Driver Address"  required>

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">City</p>
                                   <input class="form-control in1" type="text" name="city" id="city" placeholder="Enter Driver City  required">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Country</p>
                                   <input class="form-control in1" type="text" name="country" id="country" placeholder="Enter Driver country  required">
                                   <!-- <select id="gender" class="form-control in1" name="country">
                                     <option value="gender">Select Country </option>
                                     <option value="1">United States</option>
                                     <option value="2">India</option>
                                     <option value="3">Bangladesh</option>
                                     <option value="4">China</option>
                                     <option value="5">Pakistan</option>
                                     <option value="6">Germany</option>
                                     <option value="7">Italy</option>
                                     <option value="8">Malaysia</option>

                                    </select> -->
                                   <!-- <input class="form-control in1" type="text" name="name" id="name" placeholder="Enter Country"> -->

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Zip Code</p>
                                   <input class="form-control in1" type="text" name="zip_code" id="zip_code" placeholder="Enter Zip Code">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4 ">
                                   <p class="mb-2 f-w-600 ">Gender</p>
                                   <!-- <input type="text" name="name" id="name" placeholder="Enter User Gender"> -->
                                   <select id="gender" class="form-control in1" name="gender">
                                     <option value="gender">Select Gender</option>
                                     <option value="saab">Male</option>
                                     <option value="fiat">Female</option>
                                     <option value="audi">Other</option>
                                    </select>

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Driver Identity card</p>
                                   <input class="form-control in1" type="text" name="driver_government_id" id="driver_government_id" placeholder="Driver Government i.d">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                    <p class="mb-2 f-w-600">Email</p>
                                    <input class="form-control in1" type="email" name="email" id="email" placeholder="Enter Driver Email">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->name }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Driver License No.</p>
                                   <input class="form-control in1" type="text" name="driver_license_no " id="driver_license_no " placeholder="Driver License No.....">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2  my-4">
                                    <p class="mb-2 f-w-600">Phone Number</p>
                                    <input class="form-control in1" type="text" name="phone_number" id="phone_number" placeholder="Enter Driver Number">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->number }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2  my-4">
                                    <p class="mb-2 f-w-600">Date of Birth</p>
                                    <input class="form-control in1" type="date" name="date_of_birth" id="date_of_birth">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                    <p class="mb-2 f-w-600">Upload Driver Photo</p>
                                    <input class="form-control " type="file" name="photo" id="photo">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>   
                                <!-- <div class="col-sm-6 mb-2 my-4">
                                    <p class="mb-2 f-w-600">Upload RC Photo</p>
                                    <input class="form-control " type="file" name="rc_photo" id="rc_photo">
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6>
                                </div>  -->
                                <!-- <div class="col-sm-6 mb-2 my-4">
                                    <p class="mb-2 f-w-600">Upload Government i.d  Photo</p>
                                    <input class="form-control " type="file" name="government_id_photo" id="government_id_photo">
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6>
                                </div>                                                           -->
                            </div>
                           <br>
                        <div class="col-lg-12  text-center">
                        <input type="submit" value="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 my-3 btn btn-info" data-upgraded=",MaterialButton,MaterialRipple">

										<!-- <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-10 my-1 btn btn-info" data-upgraded=",MaterialButton,MaterialRipple">Submit<span class="mdl-button__ripple-container"><span class="mdl-ripple is-animating"></span></span></button> -->
									</div>
                 </form>
            </div>
        </div>
    </div>
</section>
</div>










<!-- css add  -->




  <style>
.name h3 {
  font-family: "Trirong", serif;
}

.name2 p {
  font-family: "Trirong", serif;
}
.in1 {
  border: none;
  border-bottom: 1px solid green;
}

.container{
  margin-left: 20%;

}

</style>

@endsection
