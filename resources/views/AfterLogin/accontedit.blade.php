@extends('layouts.section.dashboards')
@section('content')

<div class="content-wrapper mt-5 py-4">

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Driver Account Details</h1>
                </div>
<form action= "{!! url('account/update-data') !!}" method="POST">
@csrf
<input type="hidden" value="{{$driver_accounts->id}}" name="edit"/>
                <div class="row px-3 my-4">
                               
                               <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Bank's Name</p>
                                   <!-- <label for="cars">Choose Vehicle Type</label> -->
                                   <!-- <select id="bank_name" class="form-control in1" name="bank_name">
                                    <option value="Vehicle">Select Bank's</option>
                                    <option value="suv">JPMorgan Chase</option>
                                    <option value="Sedan">Bank of America</option>
                                    <option value="van">Citigroup</option>
                                    <option value="wagon">U.S. Bancorp </option>
                                   </select> -->
                                   <input type="text" class="form-control in1" value="{{$driver_accounts->bank_name}}" name="bank_name">
                           
                               </div>
                              
                               <!-- <div class="col-sm-6 mb-2 my-4">
                                  <p class="mb-2 f-w-600">Name Of The Account Holder</p>
                                  <input class="form-control in1" type="text" name="driver_id" id="driver_id">
                               </div>                               -->
                              
                               <div class="col-sm-6 mb-2  my-4">
                                   <p class="mb-2 f-w-600">Account Number</p>
                                   <input class="form-control in1" type="text" value="{{$driver_accounts->account_number}}" name="account_number" id="account_number" >
                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->number }}</h6> -->
                               </div>
                               <div class="col-sm-6 mb-2 my-4 ">
                                   <p class="mb-2 f-w-600">Routing Code</p>
                                   <input class="form-control in1" type="text" value="{{$driver_accounts->routing_number}}" name="routing_number" id="routing_number">
                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                               </div>

                              
                              
                             </div>
                           <br>
                        <div class="col-lg-12 p-t-20 text-center">
                        <input type="submit" value="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 my-3 btn btn-info" data-upgraded=",MaterialButton,MaterialRipple">

										<!-- <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 my-3 btn btn-info" data-upgraded=",MaterialButton,MaterialRipple">Submit<span class="mdl-button__ripple-container"><span class="mdl-ripple is-animating"></span></span></button> -->
									
									</div>                         
                   </form>
            </div>
        </div>
    </div>
</section>
</div>







<!-- css add  -->




  <style>
.name h3 {
  font-family: "Trirong", serif;
}

.name2 p {
  font-family: "Trirong", serif;
}
.in1 {
  border: none;
  border-bottom: 1px solid green;
 

}

/* .container{
  margin-left: 20%;

} */
.row>* {
    flex-shrink: 0;
    width: 100%;
    max-width: 100%;
    padding-right: calc(var(--bs-gutter-x)/ 2);
    padding-left: calc(var(--bs-gutter-x)/ 2);
    margin-top: var(--bs-gutter-y);
}


</style>
@endsection


















