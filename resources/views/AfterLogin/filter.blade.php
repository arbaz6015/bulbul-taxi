<div class="row px-3 py-2">
    <div class="col-3 mt-4 d-flex">
        <input class="form-control" type="search" placeholder="Search with meeting name" id="search" aria-label="Search">

    </div>
    <div class="col-2 mt-4 d-flex">
        <select class="form-select filter-by-category" aria-label="Default select example">
            <option value="">Filter by Category</option>
            <option value="QuickCheck" {{$quickCheck}}>Quick Check</option>
            <option value="Consultation" {{$consultation}}>Consultation</option>
            <option value="Others" {{$others}}>Others</option>
        </select>
    </div>
    <div class="col-4 mt-4 input-group">
        <b class="mt-2">From:</b>
        <input type="date" class="form-control" value="{{isset($startingdate)?$startingdate:''}}" id="startDate" placeholder="dd/mm/yyyy">
        <b class="mt-2">To:</b>
        <input type="date" class="form-control" value="{{isset($endingdate)?$endingdate:''}}" id="endDate" placeholder="dd/mm/yyyy">
    </div>
    <div class="col-3 mt-4">
        <select class="form-select filter-by-user" aria-label="Default select example">
            <option value="">Filter by Role</option>
            @if(!empty($user))
            @foreach($users as $userVal)
            <option value="{{isset($userVal->id)?$userVal->id:0}}" {{((isset($user->id)?$user->id:'') == $userVal->id)?'selected':''}}>{{isset($userVal->name)?$userVal->name:0}} ({{isset(($userVal->getRoleNames())[0])?($userVal->getRoleNames())[0]:"NA"}})</option>
            @endforeach
            @endif

        </select>
    </div>

</div>
