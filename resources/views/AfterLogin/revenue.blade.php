@extends('layouts.section.dashboards')
@section('content')

 <div class="content-wrapper mt-5 py-4">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Revenue</h1>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-append">
                                <div class="btn btn-success">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
    <div class="table-responsive mailbox-messages">
    <table class="table table-hover table-striped" >
    <thead>
												<tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">Trip ID</th>
													<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="End Time: activate to sort column ascending" style="width: 38.5625px;">Driver Name </th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip Id: activate to sort column ascending" style="width: 29.9844px;">Base Amount</th>
													<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip From: activate to sort column ascending" style="width: 81.375px;">Tip</th>
													<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.7188px;">Road Tax</th>
													<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Driver Name: activate to sort column ascending" style="width: 47.0469px;"> Tax</th>
													<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Distance: activate to sort column ascending" style="width: 68.4062px;">Total Amount</th>                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip To: activate to sort column ascending" style="width: 55.6406px;">Driver Amount</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Start Time: activate to sort column ascending" style="width: 38.6094px;">Admin Amount</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="End Time: activate to sort column ascending" style="width: 38.5625px;">Status </th>
											</thead>
											<tbody>
                                            <tr class="gradeX odd" role="row">
													<td>#2736</td>
													<td>John Smith</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-success">Paid</span>
													</td>
												</tr><tr class="gradeX even" role="row">
												<td>#54352</td>
												<td>John Smith</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-success">Paid</span>
													</td>
												</tr><tr class="gradeX odd" role="row">
												<td>#37532</td>
												<td>John Smith</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-danger">Unpaid</span>
													</td>
												</tr><tr class="gradeX even" role="row">
												<td>#35454</td>
												<td>John Smith</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-warning">Pending</span>
													</td>
												</tr><tr class="gradeX odd" role="row">
												<td>#3767</td>
												<td>John Smith</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-danger">Unpaid</span>
													</td>
												</tr><tr class="gradeX even" role="row">
												<td>#2397</td>
												<td>John Smith</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-warning">Pending</span>
													</td>
												</tr><tr class="gradeX odd" role="row">
												<td>#7352</td>
												<td>Ram</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-success">Paid</span>
													</td>
												</tr><tr class="gradeX even" role="row">
												<td>#5437</td>
												<td>John Smith</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-warning">Pending</span>
													</td>
												</tr><tr class="gradeX odd" role="row">
												<td>#2339</td>
												<td>John Smith</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-warning">Pending</span>
													</td>
												</tr><tr class="gradeX even" role="row">
												<td>#2510</td>
												<td>John Smith</td>
													<td class="center">$900</td>
													<td class="center">$50</td>
													<td class="center">$30</td>
													<td class="center">$20</td>
													<td class="center">	$200</td>
													<td class="center">$500</td>
													<td class="center">$1700</td>
													<td class="center">
														<span class="btn btn-success">Paid</span>
													</td>
												</tr></tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							</div></div></div>
                         </tbody>
                       </table>    
                    </div>    
                </div>
            </div>
        </div>
    </div>
  </div>
  </section>

  

    @endsection