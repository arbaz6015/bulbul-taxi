@extends('layouts.section.dashboards')
@section('content')
<div class="content-wrapper mt-5 py-4">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">View All Vehicle</h1>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
						<input type="text"  class="form-control" placeholder="Search" >
                            <div class="input-group-append">
                                <div class="btn btn-success">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!-- add html  -->
                     <div class="row">
						 @foreach($vehicles as $vehicles)
						<div class="col-lg-4 col-md-6 col-12 col-sm-6 my-3">
							<div class="blogThumb">

								<div class="thumb-center">
									<div class="thumb-center">
										<img class="img-responsive" src="{{ asset('storage/images/driver/'.$vehicles->vehicle_photos) }}" alt="{{$vehicles->vehicle_photos}}" width="326rem" >
										<div class="vehicle-name cyan-bgcolor">
											<div class="user-name">Vehicle Model</div>
										</div>
										<div class="vehicle-box">
											<p><strong>Engine number :</strong>
															{{$vehicles->engine_number}}
															</p>
											<p><strong>Vehicle plate_no:</strong> {{$vehicles->vehicle_license_plate_no}}</p>
											<p><strong>Seating Capacity:</strong>{{$vehicles->seating_capacity }}</p>
											<p><strong>Vehicle Type:</strong> {{$vehicles->vehicle_type}}</p>
											<p><strong>Driver Name:</strong> {{$vehicles->name}}</p>
											<!-- update  -->
											<a href="{{ url('addvehicle/delete-data/'.$vehicles->id) }}" class="btn btn-warning btn-xs ">
											<i class="far fa-trash-alt"></i>
											</a>
											<a href="{{ url('addvehicle/editvehicle/'.$vehicles->id) }}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i></a>
													</div>

												</div>
											</div>
										</div>
									</div>
									@endforeach
									<!-- end html  -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>


					<!-- </div> -->
                    <!-- card 2  -->












































<!-- css add  -->
<style>
    .vehicle-box {
    padding: 10px 20px;
    text-align: center;
    }
   .p {
    font-size: 14px;
    letter-spacing: 0;
    margin: 0 0 16px;
    line-height: 24px;
}
.vehicle-name {
    min-height: 50px;
    width: 20.5rem;
    color: #fff;
}
.user-name {
    padding: 3px;
    font-size: 22px;
    text-align: center;
    padding-top: 10px;

}
.bg-b-purple{
	background: linear-gradient(45deg, #b40bfb, #403e40);
}
.cyan-bgcolor {
	background: linear-gradient(45deg, #6008ffb3, #28bb71);
    color: #fff;
}
.light-dark-bgcolor{
	background: linear-gradient(45deg, #e74c3c, #7c8ea0)  !important;
}

/* card 2  */
.bg-b-orange{
	background: linear-gradient(45deg, #e7076d, #20c997);
}
.bg-b-green {
	background: linear-gradient(45deg, #f30a31, #262425);
}
.bg-b-danger {
	background: linear-gradient(45deg, #b40bfb, #403e40);
}
*, ::after, ::before {
    box-sizing: border-box;
}

</style>
@endsection
