@extends('layouts.section.dashboards')
@section('content')


<div class="content-wrapper mt-5 py-4">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Add Vehicle</h1>
                </div>
                <form action="{!! route('addvehicle.store') !!}" method="POST" enctype="multipart/form-data">
                       @csrf
                <div class="row px-3 my-4">
                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Driver Name</p>
                                   <!-- <input class="form-control in1"  type="text" name="vehicle_license_plate_no" id="vehicle_license_plate_no"> -->
                                    <!-- <label for="cars">Choose Vehicle Type</label> -->
                                   <select id="cars" class="form-control in1" name="name">
                                    <option value="Vehicle">Select Drivers Name</option>
                                    <option value="Arbaz kahn">Arbaz kahn</option>
                                    <option value="Ram">Ram </option>
                                    <option value="Johan">Johan</option>
                                    <option value="Wangon">Wangon</option>
                                   </select>
                               </div>
                               <div class="col-sm-6 mb-2 my-4 name2">
                                   <p class="mb-2 f-w-600">Vehicle License Plate no..</p>
                                   <input class="form-control in1"  type="text" name="vehicle_license_plate_no" id="vehicle_license_plate_no">
                                </div>
                                <div class="col-sm-6 mb-2 my-4 name2">
                                   <p class="mb-2 f-w-600">Engine Number</p>
                                   <input class="form-control in1"  type="text" name="engine_number" id="engine_number">
                                </div>
                                <div class="col-sm-6 mb-2 my-4 name2">
                                   <p class="mb-2 f-w-600">Chasis Number</p>
                                   <input class="form-control in1"  type="text" name="chasis_number" id="chasis_number">
                                </div>
                                <!-- <div class="col-sm-6 mb-2 my-4 name2 ">
                                   <p class="mb-2 f-w-600">vehicle_manufacturer	</p>
                                   <input class="form-control in1"  type="text" name="vehicle_manufacturer" id="vehicle_manufacturer">
                                </div> -->
                               <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Vehicle Types</p>
                                   <!-- <label for="cars">Choose Vehicle Type</label> -->
                                   <select id="cars" class="form-control in1" name="vehicle_type">
                                    <option value="Vehicle">Select Vehicle Type</option>
                                    <option value="Large SUV">Large SUV</option>
                                    <option value="Sedan">Sedan</option>
                                    <option value="Mid Size SUV">Mid Size SUV</option>
                                    <option value="Mini SUV">Mini SUV</option>
                                    <option value="Hatchback">Hatchback</option>
                                    <option value="Convertible">Convertible</option>
                                    <option value="Coupe">Coupe</option>
                                    <option value="Sports Car">Sports Car</option>
                                    <option value="Mini Van">Mini Van</option>
                                    <!-- <option value="wagon">Hatchback</option> -->
                                   </select>
                               </div>
                               <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Vehicle Model</p>
                                   <input class="form-control in1" type="text" name="vehicle_model" id="vehicle_model">
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Seating Capacity</p>
                                   <input class="form-control in1" type="text" name="seating_capacity" id="seating_capacity">
                                </div>
                                <!-- <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Price Per K/M (in $)</p>
                                   <input class="form-control in1" type="text" name="numbr" id="number">
                                </div> -->
                                <!-- <div class="col-sm-6 mb-2 my-4">
                                    <p class="mb-2 f-w-600">Price per Min (in $)</p>
                                    <input class="form-control in1" type="text" name="number" id="number">
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->name }}</h6>
                                </div> -->
                                <!-- <div class="col-sm-6 mb-2 my-4 ">
                                    <p class="mb-2 f-w-600">Minimun Fare (in $)</p>
                                    <input class="form-control in1" type="text" name="number" id="number" >
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->number }}</h6>
                                </div> -->
                                <!-- <div class="col-sm-6 mb-2 my-4 ">
                                    <p class="mb-2 f-w-600">Commission (in %)</p>
                                    <input class="form-control in1" type="text" name="commission" id="commission">
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6>
                                </div> -->
                                <!-- <div class="col-sm-6 mb-2 my-4">
                                    <p class="mb-2 f-w-600">Passenger Cancellation Time Limite (in minute)</p>
                                    <input class="form-control in1" type="text" name="number" id="number" >
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6>
                                </div> -->

                                <!-- <div class="col-sm-6 mb-2 my-4 ">
                                    <p class="mb-2 f-w-600">Passenger Cancellation Charges (in $)</p>
                                    <input class="form-control in1" type="text" name="number" id="number" >
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6>
                                </div> -->
                                <div class="col-sm-6 mb-2 my-4">
                                    <p class="mb-2 f-w-600">Insurance Renewal Date</p>
                                    <input class="form-control in1" type="date" name="insurance_renewal_date" id="insurance_renewal_date">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2  my-4">
                                    <p class="mb-2 f-w-600">Tax Renewal Date</p>
                                    <input class="form-control in1" type="date" name="tax_submission_date" id="tax_submission_date">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4 ">
                                    <p class="mb-2 f-w-600">Upload Vehicle Photo</p>
                                    <input class="form-control" type="file" name="vehicle_photos" id="vehicle_photos">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4 ">
                                    <p class="mb-2 f-w-600">Upload Vehicle PUC Photo</p>
                                    <input class="form-control" type="file" name="puc_photo" id="puc_photo">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 my-4 ">
                                    <p class="mb-2 f-w-600">Upload Vehicle RC Photo</p>
                                    <input class="form-control" type="file" name="rc_photo" id="rc_photo">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2  my-4">
                                <!-- <div class="input-group"> -->
                                <p class="mb-2 f-w-600">Upload Vehicle Insurance Photo</p>
                                <!-- <div class="input-group-prepend">
                                  <select class="form-select in1" id="inputGroupSelect01">
                                  <option selected>Choose...</option>
                                  <option value="1">Driving license</option>
                                <option value="2">Car Insurance Policy</option>
                                  <option value="3">Registration Certificate (RC)</option>
                                  <option value="4">Pollution Under Control Certificate (PUC)</option>
                                  <option value="5">Required Permits</option>
                                </select>
                                <br>
                                <div class="custom-file"> -->
                                <input class="form-control in1" type="file" name="insurance_photo" id="insurance_photo">
                                </div>
                            </div>
                           <br>
                        <div class="col-lg-12 p-t-20 text-center">
                        <input type="submit" value="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 my-3 btn btn-info" data-upgraded=",MaterialButton,MaterialRipple">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>










<!-- css add  -->




  <style>
.name h3 {
  font-family: "Trirong", serif;
}

.name2 p {
  font-family: "Trirong", serif;
}
.in1 {
  border: none;
  border-bottom: 1px solid green;


}

.container{
  margin-left: 20%;

}
.row>* {
    flex-shrink: 0;
    width: 100%;
    max-width: 100%;
    padding-right: calc(var(--bs-gutter-x)/ 2);
    padding-left: calc(var(--bs-gutter-x)/ 2);
    margin-top: var(--bs-gutter-y);
}

</style>
@endsection
