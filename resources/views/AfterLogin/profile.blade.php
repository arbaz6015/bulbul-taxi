@extends('layouts.section.dashboards')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper mt-5">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4 class="m-0 f-w-600">PROFILE</h4>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#" class="text-success">Home</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card user-card-full shadow border border-success">
                <div class="row m-l-0 m-r-0 mt-4">
                    <div class="col-sm-4 bg-c-lite-green user-profile">
                        <div class="card-block text-center">
                            <div class="m-b-25">
                                <img src="image/userlogo.png" class="img-radius img-fluid user-profile-image" alt="User-Profile-Image" role="button" width="60%" height="100px">
                            </div>
                            <h4 class="f-w-800 mt-3 text-success">{{ Auth::user()->name }}</h4>
                            <p>{{ Auth::user()->role_name }} </p>
                        </div>
                    </div>
                   <div class="col-sm-8 py-4 ">
                        <div class="card-block">
                            <h5 class="m-b-20 p-b-5 b-b-default f-w-600 px-3">Information</h5>
                            <hr class="bg-success">
                            <div class="row px-3">
                                <div class="col-sm-6 mb-2">
                                   <p class="mb-2 f-w-600">Email</p>
                                   <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6>
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">Name</p>
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->name }}</h6>
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">Phone Number</p>
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->number }} </h6>
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">Employee Id</p>
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6>
                                </div>
                            </div>
                            <hr class="bg-success">
                            <h5 class=" f-w-600 px-3 py-2">View Meetings Details</h5>
                            <ul class="list-unstyled  d-flex py-2">
                                <li class="px-3">
                                    <a href="">
                                        <i class="nav-icon text-success fas fa-user"></i>
                                    </a>
                                </li>
                                <li class="px-3"><a href=""><i class="nav-icon text-success fas fa-handshake"></i></a></li>
                                <li class="px-3"><a href=""><i class="nav-icon text-success fas fa-bell"></i></a></li>
                                <li class="px-3"><a href=""><i class="fas text-success fa-pen nav-icon"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
   
    <!-- /.content -->
</div>
@endsection

