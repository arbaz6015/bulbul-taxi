@extends('layouts.section.dashboards')
@section('content')

<div class="content-wrapper mt-5 py-3">
    <!-- Content Header (Page header) -->
      <!-- /.container-fluid -->
    <!-- /.content-header -->
    <!-- /.row (main row) -->
    <!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
             <div class="mb-3 row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Meeting Id</label>
                <div class="col-sm-10">
                  <h6 id="id_meeting"></h6>
                </div>
                <label for="staticEmail" class="col-sm-2 col-form-label">Meeting-Caegory</label>
                <div class="col-sm-10">
                    <h6 id="category"></h6>
                  </div>
            </div>                         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
    <section class="content">
      <div class="container-fluid mb-5">
          <div class="row">
              <div class="col-md-12">
                  <div class="card text-center">
                    <div class="row px-3 py-2">
                        <div class="col-6 mt-4 d-flex">
                            <input class="form-control" type="search" placeholder="Search with meeting name" id="search" aria-label="Search">
                          
                        </div>
                        <div class="col-3 mt-4 d-flex">
                            
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Select Filter</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                              </select>
                        </div>
                        <div class="col-3 mt-4">
                        
                            <select class="form-select" aria-label="Default select example">
                                
                            </select>
                        </div>
                    </div>
                      <div class="card-body ">
                          <div class="table-responsive pb-3">
                              <table id="user-table" class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <th>Sr</th>
                                      <th>Meeting Name</th>
                                      <th>Meeting Id</th>
                                      <th>Date</th>
                                      <th>Host Name</th>
                                      <th>Meeting Catogary</th>
                                      <th>Download Report</th>
                                    </tr>
                                  </thead>
                                  <tbody class="article-table">
                                      @foreach($addmeeting as $key=>$meeting)
                                      <tr data-widget="expandable-table" aria-expanded="false">
                                        <td>{{intval($key)+1}}</td>
                                        <td>{{$meeting->meeting_name}}</td>
                                        <td>{{$meeting->meeting_id}}</td>
                                        <td>{{$meeting->meeting_date}}</td>
                                        <td>{{$meeting->host_name}}</td>
                                        <td>{{$meeting->category}}</td>
                                        <td><a data-target-id="{{ $meeting->id }}" data-target-name="{{$meeting->meeting_name}}" data-target-category="{{$meeting->category}}" type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            Download
                                        </a>
                                      </tr>
                                      @endforeach
                                  </tbody>    
                              </table>
                              {{ $addmeeting->links() }}
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
</div>
  <script>
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".article-table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });   

            $(document).ready(function () {
                $("#exampleModal").on("show.bs.modal", function (e) {
                    var id = $(e.relatedTarget).data('target-id');
                    var name = $(e.relatedTarget).data('target-name');
                    var category = $(e.relatedTarget).data('target-category');
                    $('#id_meeting').text(id);
                    $('#exampleModalLabel').text(name);
                    $('#category').text(category);
                });
            });


</script>
@endsection