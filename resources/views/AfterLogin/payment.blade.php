@extends('layouts.section.dashboards')
@section('content')


<div class="content-wrapper mt-5 py-4">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Drivers Payment</h1>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
						<input type="text" id="search-input" class="form-control" placeholder="Search...">
                            <div class="input-group-append">
                                <div class="btn btn-success">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="mailbox-controls">
                       
                        
                            <div class="table-responsive mailbox-messages">
							<table id="search-table" class="table table-hover table-striped" >
                                 <thead>
                         <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 40px;" aria-sort="ascending" aria-label=": activate to sort column descending"></th>
                                                    <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 85.5469px;" aria-label="Transaction ID: activate to sort column ascending">Transaction ID</th>
                                                    <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 43.0938px;" aria-label="Name: activate to sort column ascending">Name</th>
                                                    <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 33.6094px;" aria-label="Date: activate to sort column ascending">Date</th>
                                                    <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 57.5781px;" aria-label="Amount: activate to sort column ascending">Amount</th>
                                                    <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 45.7812px;" aria-label="Status: activate to sort column ascending">Status</th>
                                                    <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 90.2656px;" aria-label="Commission: activate to sort column ascending">Commission</th>
											</thead>
											<tbody>
											<tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#7234486</td>
													<td class="center">John Smith</td>
													<td class="center">22 Feb 2018</td>
													<td class="center">$2341</td>
													<td class="center">
														<span class="btn btn-success">Paid</span>
													</td>
													<td class="center">12%</td>
													
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#5645634</td>
													<td class="center">William Miller</td>
													<td class="center">22 March 2018</td>
													<td class="center">$6453</td>
													<td class="center">
														<span class="btn btn-warning">Pending</span>
													</td>
													<td class="center">34%</td>
													
												</tr><tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#6464532</td>
													<td class="center">Daniel Davis</td>
													<td class="center">23 Jan 2018</td>
													<td class="center">$6453</td>
													<td class="center">
														<span class="btn btn-danger">Unpaid</span>
													</td>
													<td class="center">22%</td>
													
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#46342</td>
													<td class="center">Kevin Wilson</td>
													<td class="center">10 Jun 2018</td>
													<td class="center">$345</td>
													<td class="center">
														<span class="btn btn-success">Paid</span>
													</td>
													<td class="center">28%</td>
													
												</tr><tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#54345</td>
													<td class="center">Jason Smith</td>
													<td class="center">02 Feb 2018</td>
													<td class="center">$3453</td>
													<td class="center">
														<span class="btn btn-danger">Unpaid</span>
													</td>
													<td class="center">12%</td>
													
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#7234486</td>
													<td class="center">Ronald Thomas</td>
													<td class="center">24 March 2018</td>
													<td class="center">$1245</td>
													<td class="center">
														<span class="btn btn-warning">Pending</span>
													</td>
													<td class="center">18%</td>
													
												</tr><tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#57352</td>
													<td class="center">Ryan Allen</td>
													<td class="center">12 Dec 2018</td>
													<td class="center">$7854</td>
													<td class="center">
														<span class="btn btn-success">Paid</span>
													</td>
													<td class="center">32%</td>
													
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#453452</td>
													<td class="center">Brandon Hill</td>
													<td class="center">27 Feb 2018</td>
													<td class="center">$463</td>
													<td class="center">
														<span class="btn btn-danger">Unpaid</span>
													</td>
													<td class="center">10%</td>
													
												</tr><tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#4524534</td>
													<td class="center"> William Miller</td>
													<td class="center">22 Feb 2018</td>
													<td class="center">$2341</td>
													<td class="center">
														<span class="btn btn-success">Paid</span>
													</td>
													<td class="center">12%</td>
													
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
														<<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">#7234486</td>
													<td class="center">John Smith</td>
													<td class="center">12 Feb 2018</td>
													<td class="center">$3453</td>
													<td class="center">
														<span class="btn btn-danger">Unpaid</span>
													</td>
													<td class="center">32%</td>
													
												</tr>
											</tbody>
							            </table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

<style>
	
</style>
<script>
  // Get the search input element and the table
  const searchInput = document.getElementById('search-input');
  const table = document.getElementById('search-table');

  // Add an event listener to the search input
  searchInput.addEventListener('keyup', function () {
    const searchText = searchInput.value.toLowerCase();

    // Loop through all rows of the table body
    const rows = table.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      const cells = row.getElementsByTagName('td');
      let found = false;

      // Loop through all cells in the row
      for (let j = 0; j < cells.length; j++) {
        const cell = cells[j];
        const cellText = cell.textContent || cell.innerText;

        // Check if the cell text contains the search text
        if (cellText.toLowerCase().indexOf(searchText) > -1) {
          found = true;
          break;
        }
      }

      // Show/hide the row based on search result
      row.style.display = found ? '' : 'none';
    }
  });
</script>


@endsection
