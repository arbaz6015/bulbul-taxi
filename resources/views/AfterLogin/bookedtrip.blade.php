@extends('layouts.section.dashboards')
@section('content')


<div class="content-wrapper mt-5 py-4">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Booked Trips</h1>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                        <input type="text" id="search-input" class="form-control" placeholder="Search...">

                            <div class="input-group-append">
                                <div class="btn btn-success">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
                            <div class="table-responsive mailbox-messages">
                            <table id="search-table" class="table table-hover table-striped" >
                            <thead>
												<tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">#</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip Id: activate to sort column ascending" style="width: 38px;">Trip Id</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 104.156px;">Passenger Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip From: activate to sort column ascending" style="width: 141.062px;">Trip From</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip To: activate to sort column ascending" style="width: 134.531px;">Trip To</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Allocated Driver: activate to sort column ascending" style="width: 97.0156px;">Allocated Driver</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Start Time: activate to sort column ascending" style="width: 69.3125px;">Start Time</th>
                                                    <!-- <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 50.3594px;">Action</th> -->
                                                </tr>
											</thead>
                                            <tbody>
                                                <tr role="row" class="odd">
                                                        <td class="sorting_1">1</td>
                                                        <td>ID234</td>
                                                        <td>John Smith</td>
                                                        <td>34, Alax street</td>
                                                        <td>99 Myrtle Dr.Long Branch</td>
                                                        <td>Kevin Wilson</td>
                                                        <td>22-03-2018 12:34</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr><tr role="row" class="even">
                                                        <td class="sorting_1">2</td>
                                                        <td>ID244</td>
                                                        <td>William Miller</td>
                                                        <td>823 Lincoln Ave.Huntsville</td>
                                                        <td>3 Cedar Swamp Rd. Crown Point</td>
                                                        <td>Daniel Davis</td>
                                                        <td>12-03-2018 12:40</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr><tr role="row" class="odd">
                                                        <td class="sorting_1">3</td>
                                                        <td>ID254</td>
                                                        <td>Daniel Davis</td>
                                                        <td>7578 Vale Ave. Canfield</td>
                                                        <td>619 S. Wayne Ave. Fairport</td>
                                                        <td>Jason Smith</td>
                                                        <td>16-03-2018 11:34</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr><tr role="row" class="even">
                                                        <td class="sorting_1">4</td>
                                                        <td>ID264</td>
                                                        <td>Kevin Wilson</td>
                                                        <td>568 Canal Street Toledo</td>
                                                        <td>892 Myers Ave. Des Moines</td>
                                                        <td>William Miller</td>
                                                        <td>11-03-2018 02:14</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr><tr role="row" class="odd">
                                                        <td class="sorting_1">5</td>
                                                        <td>ID274</td>
                                                        <td>Jason Smith</td>
                                                        <td>114 East Edgewood St.</td>
                                                        <td>7 Glen Ridge Street Fairmont</td>
                                                        <td>Ronald Thomas</td>
                                                        <td>23-02-2018 12:34</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr><tr role="row" class="even">
                                                        <td class="sorting_1">6</td>
                                                        <td>ID284</td>
                                                        <td>Ronald Thomas</td>
                                                        <td>624 Monroe St. Irmo</td>
                                                        <td>8373 S. Santa Clara Drive Mc Lean</td>
                                                        <td>Mary White</td>
                                                        <td>12-03-2018 12:55</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr><tr role="row" class="odd">
                                                        <td class="sorting_1">7</td>
                                                        <td>ID294</td>
                                                        <td>Ryan Allen</td>
                                                        <td>37 La Sierra St. Mount Holly</td>
                                                        <td>425 Sage Street Leesburg</td>
                                                        <td>Nancy Clark</td>
                                                        <td>02-03-2018 07:34</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr><tr role="row" class="even">
                                                        <td class="sorting_1">8</td>
                                                        <td>ID236</td>
                                                        <td>Brandon Hill</td>
                                                        <td>61 East Tallwood Ave. Cumming</td>
                                                        <td>8490 Middle River Ave. Roslindale</td>
                                                        <td>Sarah k</td>
                                                        <td>16-03-2018 10:38</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr><tr role="row" class="odd">
                                                        <td class="sorting_1">9</td>
                                                        <td>ID233</td>
                                                        <td>William Miller</td>
                                                        <td>4 Bedford Dr. Ashland</td>
                                                        <td>17 Lyme Ave. Redford</td>
                                                        <td>Lisa Scott</td>
                                                        <td>09-03-2018 12:54</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr><tr role="row" class="even">
                                                        <td class="sorting_1">10</td>
                                                        <td>ID237</td>
                                                        <td>John Smith</td>
                                                        <td>941 Foster Street Shakopee</td>
                                                        <td>24 Amerige Rd. Utica</td>
                                                        <td>Kevin Wilson</td>
                                                        <td>12-03-2018 12:34</td>
                                                        <!-- <td>
                                                            <a href="{{url('/editdriver')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
                                                        </td> -->
                                                    </tr>
                                                </tbody>
										</table>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                </div>
			</div>
		</div>
	</div>

    <script>
  // Get the search input element and the table
  const searchInput = document.getElementById('search-input');
  const table = document.getElementById('search-table');

  // Add an event listener to the search input
  searchInput.addEventListener('keyup', function () {
    const searchText = searchInput.value.toLowerCase();

    // Loop through all rows of the table body
    const rows = table.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      const cells = row.getElementsByTagName('td');
      let found = false;

      // Loop through all cells in the row
      for (let j = 0; j < cells.length; j++) {
        const cell = cells[j];
        const cellText = cell.textContent || cell.innerText;

        // Check if the cell text contains the search text
        if (cellText.toLowerCase().indexOf(searchText) > -1) {
          found = true;
          break;
        }
      }

      // Show/hide the row based on search result
      row.style.display = found ? '' : 'none';
    }
  });
</script>


@endsection
