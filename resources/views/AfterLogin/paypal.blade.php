@extends('layouts.section.dashboards')
@section('content')
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<div class="content-wrapper mt-5 py-4">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Payment Configuration </h1>
                     </div>
                       <form>
                         <div class="row px-3 ">
                          <div class="name">
                          <div class="col-sm-6 ">
                            <!-- paypal selecter    -->
                     <a href="#submenu2" data-bs-toggle="collapse" class="nav-link px-0   ">
                      
                            <i class="fs-4 bi-bootstrap"></i> <span class="ms-1 d-none d-sm-inline my-5 " style="font-size: 20px; "><img src="{{asset('/image/paypal.png')}}" alt="" width="30px" style="border-radius: 10px; ">Paypal </span>
                            <!-- Toggle button  -->
                            <div class="fa fa-chevron-left rotate" style="margin-left: 150px;"></div>
                         <!-- <span class="icon material-symbols-outlined " >
                         expand_more
                         </span> -->
                            </a>
                            <div class="my-3">&nbsp;Off/On  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sandbox/Live</div>
                        <div>
                        <label class="switch " >
                       <input type="checkbox" id="toggle">
                       <span class="slider round"></span>
                       </label>
                       <label class="switch " style="margin-left: 30px;">
                       <input type="checkbox" id="toggle">
                       <span class="slider round"></span>
                   </label>
                </div>

            <!-- End toggle button  -->
                        <ul class="collapse nav flex-column ms-1" id="submenu2" data-bs-parent="#menu">
                            <li class="w-100 my-2">
                              <input class="form-control in1"  type="text" placeholder="Client key">
                           <li class="w-100 my-2">
                              <input class="form-control in1" type="text" placeholder="Secret key">
                            </li>
                                </div>

                                <br>
                              <!-- end paypal selectre  -->
                              <!-- Stripe selecter  -->
                              <div class="col-sm-6 mb-2  my-4">
                              <a href="#submenu3" data-bs-toggle="collapse" class="nav-link px-0 align-middle">
                            <i class="fs-4"></i> <span class="ms-1 d-none d-sm-inline" style="font-size: 20px;"><img src="{{asset('/image/paypal2.png')}}" alt="" width="40px" style="border-radius: 10px;">Stripe</span>
                           <!-- Toggle button  -->
                           <div class="fa fa-chevron-left rotate" style="margin-left: 150px;"></div>
                           </a>
                           <div class="my-3">&nbsp;Off/On  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sandbox/Live</div>
                        <div>
                        <label class="switch " >
                       <input type="checkbox" id="toggle">
                       <span class="slider round"></span>
                       </label>
                       <label class="switch " style="margin-left: 30px;">
                       <input type="checkbox" id="toggle">
                       <span class="slider round"></span>
                   </label>
                </div>

            <!-- end toggle button  -->
                            <ul class="collapse nav flex-column ms-1" id="submenu3" data-bs-parent="#menu">
                            <li class="w-100 my-2">
                              <input class="form-control in1" type="text" placeholder="Client key">
                            <li class="w-100 my-2">
                              <input class="form-control in1" type="text" placeholder="Secret key">
                            </li>
                                </div>
                                <br>
                              <!-- end stripe selecter  -->
                              <!-- Authorize .net  -->
                              <div class="col-sm-6 mb-2  my-4">
                                <!-- <div class="input-group"> -->
                               
                                <a href="#submenu1" data-bs-toggle="collapse" class=" px-0 align-middle">
                            <i class="fs-4 "></i> <span class="ms-1 d-none d-sm-inline" style="font-size: 20px;"><img src="{{asset('/image/paypal3.png')}}" alt="" width="30px" style="border-radius: 10px;">Authorize .Net</span>


                           <!-- Toggle button  -->
                           <div class="fa fa-chevron-left right rotate" style="margin-left: 100px;"></div>
                           </a>
                           <div class="my-3">&nbsp;Off/On  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sandbox/Live</div>
                        <div>
                        <label class="switch " >
                       <input type="checkbox" id="toggle">
                       <span class="slider round"></span>
                       </label>
                       <label class="switch " style="margin-left: 30px;">
                       <input type="checkbox" id="toggle">
                       <span class="slider round"></span>
                   </label>
                </div>

            <!-- end toggle button  -->
                            <ul class="collapse nav flex-column ms-1" id="submenu1" data-bs-parent="#menu">
                            <li class="w-100 my-2">
                              <input class="form-control in1" type="text" placeholder="Client key">
                            <li class="w-100 my-2">
                              <input class="form-control in1" type="text" placeholder="Secret key">
                            </li>
                                </div>
                            <!-- end authorize .net  -->
                           <br>
                        
                  
                 </form>
            </div>
        </div>
    </div>
</section>
</div>




























<!-- CSS  -->

<style>
  .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;



}
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
  border-radius: 20rem;
}
.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
  border-radius: 20rem;
}
input:checked + .slider {
  background-color: #2196F3;
}
input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}
input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

.in1 {
  border: none;
  border-bottom: 1px solid green;
 

}

.container{
  margin-left: 20%;

}
.row>* {
    flex-shrink: 0;
    width: 100%;
    max-width: 100%;
    padding-right: calc(var(--bs-gutter-x)/ 2);
    padding-left: calc(var(--bs-gutter-x)/ 2);
    margin-top: var(--bs-gutter-y);
}


.material-symbols-outlined {
  font-variation-settings:
  'FILL' 0,
  'wght' 400,
  'GRAD' 0,
  'opsz' 48
}
.rotate{
    -moz-transition: all 1s linear;
    -webkit-transition: all 1s linear;
    transition: all 1s linear;
    right: 2.2rem;
    /* margin-left: 100px; */
}

.rotate.down{
    -moz-transform:rotate(-90deg);
    -webkit-transform:rotate(-90deg);
    transform:rotate(-90deg);
}

</style>


@endsection
