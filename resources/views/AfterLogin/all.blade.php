@extends('layouts.section.dashboards')
@section('content')


<div class="content-wrapper mt-5 py-4">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Fare List</h1>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
						<input type="text" id="search-input" class="form-control" placeholder="Search...">

						<!-- <input type="text" id="myInput" class="form-control" placeholder="Search" onkeyup="searchfun()"> -->
                            <div class="input-group-append">
                                <div class="btn btn-success">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                            <div class="table-responsive mailbox-messages">
                            <table id="search-table" class="table table-hover table-striped">
								<!-- <pre>
                                {{print_r($fares)}}
								</pre> -->
                            <thead>
											<tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 40px;" aria-sort="ascending" aria-label="#: activate to sort column descending">#</th>
                                                <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 53.3594px;" aria-label=" Vehicle Type : activate to sort column ascending"> Vehicle Type </th>
                                                <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 31.2188px;" aria-label=" Fare Per KM : activate to sort column ascending"> Fare Per KM </th>
                                                <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 68.2344px;" aria-label=" Minimum Fare : activate to sort column ascending"> Minimum Fare </th>
												<th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 68.2344px;" aria-label=" Minimum Distance: activate to sort column ascending"> Minimum Distance</th>
                                                <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 55.7969px;" aria-label=" Waiting Fare: activate to sort column ascending"> Waiting Fare</th>
												<th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 45.9688px;" aria-label=" Action : activate to sort column ascending"> Action </th>
											</tr>
											</thead>
                                            <tbody>
												@foreach($fares as $fares)
											<tr  role="row">
													<td class="center sorting_1">{{$fares->id}}</td>
													<td class="center">{{$fares->vehicle_type}}</td>
													<td class="center">{{$fares->fare_per_km}}</td>
													<!-- <td class="center">{{$fares->fare_per_minute}}</td> -->
													<td class="center">{{$fares->minimun_fare	}}</td>
													<td class="center">{{$fares->minimun_distance}}</td>
													<td class="center">{{$fares->waiting_fare}}</td>
													<td class="center">
													<a href="{{ url('fare/edit/'.$fares->id) }}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i></a>

														<a href="{{ url('fare/delete-data/'.$fares->id) }}" class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td>
											</tr>
											@endforeach
												
                                            </tbody>
										</table>
                                    </div>
                              </div>
                        </div>
                   </div>
             </section>
        </div>



		<script>
  // Get the search input element and the table
  const searchInput = document.getElementById('search-input');
  const table = document.getElementById('search-table');

  // Add an event listener to the search input
  searchInput.addEventListener('keyup', function () {
    const searchText = searchInput.value.toLowerCase();

    // Loop through all rows of the table body
    const rows = table.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      const cells = row.getElementsByTagName('td');
      let found = false;

      // Loop through all cells in the row
      for (let j = 0; j < cells.length; j++) {
        const cell = cells[j];
        const cellText = cell.textContent || cell.innerText;

        // Check if the cell text contains the search text
        if (cellText.toLowerCase().indexOf(searchText) > -1) {
          found = true;
          break;
        }
      }

      // Show/hide the row based on search result
      row.style.display = found ? '' : 'none';
    }
  });
</script>



@endsection
