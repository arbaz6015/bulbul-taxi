@extends('layouts.section.dashboards')
@section('content')

 <div class="content-wrapper mt-5 py-4">
    
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">All Passenger</h1>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
						<input type="text" id="search-input" class="form-control" placeholder="Search...">

                            <div class="input-group-append">
                                <div class="btn btn-success">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <div class="table-responsive mailbox-messages">
    <table id="search-table" class="table table-hover table-striped" >
    <thead>
												<tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">#</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip Id: activate to sort column ascending" style="width: 29.9844px;">Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Driver Name: activate to sort column ascending" style="width: 47.0469px;">Mobile</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.7188px;">Email</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip From: activate to sort column ascending" style="width: 81.375px;">Address</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip To: activate to sort column ascending" style="width: 55.6406px;">Wallet Balance</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Start Time: activate to sort column ascending" style="width: 38.6094px;">Trips</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="End Time: activate to sort column ascending" style="width: 38.5625px;">Status
                                                </th>
                                                    <!-- <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Distance: activate to sort column ascending" style="width: 68.4062px;">Action</th> -->
											</thead>
											<tbody>
											<tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">Rajesh</td>
													<td class="center"><a href="tel:4444565756">
															4444565756 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															rajesh@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$22</td>
													<td class="center">22</td>
													<td>
														<span class="btn btn-success">Active</span>
													</td>
													<!-- <td class="center">
													<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a href="{{ url('passenger/delete-data/') }}" class="btn btn-warning btn-xs ">
											<i class="far fa-trash-alt"></i>
													</td> -->
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">Pooja Patel</td>
													<td class="center"><a href="tel:444786876">
															444786876 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															pooja@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$27</td>
													<td class="center">15</td>
													<td>
														<span class="btn btn-warning">Deactive
														</span>
													</td>
													<!-- <td class="center">
														<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td> -->
												</tr><tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
                                                    <img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">Sarah Smith</td>
													<td class="center"><a href="tel:44455546456">
															44455546456 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															sarah@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$05</td>
													<td class="center">05</td>
													<td>
														<span class="btn btn-success">Active
														</span>
													</td>
													<!-- <td class="center">
														<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td> -->
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">John Deo</td>
													<td class="center"><a href="tel:444543564">
															444543564 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															john@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$15</td>
													<td class="center">15</td>
										             <td><span class="btn btn-warning">Deactive
														</span>
													</td>
													<!-- <td class="center">
														<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td> -->
												</tr><tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
                                                    <img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">Jay Soni</td>
													<td class="center"><a href="tel:444543564">
															444543564 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															kenh@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$12</td>
													<td class="center">24</td>
													<td>
														<span class="btn btn-warning">Deactive
														</span>
													</td>
													<!-- <td class="center">
														<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td> -->
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
                                                    <img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">Jacob Ryan</td>
													<td class="center"><a href="tel:444543564">
															444543564 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															johnson@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$03</td>
													<td class="center">13</td>
													<td>
														<span class=" btn btn-success">Active
														</span>
													</td>
													<!-- <td class="center">
														<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td> -->
												</tr><tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">Megha Trivedi</td>
													<td class="center"><a href="tel:444543564">
															444543564 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															megha@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$17</td>
													<td class="center">05</td>
													<td>
														<span class="btn btn-warning">Deactive
														</span>
													</td>
													<!-- <td class="center">
														<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td> -->
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">Rajesh</td>
													<td class="center"><a href="tel:4444565756">
															4444565756 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															rajesh@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$22</td>
													<td class="center">17</td>
													<td>
														<span class="btn btn-success">Active
														</span>
													</td>
													<!-- <td class="center">
														<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td> -->
												</tr><tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">Pooja Patel</td>
													<td class="center"><a href="tel:444786876">
															444786876 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															pooja@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$27</td>
													<td class="center">05</td>
													<td>
														<span class="btn btn-warning">Deactive
														</span>
													</td>
													<!-- <td class="center">
														<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td> -->
												</tr><tr class="gradeX even" role="row">
													<td class="user-circle-img sorting_1">
														<img src="{{asset('/image/driver.png')}}" alt="" width="70px" style="border-radius: 10px;">
													</td>
													<td class="center">Sarah Smith</td>
													<td class="center"><a href="tel:44455546456">
															44455546456 </a></td>
													<td class="center"><a href="mailto:shuxer@example.com">
															sarah@example.com </a></td>
													<td class="center">22,tilak appt. surat</td>
													<td class="center">$05</td>
													<td class="center">14</td>
													<td>
														<span class="btn btn-warning">Deactive
														</span>
													</td>
													<!-- <td class="center">
														<a href="{{url('/editpassenger')}}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td> -->
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
									

    <!--
    <tr>
    <td>
    <div class="icheck-success">
    <input type="checkbox" value="" id="check1">
    <label for="check1"></label>
    </div>
    </td>
    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
    <td class="mailbox-subject"><b>name of sender</b> - Trying to find a solution to this problem...
    </td>
    <td class="mailbox-attachment"></td>
    <td class="mailbox-date">5 mins ago</td>
    </tr>
    <tr>
    <td>
    <div class="icheck-success">
    <input type="checkbox" value="" id="check2">
    <label for="check2"></label>
    </div>
    </td>
    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
    <td class="mailbox-name"><a href="read-mail.html">maxx Pierce</a></td>
    <td class="mailbox-subject"><b>name of sender</b> - Trying to find a solution to this problem...
    </td>
    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
    <td class="mailbox-date">28 mins ago</td>
    </tr>
    <tr>
    <td>
    <div class="icheck-success">
    <input type="checkbox" value="" id="check3">
    <label for="check3"></label>
    </div>
    </td>
    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
    <td class="mailbox-name"><a href="read-mail.html">jhon Pierce</a></td>
    <td class="mailbox-subject"><b>name of sender</b> - Trying to find a solution to this problem...
    </td>
    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
    <td class="mailbox-date">11 hours ago</td>
    </tr>
    <tr>
    <td>
    <div class="icheck-success">
    <input type="checkbox" value="" id="check4">
    <label for="check4"></label>
    </div>
    </td>
    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
    <td class="mailbox-subject"><b>name of sender</b> - Trying to find a solution to this problem...
    </td>
    <td class="mailbox-attachment"></td>
    <td class="mailbox-date">15 hours ago</td>
    </tr>
    <tr>
    <td>
    <div class="icheck-success">
    <input type="checkbox" value="" id="check5">
    <label for="check5"></label>
    </div>
    </td>
    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
    <td class="mailbox-subject"><b>name of sender</b> - Trying to find a solution to this problem...
    </td>
    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
    <td class="mailbox-date">Yesterday</td>
    </tr>
    <tr>
    <td>
    <div class="icheck-success">
    <input type="checkbox" value="" id="check6">
    <label for="check6"></label>
    </div>
    </td>
    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
    <td class="mailbox-subject"><b>name of sender</b> - Trying to find a solution to this problem...
    </td>
    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
    <td class="mailbox-date">2 days ago</td>
    </tr>
    <tr>
    <td>
    <div class="icheck-success">
    <input type="checkbox" value="" id="check7">
    <label for="check7"></label>
    </div>
    </td>
    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
    <td class="mailbox-subject"><b>name of sender</b> - Trying to find a solution to this problem...
    </td>
    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
    <td class="mailbox-date">2 days ago</td>
    </tr>
    <tr>
    <td>
    <div class="icheck-success">
    <input type="checkbox" value="" id="check11">
    <label for="check11"></label>
    </div>
    </td>
    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
    <td class="mailbox-subject"><b>name of sender</b> - Trying to find a solution to this problem...
    </td>
    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
    <td class="mailbox-date">4 days ago</td>
    </tr>
     -->
    </tbody>
    </table>
    
    </div>
    
    </div>
    
    
    </div>
    </div>
    
    </div>
    
    </div>
    
    </section>
	<script>
  // Get the search input element and the table
  const searchInput = document.getElementById('search-input');
  const table = document.getElementById('search-table');

  // Add an event listener to the search input
  searchInput.addEventListener('keyup', function () {
    const searchText = searchInput.value.toLowerCase();

    // Loop through all rows of the table body
    const rows = table.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      const cells = row.getElementsByTagName('td');
      let found = false;

      // Loop through all cells in the row
      for (let j = 0; j < cells.length; j++) {
        const cell = cells[j];
        const cellText = cell.textContent || cell.innerText;

        // Check if the cell text contains the search text
        if (cellText.toLowerCase().indexOf(searchText) > -1) {
          found = true;
          break;
        }
      }

      // Show/hide the row based on search result
      row.style.display = found ? '' : 'none';
    }
  });
</script>

    @endsection