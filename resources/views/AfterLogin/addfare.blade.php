@extends('layouts.section.dashboards')
@section('content')

<div class="content-wrapper mt-5 py-4">

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Add Fare</h1>
                </div>
<form action="{!! route('fare.store') !!}" method="POST">
  @csrf
                <div class="row px-3 my-4">
                               
                               <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Vehicle Types</p>
                                   <!-- <label for="cars">Choose Vehicle Type</label> -->
                                   <select id="cars" class="form-control in1" name="vehicle_type">
                                   <option value="Vehicle">Select Vehicle Type</option>
                                    <option value="suv">Large SUV</option>
                                    <option value="Sedan">Sedan</option>
                                    <option value="van">Mid Size SUV</option>
                                    <option value="wagon">Mini SUV</option>
                                    <option value="wagon">Hatchback</option>
                                    <option value="wagon">Convertible</option>
                                    <option value="wagon">Coupe</option>
                                    <option value="wagon">Sports Car</option>
                                    <option value="wagon">Mini Van</option>
                                    <option value="wagon">Hatchback</option>
                                   </select>
                           
                               </div>
                              
                               <div class="col-sm-6 mb-2 my-4">
                                  <p class="mb-2 f-w-600">Fare Per KM (in $)</p>
                                  <input class="form-control in1" type="text" name="fare_per_km" id="fare_per_km">
                               </div>      
                               <div class="col-sm-6 mb-2  my-4">
                                   <p class="mb-2 f-w-600"> Fare per minute (in $)</p>
                                   <input class="form-control in1" type="text" name="fare_per_minute" id="fare_per_minute" >
                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->number }}</h6> -->
                               </div>                        
                              
                               <div class="col-sm-6 mb-2  my-4">
                                   <p class="mb-2 f-w-600">Minimun Fare (in $)</p>
                                   <input class="form-control in1" type="text" name="minimun_fare" id="minimun_fare" >
                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->number }}</h6> -->
                               </div>
                               <div class="col-sm-6 mb-2 my-4 ">
                                   <p class="mb-2 f-w-600">Minimun Distance (in KM)</p>
                                   <input class="form-control in1" type="text" name="minimun_distance" id="minimun_distance">
                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                               </div>

                               <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Waiting Fare (in $)</p>
                                   <input class="form-control in1" type="text" name="waiting_fare" id="waiting_fare" >
                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                               </div>
                              
                             </div>
                           <br>
                        <div class="col-lg-12 p-t-20 text-center">
                          <input type="submit" value="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 my-3 btn btn-info" data-upgraded=",MaterialButton,MaterialRipple">
									
									
									</div>                         
                   </form>
            </div>
        </div>
    </div>
</section>
</div>







<!-- css add  -->




  <style>
.name h3 {
  font-family: "Trirong", serif;
}

.name2 p {
  font-family: "Trirong", serif;
}
.in1 {
  border: none;
  border-bottom: 1px solid green;
 

}

/* .container{
  margin-left: 20%;

} */
.row>* {
    flex-shrink: 0;
    width: 100%;
    max-width: 100%;
    padding-right: calc(var(--bs-gutter-x)/ 2);
    padding-left: calc(var(--bs-gutter-x)/ 2);
    margin-top: var(--bs-gutter-y);
}


</style>
@endsection


















