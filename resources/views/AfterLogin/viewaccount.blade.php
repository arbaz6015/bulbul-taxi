@extends('layouts.section.dashboards')
@section('content')


<div class="content-wrapper mt-5 py-4">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">View Account Details</h1>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
						<input type="text" class="form-control" placeholder="Search" >
                            <div class="input-group-append">
                                <div class="btn btn-success">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                            <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped">
                            <thead>
											<tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 40px;" aria-sort="ascending" aria-label="#: activate to sort column descending">#</th>
                                                <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 53.3594px;" aria-label=" Vehicle Type : activate to sort column ascending"> Bank's Name </th>
                                                <!-- <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 31.2188px;" aria-label=" Fare Per KM : activate to sort column ascending"> Name Of The Account Holder </th> -->
                                                <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 68.2344px;" aria-label=" Minimum Fare : activate to sort column ascending"> Account Number </th>
                                                <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 55.7969px;" aria-label=" Waiting Fare: activate to sort column ascending">Routing Code</th>
                                                <!-- <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 45.9688px;" aria-label=" Action : activate to sort column ascending"> Account Holder Number </th> -->
                                                <th class="center sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" style="width: 45.9688px;" aria-label=" Action : activate to sort column ascending"> Action </th>                                           
                                            </tr>
											</thead>
                                            <tbody>
											@foreach($driver_accounts as $driver_accounts)
											<tr class="gradeX odd" role="row">
													<td class="user-circle-img sorting_1">
													{{$driver_accounts->id}}
													</td>
													<td class="center">
													{{$driver_accounts->bank_name}}
													</td>
													<td class="center">
													{{$driver_accounts->account_number}}
												</td>
													<td class="center">
													{{$driver_accounts->routing_number}}						
														</td>													
													<td class="center">
														<a href="{{ url('account/accountedit/'.$driver_accounts->id) }}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i>
														</a>
														<a href="{{ url('account/delete-data/'.$driver_accounts->id) }}" class="btn btn-warning btn-xs">
                                                        <i class="far fa-trash-alt"></i>
														</a>
													</td>
												</tr>
												@endforeach										
                                            </tbody>
										</table>
                                    </div>
                              </div>
                        </div>
                   </div>
             </section>
        </div>






@endsection
