@extends('layouts.section.dashboards')
@section('content')


<div class="content-wrapper mt-5 py-4">

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h1 class="card-title">Edit Vehicle</h1>
                </div>
                <form action="{!! url('addvehicle/update-data') !!}" method="POST" enctype="multipart/form-data">
                       @csrf
                       <input type="hidden" value="{{$vehicles->id}}" name="edit"/>

                <div class="row px-3 my-4">
                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Driver Name</p>
                                   <!-- <input class="form-control in1"  type="text" name="vehicle_license_plate_no" id="vehicle_license_plate_no"> -->
                                   <input class="form-control in1"  type="text" value="{{$vehicles->name}}" name="name" id="name">

                                    <!-- <label for="cars">Choose Vehicle Type</label> -->
                                   <!-- <select id="cars" class="form-control in1" value="{{$vehicles->name}}" name="name">
                                    <option value="Vehicle">Select Drivers Name</option>
                                    <option value="suv">Arbaz kahn</option>
                                    <option value="Sedan">Ram </option>
                                    <option value="van">Johan</option>
                                    <option value="wagon">Wangon</option>
                                   </select> -->
                               </div>
                               <div class="col-sm-6 mb-2 my-4 name2">
                                   <p class="mb-2 f-w-600">Vehicle License Plate no..</p>
                                   <input class="form-control in1"  type="text" value="{{$vehicles->vehicle_license_plate_no}}" name="vehicle_license_plate_no" id="vehicle_license_plate_no">
                                </div>
                                <div class="col-sm-6 mb-2 my-4 name2">
                                   <p class="mb-2 f-w-600">Engine Number</p>
                                   <input class="form-control in1"  type="text" value="{{$vehicles->engine_number}}" name="engine_number" id="engine_number">
                                </div>
                                <div class="col-sm-6 mb-2 my-4 name2">
                                   <p class="mb-2 f-w-600">Chasis Number</p>
                                   <input class="form-control in1"  type="text" value="{{$vehicles->chasis_number}}" name="chasis_number" id="chasis_number">
                                </div>
                                <!-- <div class="col-sm-6 mb-2 my-4 name2 ">
                                   <p class="mb-2 f-w-600">vehicle_manufacturer	</p>
                                   <input class="form-control in1"  type="text" name="vehicle_manufacturer" id="vehicle_manufacturer">
                                </div> -->

                            

                               <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Vehicle Types</p>
                                   <!-- <label for="cars">Choose Vehicle Type</label> -->
                                   <select id="cars" class="form-control in1" value="{{$vehicles->vehicle_type}}" name="vehicle_type">
                                    <option value="Vehicle">Select Vehicle Type</option>
                                    <option value="Large SUV">Large SUV</option>
                                    <option value="Sedan">Sedan</option>
                                    <option value="Mid Size SUV">Mid Size SUV</option>
                                    <option value="Mini SUV">Mini SUV</option>
                                    <option value="Hatchback">Hatchback</option>
                                    <option value="Convertible">Convertible</option>
                                    <option value="Coupe">Coupe</option>
                                    <option value="Sports Car">Sports Car</option>
                                    <option value="Mini Van">Mini Van</option>
                                   </select>
                               </div>
                               <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Vehicle Model</p>
                                   <input class="form-control in1" type="text" value="{{$vehicles->vehicle_model}}" name="vehicle_model" id="vehicle_model">
                                </div>
                                <div class="col-sm-6 mb-2 my-4">
                                   <p class="mb-2 f-w-600">Seating Capacity</p>
                                   <input class="form-control in1" type="text" value="{{$vehicles->seating_capacity}}" name="seating_capacity" id="seating_capacity">
                                </div>
                               
                                <div class="col-sm-6 mb-2 my-4">
                                    <p class="mb-2 f-w-600">Insurance Renewal Date</p>
                                    <input class="form-control in1" type="date" value="{{$vehicles->insurance_renewal_date}}" name="insurance_renewal_date" id="insurance_renewal_date">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2  my-4">
                                    <p class="mb-2 f-w-600">Tax Renewal Date</p>
                                    <input class="form-control in1" type="date" value="{{$vehicles->tax_submission_date}}" name="tax_submission_date" id="tax_submission_date">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                              
                            
                                <div class="col-sm-6 mb-2 my-4 ">
                                    <p class="mb-2 f-w-600">Upload Vehicle Photo</p>
                                    <img id="blah" class="img-responsive" src="{{ asset('storage/images/driver/'.$vehicles->vehicle_photos) }}" alt="{{$vehicles->vehicle_photos}}" width="326rem" >
                                    <!-- <img id="blah" src="http://placehold.it/180" value="{{$vehicles->vehicle_photos}}" name="vehicle_photos" id="vehicle_photos" alt="your image" /> -->
                                    <input class="form-control my-2  " type='file' value="{{$vehicles->vehicle_photos}}" name="vehicle_photos" id="vehicle_photos" onchange="readURL(this);"  />
                                   
                                </div>

                                <!-- <div class="col-sm-6 mb-2  ">
                                    <p class="mb-2 f-w-600">Upload Vehicle PUC Photo</p>
                                    <img id="blah2" src="http://placehold.it/180" alt="your image" />
                                    <input class="form-control my-2  " type='file' value="{{$vehicles->puc_photo}}" name="puc_photo" id="puc_photo" onchange="readURL(this);"  />
                                   
                                </div> -->
                                <!-- <div class="col-sm-6 mb-2 my-4 ">
                                    <p class="mb-2 f-w-600">Upload Vehicle RC Photo</p>
                                    <img id="blah" src="http://placehold.it/180" alt="your image" />
                                    <input class="form-control my-2  " type='file' value="{{$vehicles->rc_photo}}" name="rc_photo" id="rc_photo" onchange="readURL(this);"  />
                                   
\                                </div> -->
                                <!-- <div class="col-sm-6 mb-2  my-4">
                                
                                <p class="mb-2 f-w-600">Upload Vehicle Insurance Photo</p>
                                <img id="blah" src="http://placehold.it/180" alt="your image" />
                                    <input class="form-control my-2  " type='file' value="{{$vehicles->insurance_photo}}" name="insurance_photo" id="insurance_photo" onchange="readURL(this);"  />
                                   
                                </div> -->
                            </div>
                           <br>
                        <div class="col-lg-12 p-t-20 text-center">
                        <input type="submit" value="Update" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 my-3 btn btn-info" data-upgraded=",MaterialButton,MaterialRipple">

						<!-- <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 my-3 btn btn-info" data-upgraded=",MaterialButton,MaterialRipple">Submit<span class="mdl-button__ripple-container"><span class="mdl-ripple is-animating"></span></span></button> -->
						</div>
                   </form>
            </div>
        </div>
    </div>
</section>
</div>










<!-- css add  -->




  <style>
.name h3 {
  font-family: "Trirong", serif;
}

.name2 p {
  font-family: "Trirong", serif;
}
.in1 {
  border: none;
  border-bottom: 1px solid green;


}

.container{
  margin-left: 20%;

}
.row>* {
    flex-shrink: 0;
    width: 100%;
    max-width: 100%;
    padding-right: calc(var(--bs-gutter-x)/ 2);
    padding-left: calc(var(--bs-gutter-x)/ 2);
    margin-top: var(--bs-gutter-y);
}
img{
  max-width:180px;
}


</style>
<script>
         function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
       
               
</script>
@endsection
