@php
$roleId = isset($role->id)?($role->id):0;
@endphp
@extends('layouts.section.dashboards')
@if(isset(($errors->all())[0]))
@php
toastr()->warning(($errors->all())[0]);
@endphp
@endif
@section('content')



			<!-- Content Wrapper. Contains page content --> <div class="content-wrapper mt-5 py-4">

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h3 class="card-title">Active Trips</h3>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-append">
                                <div class="btn btn-success">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
	
    <div class="table-responsive mailbox-messages">
    <table class="table table-hover table-striped">

                                                        <!-- <table id="tableExport" class="display dataTable no-footer" role="grid" aria-describedby="tableExport_info"> -->
											<thead>
												<tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">#</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip Id: activate to sort column ascending" style="width: 29.9062px;">Trip Id</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Driver Name: activate to sort column ascending" style="width: 46.8125px;">Driver Name</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.4844px;">Passenger Name</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip From: activate to sort column ascending" style="width: 80.6406px;">Trip From</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip To: activate to sort column ascending" style="width: 54.8125px;">Trip To</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Start Time: activate to sort column ascending" style="width: 38.4219px;">Start Time</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="View Route: activate to sort column ascending" style="width: 44.7812px;">View Route</th></tr>
											</thead>


											<tbody																																																																																																	<tr role="row" class="odd">
													<td class="sorting_1">1</td>
													<td>ID234</td>
													<td>John Smith</td>
													<td>Kevin Wilson</td>
													<td>34, Alax street</td>
													<td>99 Myrtle Dr.Long Branch</td>
													<td>12:34</td>
													<td>
														<a href="https://www.google.com/maps/@25.0994627,85.3102109,15z" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">2</td>
													<td>ID244</td>
													<td>William Miller</td>
													<td>Daniel Davis</td>
													<td>823 Lincoln Ave.Huntsville</td>
													<td>3 Cedar Swamp Rd. Crown Point</td>
													<td>12:34</td>
													<td>

														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
															
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">3</td>
													<td>ID254</td>
													<td>Daniel Davis</td>
													<td>Jason Smith</td>
													<td>7578 Vale Ave. Canfield</td>
													<td>619 S. Wayne Ave. Fairport</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">4</td>
													<td>ID264</td>
													<td>Kevin Wilson</td>
													<td>William Miller</td>
													<td>568 Canal Street Toledo</td>
													<td>892 Myers Ave. Des Moines</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">5</td>
													<td>ID274</td>
													<td>Jason Smith</td>
													<td>Ronald Thomas</td>
													<td>114 East Edgewood St. Lake Charles</td>
													<td>7 Glen Ridge Street Fairmont</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">6</td>
													<td>ID284</td>
													<td>Ronald Thomas</td>
													<td>Mary White</td>
													<td>624 Monroe St. Irmo</td>
													<td>8373 S. Santa Clara Drive Mc Lean</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">7</td>
													<td>ID294</td>
													<td>Ryan Allen</td>
													<td>Nancy Clark</td>
													<td>37 La Sierra St. Mount Holly</td>
													<td>425 Sage Street Leesburg</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">8</td>
													<td>ID236</td>
													<td>Brandon Hill</td>
													<td>Sarah k</td>
													<td>61 East Tallwood Ave. Cumming</td>
													<td>8490 Middle River Ave. Roslindale</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">9</td>
													<td>ID233</td>
													<td>William Miller</td>
													<td>Lisa Scott</td>
													<td>4 Bedford Dr. Ashland</td>
													<td>17 Lyme Ave. Redford</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</table>
											</div>
										</div>
									</div>
									<!-- /.container-fluid -->
								</section>
								<!-- /.content -->
							</div>

<!-- <div class="content-wrapper mt-5" style="min-height: 0px;">
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5 class="m-0" style="color:green">Active Trips</h5>

</div> -->

			<!-- end sidebar menu -->
			<!-- start page content -->
			<!-- <div class="page-content-wrapper">
				<div class="page-content" style="min-height:453.9062px">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left"> -->

							<!-- <ol class="breadcrumb  pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="">Trips</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Active Trips</li>
							</ol>
						</div>
					</div> -->
					<!-- <div class="row">
						<div class="col-md-12">
							<div class="card card-box"> -->
								<!-- <div class="card-head">
									<header>Active Trips</header>
									<div class="tools">
										<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a> -->
										<!-- <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a> -->
										<!-- <a class="t-close btn-color fa fa-times" href="javascript:;"></a> -->
									<!-- </div> -->
								<!-- </div> -->
								<!-- <div class="card-body "> -->
									<!-- <div class="table-scrollable"> -->
										<!-- <div id="tableExport_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                            <div class="dt-buttons">      <button class=" btn btn-outline-dark" tabindex="0" aria-controls="tableExport">
                                            <span>Copy</span></button> <button class=" btn btn-outline-success" tabindex="0" aria-controls="tableExport">
                                                <span>Excel</span></button> <button class=" btn btn-outline-info" tabindex="0" aria-controls="tableExport">
                                                    <span>CSV</span></button> <button class="btn btn-outline-danger" tabindex="0" aria-controls="tableExport">
                                                        <span>PDF</span></button> </div><div id="tableExport_filter" class="dataTables_filter">
                                                        <b class="mt-2">Search:</b>
                                                            <label><input type="search" class="form-control my-3" placeholder=""></label>
                                                        </div><table id="tableExport" class="display dataTable no-footer" role="grid" aria-describedby="tableExport_info">
											<thead>
												<tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">#</th>
                                                    <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip Id: activate to sort column ascending" style="width: 29.9062px;">Trip Id</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Driver Name: activate to sort column ascending" style="width: 46.8125px;">Driver Name</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.4844px;">Passenger Name</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip From: activate to sort column ascending" style="width: 80.6406px;">Trip From</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip To: activate to sort column ascending" style="width: 54.8125px;">Trip To</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Start Time: activate to sort column ascending" style="width: 38.4219px;">Start Time</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="View Route: activate to sort column ascending" style="width: 44.7812px;">View Route</th></tr>
											</thead>
											<tbody																																																																																																	<tr role="row" class="odd">
													<td class="sorting_1">1</td>
													<td>ID234</td>
													<td>John Smith</td>
													<td>Kevin Wilson</td>
													<td>34, Alax street</td>
													<td>99 Myrtle Dr.Long Branch</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">2</td>
													<td>ID244</td>
													<td>William Miller</td>
													<td>Daniel Davis</td>
													<td>823 Lincoln Ave.Huntsville</td>
													<td>3 Cedar Swamp Rd. Crown Point</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">3</td>
													<td>ID254</td>
													<td>Daniel Davis</td>
													<td>Jason Smith</td>
													<td>7578 Vale Ave. Canfield</td>
													<td>619 S. Wayne Ave. Fairport</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">4</td>
													<td>ID264</td>
													<td>Kevin Wilson</td>
													<td>William Miller</td>
													<td>568 Canal Street Toledo</td>
													<td>892 Myers Ave. Des Moines</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">5</td>
													<td>ID274</td>
													<td>Jason Smith</td>
													<td>Ronald Thomas</td>
													<td>114 East Edgewood St. Lake Charles</td>
													<td>7 Glen Ridge Street Fairmont</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">6</td>
													<td>ID284</td>
													<td>Ronald Thomas</td>
													<td>Mary White</td>
													<td>624 Monroe St. Irmo</td>
													<td>8373 S. Santa Clara Drive Mc Lean</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">7</td>
													<td>ID294</td>
													<td>Ryan Allen</td>
													<td>Nancy Clark</td>
													<td>37 La Sierra St. Mount Holly</td>
													<td>425 Sage Street Leesburg</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">8</td>
													<td>ID236</td>
													<td>Brandon Hill</td>
													<td>Sarah k</td>
													<td>61 East Tallwood Ave. Cumming</td>
													<td>8490 Middle River Ave. Roslindale</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">9</td>
													<td>ID233</td>
													<td>William Miller</td>
													<td>Lisa Scott</td>
													<td>4 Bedford Dr. Ashland</td>
													<td>17 Lyme Ave. Redford</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">10</td>
													<td>ID237</td>
													<td>John Smith</td>
													<td>Kevin Wilson</td>
													<td>941 Foster Street Shakopee</td>
													<td>24 Amerige Rd. Utica</td>
													<td>12:34</td>
													<td>
														<a href="route_map.html" class="btn btn-tbl-delete btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr></tbody>
										</table><div class="dataTables_info" id="tableExport_info" role="status" aria-live="polite">Showing 1 to 10 of 18 entries</div><div class="dataTables_paginate paging_simple_numbers" id="tableExport_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="tableExport_previous"><a href="#" aria-controls="tableExport" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="tableExport" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="tableExport" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item next" id="tableExport_next"><a href="#" aria-controls="tableExport" data-dt-idx="3" tabindex="0" class="page-link">Next</a></li></ul></div></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> -->
			<!-- end page content -->

		<!-- </div> -->
                <!-- <div class="col-sm-6 text-right">
                </div> -->
            <!-- </div> -->
        <!-- </div> -->
    <!-- </div> -->

<!-- </div> -->
@endsection


























<!-- <form method="POST" action="{{ url('role/store') }}" class="row g-3 py-3 px-4">
                    @csrf
                    <input type="hidden" name="current_role_id" value="{{$roleId}}">
                    <div class="col-md-12">
                        <label class="form-label">Name Of Role:</label>
                        <input type="text" class="form-control" value="{{isset($role->name)?$role->name:''}}" name="name">
                    </div>





                    <div class="col-12">
                        <label class="form-label">Parent Role:</label>
                        <select class="form-select" aria-label="Default select example" name="role_name" value="{{ old('roll_name') }}" autocomplete="roll_name">
                            <option value="">No parent role</option>
                            @foreach($roles as $key=>$roleVal)
                            <option value="{{$roleVal->id}}">{{$roleVal->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 text-right">
                        <button type="submit" class="btn btn-success px-5">Create</button>
                    </div>
                </form> -->
