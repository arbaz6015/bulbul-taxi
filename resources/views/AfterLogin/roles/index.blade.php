@php
$roleId = isset($role->id)?($role->id):0;
@endphp
@extends('layouts.section.dashboards')
@if(isset(($errors->all())[0]))
@php
toastr()->warning(($errors->all())[0]);
@endphp
@endif
@section('content')


<div class="content-wrapper mt-5 py-4">

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success card-outline">
                <div class="card-header">
                    <h3 class="card-title">Completed Trips</h3>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-append">
                                <div class="btn btn-success">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
	
    <div class="table-responsive mailbox-messages">
    <table class="table table-hover table-striped">
           <!-- <table id="tableExport" class="dataTable no-footer" role="grid" aria-describedby="tableExport_info"> -->
											<thead>
												<tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">#</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip Id: activate to sort column ascending" style="width: 29.9844px;">Trip Id</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Driver Name: activate to sort column ascending" style="width: 47.0469px;">Driver Name</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.7188px;">Passenger Name</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip From: activate to sort column ascending" style="width: 81.375px;">Trip From</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip To: activate to sort column ascending" style="width: 55.6406px;">Trip To</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Start Time: activate to sort column ascending" style="width: 38.6094px;">Start Time</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="End Time: activate to sort column ascending" style="width: 38.5625px;">End Time</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Distance: activate to sort column ascending" style="width: 68.4062px;">Distance</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Fare: activate to sort column ascending" style="width: 34.0625px;">Fare</th><th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="View Route: activate to sort column ascending" style="width: 45.0312px;">View Route</th></tr>
											</thead>
											<tbody>
											<tr role="row" class="odd">
													<td class="sorting_1">1</td>
													<td>ID234</td>
													<td>John Smith</td>
													<td>Kevin Wilson</td>
													<td>34, Alax street</td>
													<td>99 Myrtle Dr.Long Branch</td>
													<td>12:34</td>
													<td>01:14</td>
													<td>4.5 KM</td>
													<td>$20</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">2</td>
													<td>ID244</td>
													<td>William Miller</td>
													<td>Daniel Davis</td>
													<td>823 Lincoln Ave.Huntsville</td>
													<td>3 Cedar Swamp Rd. Crown Point</td>
													<td>11:34</td>
													<td>12:14</td>
													<td>7.5 KM</td>
													<td>$40</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">3</td>
													<td>ID254</td>
													<td>Daniel Davis</td>
													<td>Jason Smith</td>
													<td>7578 Vale Ave. Canfield</td>
													<td>619 S. Wayne Ave. Fairport</td>
													<td>07:39</td>
													<td>07:59</td>
													<td>1.5 KM</td>
													<td>$7</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">4</td>
													<td>ID264</td>
													<td>Kevin Wilson</td>
													<td>William Miller</td>
													<td>568 Canal Street Toledo</td>
													<td>892 Myers Ave. Des Moines</td>
													<td>09:44</td>
													<td>10:14</td>
													<td>9.67 KM</td>
													<td>$28</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">5</td>
													<td>ID274</td>
													<td>Jason Smith</td>
													<td>Ronald Thomas</td>
													<td>114 East Edgewood St. Lake Charles</td>
													<td>7 Glen Ridge Street Fairmont</td>
													<td>02:23</td>
													<td>02:54</td>
													<td>5.3 KM</td>
													<td>$10</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">6</td>
													<td>ID284</td>
													<td>Ronald Thomas</td>
													<td>Mary White</td>
													<td>624 Monroe St. Irmo</td>
													<td>8373 S. Santa Clara Drive Mc Lean</td>
													<td>12:34</td>
													<td>01:14</td>
													<td>4.5 KM</td>
													<td>$20</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">7</td>
													<td>ID294</td>
													<td>Ryan Allen</td>
													<td>Nancy Clark</td>
													<td>37 La Sierra St. Mount Holly</td>
													<td>425 Sage Street Leesburg</td>
													<td>10:24</td>
													<td>11:54</td>
													<td>11.5 KM</td>
													<td>$60</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">8</td>
													<td>ID236</td>
													<td>Brandon Hill</td>
													<td>Sarah k</td>
													<td>61 East Tallwood Ave. Cumming</td>
													<td>8490 Middle River Ave. Roslindale</td>
													<td>04:00</td>
													<td>05:14</td>
													<td>7.5 KM</td>
													<td>$30</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="odd">
													<td class="sorting_1">9</td>
													<td>ID233</td>
													<td>William Miller</td>
													<td>Lisa Scott</td>
													<td>4 Bedford Dr. Ashland</td>
													<td>17 Lyme Ave. Redford</td>
													<td>11:34</td>
													<td>12:14</td>
													<td>8.5 KM</td>
													<td>$43</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr><tr role="row" class="even">
													<td class="sorting_1">10</td>
													<td>ID237</td>
													<td>John Smith</td>
													<td>Kevin Wilson</td>
													<td>941 Foster Street Shakopee</td>
													<td>24 Amerige Rd. Utica</td>
													<td>12:34</td>
													<td>01:14</td>
													<td>4.5 KM</td>
													<td>$20</td>
													<td>
														<a href="route_map.html" class="btn btn-warning btn-xs">
															<i class="fa fa-map-marker"></i>
														</a>
													</td>
												</tr>
											</tbody>
										</table>	
									</div>
								</div>
							</div>
						</section>				
				    </div>
			<!-- end page content -->
@endsection
