@php
use App\Models\Role;
$roles = Role::orderBy('name', 'asc')->get();
@endphp

@extends('layouts.section.dashboards')
@section('content')

<div class="content-wrapper mt-5">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5 class="m-0">Dashboard /Edit Partners</h5>
                </div>
                <div class="col-sm-6 text-right">
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid mb-5">
            <div class="card border border-success">
                <form method="POST" action="{{ url('user/update-user') }}" class="row g-3 py-3 px-4">
                    @csrf
                    <input type="hidden" name="user_id" value="{{isset($user->id)?$user->id:''}}">
                    <div class="col-md-12">
                        <label class="form-label">Name:</label>
                        <input type="text" class="form-control" value="{{isset($user->name)?$user->name:''}}" name="name">
                    </div>

                    <div class="col-md-12">
                        <label class="form-label">Email:</label>
                        <input type="email" class="form-control" name="email" value="{{isset($user->email)?$user->email:''}}">
                    </div>

                    <div class="col-md-12">
                        <label class="form-label">Employee ID:</label>
                        <input type="text" class="form-control" name="employee_id" value="{{isset($user->employee_id)?$user->employee_id:''}}">
                    </div>

                    <div class="col-12">
                        <label class="form-label">Role:</label>
                        <select class="form-select" aria-label="Default select example" name="role_name" value="{{ old('roll_name') }}" autocomplete="roll_name">
                            <option value="">Select Role</option>
                            @foreach($roles as $key=>$role)
                            @if($role->name != 'Admin')
                            <option value="{{$role->name}}" {{((isset((($user->getRoleNames())[0]))?(($user->getRoleNames())[0]):'') == $role->name)?'selected':''}}>{{$role->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12">
                        <label class="form-label">Password</label>
                        <input type="text" class="form-control" placeholder="" name="password">
                    </div>
                    <div class="col-12">
                        <label class="form-label">Phone:</label>
                        <input type="text" class="form-control" name="number" value="{{isset($user->number)?$user->number:''}}">
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-success px-5">Updated</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection