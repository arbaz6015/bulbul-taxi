@extends('layouts.section.dashboards')
@section('content')
@php
$quickCheck = $category == 'QuickCheck' ? "selected" : "";
$consultation = $category == 'Consultation' ? "selected" : "";
$others = $category == 'Others' ? "selected" : "";
@endphp
<div class="content-wrapper mt-5 py-3">
    <!-- Content Header (Page header) -->
    <!-- /.container-fluid -->
    <!-- /.content-header -->
    <!-- /.row (main row) -->
    <section class="content">
        <div class="container-fluid mb-5">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Meetings</span>
                            <span class="info-box-number">
                                @if(isset($meeting) && !empty($meeting))
                                {{count($meeting)}}
                                @else
                                0
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card text-center">
                        @include('AfterLogin.filter')
                        <div class="card-body ">
                            <div class="table-responsive pb-3">
                                <table id="user-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sr</th>
                                            <th>Meeting Name</th>
                                            <th>Meeting Id</th>
                                            <th>Date</th>
                                            <th>Host Name</th>
                                            <th>Meeting Catogary</th>
                                            <th>Attend Partners</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="meeting-table">
                                        @foreach($addmeeting as $key=>$meeting)
                                        @php
                                        $userCollection = array();
                                        if (isset($meeting->meeting)){
                                        foreach ($meeting->meeting as $userVal){
                                        if (!in_array((($userVal['name']) ? $userVal['name'] : ""), $userCollection)) {
                                        array_push($userCollection, (($userVal['name']) ? $userVal['name'] : ""));
                                        }
                                        }
                                        }
                                        $userCollection = array_filter($userCollection);
                                        $userName = implode(", ", $userCollection);
                                        @endphp
                                        <tr data-widget="expandable-table" aria-expanded="false">
                                            <td>{{intval($key)+1}}</td>
                                            <td>{{$meeting->meeting_name}}</td>
                                            <td>{{$meeting->meeting_id}}</td>
                                            <td>{{$meeting->meeting_date}}</td>
                                            <td>{{isset($meeting->users->name)?$meeting->users->name:'NA'}}</td>
                                            <td>{{$meeting->category}}</td>
                                            <td>{{(isset($userName) && !empty($userName))?$userName:'NA'}}</td>
                                            <td class="text-success fw-700"><select data-meeting-id="{{($meeting->id)?$meeting->id:0}}" class="form-select meeting-status" aria-label="Default select example">
                                                    <option value="0" {{$meeting->status == 0?'selected':''}}>Happen</option>
                                                    <option value="1" {{$meeting->status == 1?'selected':''}}>Delay</option>
                                                    <option value="2" {{$meeting->status == 2?'selected':''}}>Cancel</option>
                                                </select>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $addmeeting->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('report-js')
<script>
    $("#endDate").change(function() {
        let startDate = document.getElementById("startDate").value;
        let endDate = document.getElementById("endDate").value;
        if ((Date.parse(startDate) >= Date.parse(endDate))) {
            toastr.warning("End date should be greater than start date");
            document.getElementById("endDate").value = "";
        }
    });
    let host = "{{URL::to('/')}}";
    //Category
    $('.filter-by-category').on('change', function() {
        let category = $(this).val();
        document.location.href = `${host}/report/${category}`;

    });
    //End Category
    //Status
    $('.meeting-status').on('change', function() {
        let meetingStatus = $(this).val();
        let meetingId = $(this).data("meeting-id");
        $.ajax({
            type: "GET",
            url: `${host}/report-status/${meetingId}/${meetingStatus}`,
            success: function(response) {
                if (response.status == 200) {
                    toastr.success('Status updated')
                }
                else{
                    toastr.error(response.msg)
                }
            }
        });

    });
    //End of Status
    //Status
    $('.filter-by-user').on('change', function(e) {
        e.preventDefault();
        let userId = $(this).val();
        let startDate = $("#startDate");
        let endDate = $("#endDate");
        if (!Date.parse(startDate.val())) {
            toastr.warning("Select from date")
            startDate.css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "red"
            });
            setInterval(function() {
                startDate.css({
                    "border-width": "1px",
                    "border-style": "solid",
                    "border-color": " #ced4da"
                });
            }, 3000);
            return false;
        } else if (!Date.parse(endDate.val())) {
            toastr.warning("Select end date")
            endDate.css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "red"
            });
            setInterval(function() {
                endDate.css({
                    "border-width": "1px",
                    "border-style": "solid",
                    "border-color": " #ced4da"
                });
            }, 3000);
            return false;

        } else if (userId == '') {
            toastr.warning("Select User")
            $(this).css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "red"
            });
            setInterval(function() {
                $(this).css({
                    "border-width": "1px",
                    "border-style": "solid",
                    "border-color": " #ced4da"
                });
            }, 3000);
            return false;
        } else {
            let category = $('.filter-by-category').val();
            console.log(category);
            if (category == '') {
                document.location.href = `${host}/report/no/${startDate.val()}/${endDate.val()}/${userId}`;
            } else {
                document.location.href = `${host}/report/${category}/${startDate.val()}/${endDate.val()}/${userId}`;
            }
        }
    });
    //End of Status
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".meeting-table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
</script>
@endsection