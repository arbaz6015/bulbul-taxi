 @php
use App\Models\Role;
$roles = Role::orderBy('name', 'asc')->get();
@endphp

@extends('layouts.section.dashboards')
@section('content')

<section class="content">
        <div class="container ">
            <div class="card user-card-full shadow">
                <div class="row m-l-0 m-r-0 mt-1">
                    <div class=" bg-c-lite-green user-profile">
                        <div class="card-block text-center">
                            <div class="m-b-20 mt-1 py-2">
              
                                <!-- <img src="image/userlogo.png" class="img-radius img-fluid user-profile-image" alt="User-Profile-Image" role="button"> -->
                            </div>
                            <!-- <h4 class="f-w-800 mt-3 text-success">{{ Auth::user()->name }}</h4>
                            <p>{{Auth::user()->getRoleNames()[0]}}</p> -->
                        </div>
                    </div>
                    <!--font css link h3  -->
                    <head>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Trirong">
<style>
.name h3 {
  font-family: "Trirong", serif;
}

.name2 p {
  font-family: "Trirong", serif;
}
.in1 {
  border: none;
  border-bottom: 1px solid green;
}

.container{
  margin-left: 20%;

}

</style>
</head>

<!-- font css end  -->
                   <div class="col-sm-8 py-4">
                        <div class="card-block name">
                            <h3 class="m-b-20 p-b-5 b-b-default f-w-600 px-3">Driver Information</h3>
                            <hr class="bg-success">
                            <div class="row px-3">
                                <div class="col-sm-6 mb-2 name2">
                                   <p class="mb-2 f-w-600">Name</p>
                                   <input class="form-control in1"  type="text" name="name" id="name" placeholder="Enter your Name">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>

                                <div class="col-sm-6 mb-2 ">
                                   <p class="mb-2 f-w-600">Street Address</p>
                                   <input class="form-control in1" type="text" name="name" id="name" placeholder="Enter your Address">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2">
                                   <p class="mb-2 f-w-600">City</p>
                                   <input class="form-control in1" type="text" name="name" id="name" placeholder="Enter your City">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2">
                                   <p class="mb-2 f-w-600">Country</p>
                                   <input class="form-control in1" type="text" name="name" id="name" placeholder="Enter User Name">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2">
                                   <p class="mb-2 f-w-600">Zip Code</p>
                                   <input class="form-control in1" type="text" name="name" id="name" placeholder="Enter User Name">

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 ">
                                   <p class="mb-2 f-w-600 ">Gender</p>
                                   <!-- <input type="text" name="name" id="name" placeholder="Enter User Gender"> -->
                                   <select id="gender" class="form-control in1">
                                     <option value="gender">select Gender</option>
                                     <option value="saab">Male</option>
                                     <option value="fiat">Female</option>
                                     <option value="audi">Other</option>
                                    </select>

                                   <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">Email</p>
                                    <input class="form-control in1" type="email" name="email" id="email" placeholder="Enter your Email">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->name }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 ">
                                    <p class="mb-2 f-w-600">Phone Number</p>
                                    <input class="form-control in1" type="text" name="number" id="number" placeholder="Enter your Number">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->number }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 ">
                                    <p class="mb-2 f-w-600">Date of Birth</p>
                                    <input class="form-control in1" type="date" name="date" id="date">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">Upload Driver Photo</p>
                                    <input class="form-control " type="file" name="document" id="document">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <br>
                                <br>
                                <br>
                                <br><br>
                                <div class="name">

                                <h3 class="m-b-20 p-b-5 b-b-default f-w-600 px-3">Vehicle Information</h3>
                                <hr class="bg-info">
                                </div>

                                <div class="col-sm-6 mb-2 ">
                                    <p class="mb-2 f-w-600">Vehicle Number</p>
                                    <input class="form-control in1" type="text" name="id" id="id" placeholder="Enter Vehicle number">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">Vehicle Types</p>
                                    <!-- <label for="cars">Choose Vehicle Type</label> -->
                                    <select id="cars" class="form-control in1">
                                     <option value="Vehical">select vehical type</option>
                                     <option value="suv">Suv</option>
                                     <option value="Sedan">Sedan</option>
                                     <option value="van">Van</option>
                                     <option value="wagon">Wangon</option>
                                    </select>
                                    <!-- <input type="text" name="id" id="id" placeholder="Enter your I.D"> -->
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 ">
                                    <p class="mb-2 f-w-600">Vehicle Model</p>
                                    <input class="form-control in1" type="text" name="id" id="id" placeholder="Enter Vehicle model">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 ">
                                    <p class="mb-2 f-w-600">Seting Capcity</p>
                                    <input class="form-control in1" type="text" name="id" id="id" placeholder="Enter Vehicle number">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 ">
                                    <p class="mb-2 f-w-600">Tax Renewal Date</p>
                                    <input class="form-control in1" type="date" name="date" id="date">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">Insurance Renewal Date</p>
                                    <input class="form-control in1" type="date" name="date" id="date">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2 ">
                                    <p class="mb-2 f-w-600">Upload Vehical Documents</p>
                                    <input class="form-control" type="file" name="document" id="document">
                                    <!-- <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6> -->
                                </div>
          
                              
                            </div>
                            <br>
                            <button class="btn btn-info">submit</button>





                            <!-- <button class=" btn btn-dark " >Cancel</button> -->
                            <!-- <hr class="bg-success"> -->
                            <!-- <h5 class=" f-w-600 px-3 py-2 btn btn-info">Register</h5> -->
                            <ul class="list-unstyled  d-flex py-2">
                                <li class="px-3">
                                    <a href="">
                                        <!-- <i class="nav-icon text-success fas fa-user"></i> -->
                                    </a>
                                </li>
                                <!-- <li class="px-3"><a href=""><i class="nav-icon text-success fas fa-handshake"></i></a></li>
                                <li class="px-3"><a href=""><i class="nav-icon text-success fas fa-bell"></i></a></li>
                                <li class="px-3"><a href=""><i class="fas text-success fa-pen nav-icon"></i></a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </form>
      </div>
    </div>
  </section>
<!--
<div class="content-wrapper mt-5">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h5 class="m-0">Dashboard /Add Partners</h5>
        </div>
        <div class="col-sm-6 text-right">
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid mb-5">
      <div class="card border border-success">
        <form method="POST" action="{{ url('add') }}" class="row g-3 py-3 px-4">
          @csrf
          <div class="col-md-12">
            <label class="form-label">Name:</label>
            <input type="text" class="form-control" name="name">
          </div>
          @if ($errors->has('name'))

          @php
          toastr()->warning($errors->first('name'));
          @endphp
          @endif
          <div class="col-md-12">
            <label class="form-label">Email:</label>
            <input type="email" class="form-control" name="email">
          </div>
          @if ($errors->has('email') && !$errors->has('name'))
          @php
          toastr()->warning($errors->first('email'));
          @endphp
          @endif
          <div class="col-md-12">
            <label class="form-label">Employee ID:</label>
            <input type="text" class="form-control" name="employee_id">
          </div>
          @if (!$errors->has('email') && !$errors->has('name') && $errors->has('employee_id'))
          @php
          toastr()->warning($errors->first('employee_id'));
          @endphp
          @endif
          <div class="col-12">
            <label class="form-label">Password</label>
            <input type="text" class="form-control" placeholder="" name="password">
          </div>
          @if (!$errors->has('email') && !$errors->has('name') && !$errors->has('employee_id') && $errors->has('password'))
          @php
          toastr()->warning($errors->first('password'));
          @endphp
          @endif
          <div class="col-12">
            <label class="form-label">Role:</label>
            <select class="form-select" aria-label="Default select example" name="role_name" value="{{ old('roll_name') }}" autocomplete="roll_name">
              <option value="">Select Role</option>
              @foreach($roles as $key=>$role)
              @if($role->name != 'Admin')
              <option value="{{$role->name}}">{{$role->name}}</option>
              @endif
              @endforeach
            </select>
          </div>
          @if (!$errors->has('email') && !$errors->has('name') && !$errors->has('employee_id') && !$errors->has('password') && $errors->has('role_name'))
          @php
          toastr()->warning($errors->first('role_name'));
          @endphp
          @endif
          <div class="col-12">
            <label class="form-label">Phone:</label>
            <input type="text" class="form-control" name="number">
          </div>
          <div class="col-12">
            <button type="submit" class="btn btn-success px-5">Add Member</button>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
@endsection -->

<!-- <div class="card-body row">
									<div class="col-lg-12">
										<h3>Basic Information</h3>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" id="txtFName">
											<label class="mdl-textfield__label">First Name</label>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" id="txtLName">
											<label class="mdl-textfield__label">Last Name</label>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" id="txtsa1">
											<label class="mdl-textfield__label">Street address 1</label>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" id="txtsa2">
											<label class="mdl-textfield__label">Street address 2</label>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" id="txtCity">
											<label class="mdl-textfield__label">City</label>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" id="txtCountry">
											<label class="mdl-textfield__label">Country</label>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="textZip">
											<label class="mdl-textfield__label" for="textZip">Zip Code</label>
											<span class="mdl-textfield__error">Number required!</span>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="email" id="txtemail">
											<label class="mdl-textfield__label">Email</label>
											<span class="mdl-textfield__error">Enter Valid Email Address!</span>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="text5">
											<label class="mdl-textfield__label" for="text5">Mobile Number</label>
											<span class="mdl-textfield__error">Number required!</span>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width is-upgraded" data-upgraded=",MaterialTextfield" style="width: 124px;">
											<input class="mdl-textfield__input" type="text" id="sample2" value="" readonly="" tabindex="-1">
											<label for="sample2" class="pull-right margin-0">
												<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
											</label>
											<label for="sample2" class="mdl-textfield__label">Gender</label>
											<div class="mdl-menu__container is-upgraded"><div class="mdl-menu__outline mdl-menu--bottom-left"></div><ul data-mdl-for="sample2" class="mdl-menu mdl-menu--bottom-left mdl-js-menu" data-upgraded=",MaterialMenu">
												<li class="mdl-menu__item" data-val="DE" tabindex="-1">Male</li>
												<li class="mdl-menu__item" data-val="BY" tabindex="-1">Female</li>
											</ul></div>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width mdl-textfield--file has-placeholder is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" placeholder="Upload Profile Photo" type="text" id="uploadFile">
											<div class="mdl-button mdl-button--primary mdl-button--icon mdl-button--file">
												<i class="material-icons">attach_file</i><input type="file" id="uploadBtn">
											</div>
										</div>
									</div>
									<div class="col-lg-12">
										<h3>Vehicle Information</h3>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="txtVno">
											<label class="mdl-textfield__label" for="text5">Vehicle Number</label>
											<span class="mdl-textfield__error">Number required!</span>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width is-upgraded" data-upgraded=",MaterialTextfield" style="width: 124px;">
											<input class="mdl-textfield__input" type="text" id="vtype" value="" readonly="" tabindex="-1">
											<label for="vtype" class="pull-right margin-0">
												<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
											</label>
											<label for="vtype" class="mdl-textfield__label">Vehicle Type</label>
											<div class="mdl-menu__container is-upgraded"><div class="mdl-menu__outline mdl-menu--bottom-left"></div><ul data-mdl-for="vtype" class="mdl-menu mdl-menu--bottom-left mdl-js-menu" data-upgraded=",MaterialMenu">
												<li class="mdl-menu__item" data-val="1" tabindex="-1">SUV</li>
												<li class="mdl-menu__item" data-val="2" tabindex="-1">SEDAN</li>
												<li class="mdl-menu__item" data-val="3" tabindex="-1">Crossover</li>
												<li class="mdl-menu__item" data-val="4" tabindex="-1">Coupe</li>
												<li class="mdl-menu__item" data-val="5" tabindex="-1">Van</li>
												<li class="mdl-menu__item" data-val="6" tabindex="-1">Wagon</li>
											</ul></div>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" id="txtvmodel">
											<label class="mdl-textfield__label">Vehicle Model</label>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="txtSCapacity">
											<label class="mdl-textfield__label" for="text5">Seating Capacity</label>
											<span class="mdl-textfield__error">Number required!</span>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input flatpickr-input" type="text" id="date">
											<label class="mdl-textfield__label">Tax Renewal Date </label>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input flatpickr-input" type="text" id="date1">
											<label class="mdl-textfield__label">Insurance Renewal Date </label>
										</div>
									</div>
									<div class="col-lg-6 p-t-20">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width mdl-textfield--file has-placeholder is-upgraded" data-upgraded=",MaterialTextfield">
											<input class="mdl-textfield__input" placeholder="Upload Vehicle Documents" type="text" id="uploadLicence">
											<div class="mdl-button mdl-button--primary mdl-button--icon mdl-button--file">
												<i class="material-icons">attach_file</i><input type="file" id="uploadBtnLicence">
											</div>
										</div>
									</div>
									<div class="col-lg-12 p-t-20 text-center">
										<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" data-upgraded=",MaterialButton,MaterialRipple">Submit<span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>
										<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-upgraded=",MaterialButton,MaterialRipple">Cancel<span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>
									</div>
								</div>
               -->
