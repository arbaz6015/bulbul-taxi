@extends('layouts.section.dashboards')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper mt-5">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add New Meeting</h1>
                    <ul class="error">
                        @if(isset(($errors->all())[0]))
                        @php
                        toastr()->warning(($errors->all())[0]);
                        @endphp
                        @endif
                    </ul>

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add meeting</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header py-2">
                    <h5 class="modal-title" id="exampleModalToggleLabel">All User</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="card text-center">
                            <div class="card-header border-0">
                                <h3 class="card-title">Users</h3>
                            </div>
                            <div class="card-body ">
                                <div class="table-responsive pb-3">
                                    <table id="user-table" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>Sr.</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Roll Name</th>
                                                <th>Emplyee ID</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $key=>$user)
                                            <tr>
                                                <td><input type="checkbox" data-user-id="{{$user->id}}" value="{{$user->name}}">
                                                <td>{{intval($key)+1}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{isset((($user->getRoleNames())[0]))?(($user->getRoleNames())[0]):'NA';}}</td>
                                                <td>{{$user->employee_id}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" data-bs-target="#exampleModalToggle2" id="get-selected" data-bs-toggle="modal">Select Participant</a>
                </div>
            </div>
        </div>
    </div>
    <a class="btn btn-successs bg-success  mx-2 mb-2" href="{{url('addUser')}}">Add New Partners</a>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <form method="POST" action="{{ url('/') }}">
                    @csrf
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">GENERAL DETAILS</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Meeting Topic(Agenda) :</label>
                                <input type="text" id="inputName" name="meeting_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">Meeting Description :</label>
                                <textarea id="inputDescription" name="description" class="form-control" rows="2"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="inputStatus">Meeting Id :</label>
                                <input type="text" id="inputName" name="meeting_id" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="inputClientCompany">Partners :</label>
                                <input type="hidden" id="participants_id" name="participants" class="form-control text-success">
                                <input type="text" value="" id="participants" class="form-control text-success" data-bs-toggle="modal" href="#exampleModalToggle" role="button">
                            </div>
                            <div class="form-group">
                                <label for="inputProjectLeader">Category :</label>
                                <select class="form-control" name="category">
                                    <option value="">Select Category</option>
                                    <option>Quick Check</option>
                                    <option>Consultation</option>
                                    <option>Others</option>
                                </select>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <!-- /.card -->
            </div>
            <div class="col-md-6">

                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title">DATE & TIME</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Date :</label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                </div>
                                <input type="date" name="meeting_date" class="form-control float-right date">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label for="inputSpentBudget">Time:</label>
                            <input type="time" name="meeting_time" class="form-control">
                        </div>
                        <label>Duration:</label>
                        <div class="input-group">
                            <input type="text" class="form-control hh N" min="0" max="23" placeholder="hh" maxlength="2" style="outline: none;" name="hours" />:
                            <input type="text" class="form-control mm N" min="0" max="59" placeholder="mm" maxlength="2" style="outline: none;" name="minute" />
                            <!-- <input type="time" name="duration" class="form-control duration"> -->
                        </div>
                        <div class="form-group">
                            <label for="inputSpentBudget">Place of Meeting:</label>
                            <input type="text" name="meeting_place" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputProjectLeader">Host Name</label>
                            <select class="form-control" name="host_name">
                                <option selected>{{ Auth::user()->name }}</option>
                            </select>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>
        <div class="row text-center mt-3">
            <div class="col-12">
                <a href="{{url('allmeeting')}}" class="btn btn-secondary px-4 mx-5">Cancel</a>
                <input type="submit" value="Create new Meeting" class="btn btn-success">
            </div>
        </div>
        </form>
    </section>

    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('add-meeting-js')
<script src="{{asset('/dist/js/meeting-form.js')}}"></script>
@endsection