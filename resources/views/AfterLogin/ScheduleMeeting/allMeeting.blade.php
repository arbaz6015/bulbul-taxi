@extends('layouts.section.dashboards')
@section('content')
@php
$quickCheck = $category == 'QuickCheck' ? "selected" : "";
$consultation = $category == 'Consultation' ? "selected" : "";
$others = $category == 'Others' ? "selected" : "";
@endphp
<div class="content-wrapper mt-5 py-3">
    <!-- Content Header (Page header) -->
    <section class="content">
        <div class="container-fluid mb-5">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Meetings</span>
                            <span class="info-box-number">
                                @if(isset($meeting) && !empty($meeting))
                                {{count($meeting)}}
                                @else
                                0
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card text-center">
                        @include('AfterLogin.filter')
                        <div class="card-body ">
                            <div class="table-responsive pb-3">
                                <table id="user-table" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sr.</th>
                                            <th>Meeting Name</th>
                                            <th>Participant</th>
                                            <th>Meeting ID</th>
                                            <th>Host Name</th>
                                            <th>Category</th>
                                            <th>Meeting Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="article-table">
                                        @foreach($addmeeting as $key=>$meeting)
                                        @php
                                        $userCollection = array();
                                        if (isset($meeting->meeting)){
                                        foreach ($meeting->meeting as $userVal){
                                        if (!in_array((($userVal['name']) ? $userVal['name'] : ""), $userCollection)) {
                                        array_push($userCollection, (($userVal['name']) ? $userVal['name'] : ""));
                                        }
                                        }
                                        }
                                        $userCollection = array_filter($userCollection);
                                        $userName = implode(", ", $userCollection);
                                        @endphp
                                        <tr>
                                            <td>{{intval($key)+1}}</td>
                                            <td>{{isset($meeting->meeting_name)?$meeting->meeting_name:'NA'}}</td>
                                            <td>{{isset($userName)?$userName:'NA'}}</td>
                                            <td>{{isset($meeting->meeting_id)?$meeting->meeting_id:'NA'}}</td>
                                            <td>{{isset($meeting->users->name)?$meeting->users->name:'NA'}}</td>
                                            <td>{{$meeting->category}}</td>
                                            <td>{{$meeting->meeting_date}}</td>
                                            <td class="form-inline border 0"><a href="{{url('editmeeting')}}/{{ $meeting->id }}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"> Edit</i></a>
                                            <form method="POST" action="{{ url('delmeeting', $meeting->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-sm ml-2"><i class="fas fa-trash">
                                                        </i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $addmeeting->links() }}
                                <div class="d-flex justify-content-center">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
@section('meeting-js')
<script>
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".article-table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    $("#endDate").change(function() {
        let startDate = document.getElementById("startDate").value;
        let endDate = document.getElementById("endDate").value;
        if ((Date.parse(startDate) >= Date.parse(endDate))) {
            toastr.warning("End date should be greater than start date");
            document.getElementById("endDate").value = "";
        }
    });
    let host = "{{URL::to('/')}}";
    //Category
    $('.filter-by-category').on('change', function() {
        let category = $(this).val();
        document.location.href = `${host}/allmeeting/${category}`;

    });
    //End Category
    //Status
    $('.meeting-status').on('change', function() {
        let meetingStatus = $(this).val();
        let meetingId = $(this).data("meeting-id");
        $.ajax({
            type: "GET",
            url: `${host}/allmeeting/${meetingId}/${meetingStatus}`,
            success: function(response) {
                if (response.status == 200) {
                    toastr.success('Status updated')
                }
            }
        });

    });
    //End of Status
    //Status
    $('.filter-by-user').on('change', function(e) {
        e.preventDefault();
        let userId = $(this).val();
        let startDate = $("#startDate");
        let endDate = $("#endDate");
        if (!Date.parse(startDate.val())) {
            toastr.warning("Select from date")
            startDate.css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "red"
            });
            setInterval(function() {
                startDate.css({
                    "border-width": "1px",
                    "border-style": "solid",
                    "border-color": " #ced4da"
                });
            }, 3000);
            return false;
        } else if (!Date.parse(endDate.val())) {
            toastr.warning("Select end date")
            endDate.css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "red"
            });
            setInterval(function() {
                endDate.css({
                    "border-width": "1px",
                    "border-style": "solid",
                    "border-color": " #ced4da"
                });
            }, 3000);
            return false;

        } else if (userId == '') {
            toastr.warning("Select Role")
            $(this).css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "red"
            });
            setInterval(function() {
                $(this).css({
                    "border-width": "1px",
                    "border-style": "solid",
                    "border-color": " #ced4da"
                });
            }, 3000);
            return false;
        } else {
            let category = $('.filter-by-category').val();
            console.log(category);
            if (category == '') {
                document.location.href = `${host}/allmeeting/no/${startDate.val()}/${endDate.val()}/${userId}`;
            } else {
                document.location.href = `${host}/allmeeting/${category}/${startDate.val()}/${endDate.val()}/${userId}`;
            }
        }
    });
    //End of Status
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        console.log(value);
        $(".meeting-table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
</script>
@endsection