@extends('layouts.section.dashboards')
@section('content')
<div class="content-wrapper mt-5 py-4">
    <!-- Content Header (Page header) -->
    <section class="content">
        <div class="container-fluid mb-5">
            <div class="row">
              <div class="col-12 col-sm-6 col-md-3">
                      <div class="info-box mb-3">
                          <span class="info-box-icon bg-warning elevation-1"><i class="fas fa fa-users"></i></span>
                          <div class="info-box-content">
                              <span class="info-box-text">Total Contacts</span>
                              <span class="info-box-number">
                                  @if(isset($contact) && !empty($contact))
                                  {{count($contact)}}
                                  @else
                                  0
                                  @endif
                              </span>
                          </div>
                      </div>
                  </div>
                <div class="col-md-12">
                    <div class="card text-center">
                      <div class="row px-3 py-2">
                          <div class="col-6 mt-4 d-flex">
                              <input class="form-control" type="search" placeholder="Search with meeting name" id="search" aria-label="Search">
                            
                          </div>
                          <div class="col-3 mt-4 d-flex">
                              
                              <select class="form-select" aria-label="Default select example">
                                  <option selected>Select Filter</option>
                                  <option value="1">One</option>
                                  <option value="2">Two</option>
                                  <option value="3">Three</option>
                                </select>
                          </div>
                          <div class="col-3 mt-4">
                          
                              <select class="form-select" aria-label="Default select example">
                                  
                              </select>
                          </div>
                      </div>
                        <div class="card-body ">
                            <div class="table-responsive pb-3">
                                <table id="user-table" class="table table-bordered table-hover">
                                    <thead>
                                      <tr>
                                        <th>Sr.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Subject</th>
                                        <th>Message</th>
                                      </tr>
                                    </thead>
                                    <tbody class="article-table">
                                        @foreach($contact as $key=>$cont)
                                        <tr>
                                          <td>{{intval($key)+1}}</td>
                                          <td>{{$cont->name}}</td>
                                          <td>{{$cont->email}}</td>
                                          <td>{{$cont->subject}}</td>
                                          <td>{{$cont->message}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".article-table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
</script>
@endsection


 
  
  
