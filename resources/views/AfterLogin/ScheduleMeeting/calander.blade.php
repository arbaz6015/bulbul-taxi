

 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
 <style>
    .fc-day-grid-event .fc-time {
    font-weight: 200;
    display: none;

}
.fc-content{
    background-color: green;
}

   
   
 </style>

<div class="modal fade" id="modalnew" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
       
        <div class="modal-header">
            
          <h5 class="modal-title">Meeting Name:</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        
        <div class="modal-body" >
            <h3 id="title"></h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-bs-dismiss="modal">Close</button>   
        </div>  
      </div>
    </div>
  </div>
 <div class="content-wrapper py-3 px-5">   
    <div id='calendar' class="shadow"></div>  
        <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            
            // put your options and callbacks here
            header: {
          left: 'prev,next today',
           center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
            events : [
                @foreach($addmeeting as $meeting)
                {
                    date : '{{ $meeting->meeting_date }}',
                    title : '{{ $meeting->meeting_name }}', 
                              
                },
                @endforeach
            ]
            
        })
        // //$('.fc-content').css('background-color', 'green');
       $('.fc-content').click(function(e) {
        $('#modalnew').modal('show');
       let title = $(this).children(".fc-title").text();
       $("#title").text(title);
       

       })
        //     var first = $('.fc-title').text();
        //     $('#modalnew').modal('show');  
        //     $('.modal-title').val(first);
        // })
        // $('.fc-content').click(function(e) {
        //     $('#modalnew').modal('show');   
});
  
</script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>