@extends('layouts.section.dashboards')
@section('content')
<div class="content-wrapper mt-5 py-4">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-success card-outline">
					<div class="card-header">
						<h1  class="card-title">All Drivers</h1>
						<div class="card-tools">
							<div class="input-group input-group-sm">
							<input type="text" id="search-input" class="form-control" placeholder="Search...">

								<!-- <input type="text"  class="form-control" placeholder="Search" > -->
								<div class="input-group-append">
									<div class="btn btn-success">
										<i class="fas fa-search"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					
						<div class="table-responsive mailbox-messages">
							<table id="search-table" class="table table-hover table-striped" >
								<thead>
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">#</th>
										<th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">Photo</th>
										<!-- <th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">Identity card</th>													 -->
										<!-- <th class="sorting_asc" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 13.5625px;">RC Photo</th> -->
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip Id: activate to sort column ascending" style="width: 29.9844px;">Name</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Driver Name: activate to sort column ascending" style="width: 47.0469px;">Mobile</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.7188px;">Email</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.7188px;">Gender</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.7188px;">Postal Code</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip From: activate to sort column ascending" style="width: 81.375px;">Address</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.7188px;">City</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Passenger Name: activate to sort column ascending" style="width: 81.7188px;">Country</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Trip To: activate to sort column ascending" style="width: 55.6406px;">Driver license no..</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="End Time: activate to sort column ascending" style="width: 38.5625px;">Driver Identity card</th>
										<!-- <th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Start Time: activate to sort column ascending" style="width: 38.6094px;">Trips</th> -->
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="End Time: activate to sort column ascending" style="width: 38.5625px;">Status
									</th>
										<th class="sorting" tabindex="0" aria-controls="tableExport" rowspan="1" colspan="1" aria-label="Distance: activate to sort column ascending" style="width: 68.4062px;">Action</th>
								</thead>
								<tbody>
								@foreach($drivers as $drivers)
									<tr class="gradeX odd" role="row">
										<td class="center sorting_1">{{$drivers->id}}</td>
										<td class="center">
											<!-- {{$drivers->photo}} -->
											<img src="{{ asset('storage/images/driver/'.$drivers->photo) }}" alt="{{$drivers->photo}}" width="70px" style="border-radius: 10px;">
										</td>
										<!-- <td class="center">
											{{$drivers->government_id_photo}}
											<img src="{{ url('storage/images/driver/'.$drivers->government_id_photo) }}" alt="{{$drivers->government_id_photo}}" width="70px" style="border-radius: 10px;">

										</td>
										<td class="center">
											{{$drivers->rc_photo}}
											<img src="{{ url('storage/images/driver/'.$drivers->rc_photo) }}" alt="{{$drivers->rc_photo}}" width="70px" style="border-radius: 10px;">
										</td> -->
										<td class="center">{{$drivers->name}}</td>
										<td class="center">
											{{$drivers->phone_number}}
										</td>
										<td class="center">
											{{$drivers->email}}
										</td>
										<td class="center">{{$drivers->gender}}</td>
										<td class="center">{{$drivers->zip_code}}</td>
										<td class="center">{{$drivers->street_address}}</td>
										<td class="center">{{$drivers->city}}</td>
										<td class="center">{{$drivers->country}}</td>
										
										<td class="center">{{$drivers->driver_license_no}}</td>
										<td class="center">{{$drivers->driver_government_id	}}</td>
										<td>
										
											@if($drivers->status == "1")
											Active 
											@else
											Inactive
											@endif

										</td>
										<td class="center d-flex">
										<a href="{{ url('adddriver/editdriver/'.$drivers->id) }}" class="btn btn-dark btn-xs">
														<i class="fas fa-regular fa-pen"></i></a>
											<a href="{{ url('adddriver/delete-data/'.$drivers->id) }}" class="btn btn-warning btn-xs">
											<i class="far fa-trash-alt"></i>
											</a>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		
	</section>	
</div>
@endsection
@section('user-js')
<script>
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".driver-table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    $("#endDate").change(function() {
        let startDate = document.getElementById("startDate").value;
        let endDate = document.getElementById("endDate").value;
        if ((Date.parse(startDate) >= Date.parse(endDate))) {
            toastr.warning("End date should be greater than start date");
            document.getElementById("endDate").value = "";
        }
    });
    let host = "{{URL::to('/')}}";
    //Status
    $('.filter-by-role').on('change', function(e) {
        e.preventDefault();
        let roleId = $(this).val();
        let startDate = $("#startDate");
        let endDate = $("#endDate");
        if (!Date.parse(startDate.val())) {
            toastr.warning("Select from date")
            startDate.css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "red"
            });
            setInterval(function() {
                startDate.css({
                    "border-width": "1px",
                    "border-style": "solid",
                    "border-color": " #ced4da"
                });
            }, 3000);
            return false;
        } else if (!Date.parse(endDate.val())) {
            toastr.warning("Select end date")
            endDate.css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "red"
            });
            setInterval(function() {
                endDate.css({
                    "border-width": "1px",
                    "border-style": "solid",
                    "border-color": " #ced4da"
                });
            }, 3000);
            return false;

        } else if (roleId == '') {
            toastr.warning("Select Role")
            $(this).css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "red"
            });
            setInterval(function() {
                $(this).css({
                    "border-width": "1px",
                    "border-style": "solid",
                    "border-color": " #ced4da"
                });
            }, 3000);
            return false;
        } else {
            document.location.href = `${host}/user/alluser/${startDate.val()}/${endDate.val()}/${roleId}`;

        }
    });
    //End of Status
	
  // Get the search input element and the table
  const searchInput = document.getElementById('search-input');
  const table = document.getElementById('search-table');

  // Add an event listener to the search input
  searchInput.addEventListener('keyup', function () {
    const searchText = searchInput.value.toLowerCase();

    // Loop through all rows of the table body
    const rows = table.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      const cells = row.getElementsByTagName('td');
      let found = false;

      // Loop through all cells in the row
      for (let j = 0; j < cells.length; j++) {
        const cell = cells[j];
        const cellText = cell.textContent || cell.innerText;

        // Check if the cell text contains the search text
        if (cellText.toLowerCase().indexOf(searchText) > -1) {
          found = true;
          break;
        }
      }

      // Show/hide the row based on search result
      row.style.display = found ? '' : 'none';
    }
  });
</script>


@endsection
