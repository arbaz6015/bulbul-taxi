@extends('layouts.section.dashboards')
@section('content')

<div class="content-wrapper mt-5">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3 class="m-0">Meeting Notification</h3>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Notification</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- /.row (main row) -->

  <ul class="nav nav-pills mb-3 px-4" id="pills-tab" role="tablist">
    <li class="nav-item px-2" role="presentation">
      <button class="nav-link btn-success active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Today </button>
    </li>
    <li class="nav-item px-2" role="presentation">
      <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Upcoming </button>
    </li>
    <li class="nav-item px-2" role="presentation">
      <button class="nav-link btn-danger" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Expired </button>
    </li>
  </ul>
  <div class="tab-content px-4" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
      <section class="content">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-exclamation-triangle"></i>
              <span>Today </span>@if(isset($today) && !empty($today))
              {{count($today)}}
              @endif
            </h3>
          </div>
          <!-- /.card-header -->
          @foreach($today as $tody)
          <div class="card-body">
            <div class="alert  border border-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i><span>{{$tody->meeting_name}}</span></h5>
              At {{$tody->meeting_time}} O'Clock Meeting .
            </div>
          </div>
          <!-- /.card-body -->
          @endforeach
        </div>
      </section>
    </div>
    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
      <section class="content">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-exclamation-triangle"></i>
              Upcoming Meeting @if(isset($up_date) && !empty($up_date))
              {{count($up_date)}}
              @endif
            </h3>
          </div>
          <!-- /.card-header -->
          @foreach($up_date as $up_dat)
          <div class="card-body">
            <div class="alert shadow border border-primary alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i>{{$up_dat->meeting_name}}</h5>
              At : {{$up_dat->meeting_time}} Clock Meeting Date: {{$up_dat->meeting_date}}
            </div>
          </div>
          @endforeach
          <!-- /.card-body -->
        </div>
      </section>
    </div>
    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
      <section class="content">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-exclamation-triangle"></i>
              Expired Meeting @if(isset($expiry) && !empty($expiry))
              {{count($expiry)}}
              @endif
            </h3>
          </div>
          <!-- /.card-header -->
          @foreach($expiry as $ad)
          <div class="card-body">
            <div class="alert shadow border border-danger alert-dismissible pr-0">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i>{{$ad->meeting_name}}</h5>
              <div class="row">
                <div class="col-md-6">
                  At : {{$ad->meeting_time}} Clock Meeting Date: {{$ad->meeting_date}}
                </div>
                <div class="col-md-6">
                  <p class="text-right pr-2">{{($ad->status == 0)?'Happen':($ad->status == 1)?'Delay':'Cancel'}}</p>
                </div>
              </div>
            </div>
            @endforeach
            <!-- /.card-body -->
          </div>
      </section>
    </div>
  </div>
  <!-- /.content -->
</div>
@endsection