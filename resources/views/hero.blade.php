<!----hero---->
    <div class="container col-xxl-8 px-4 " style="padding:110px 0px;">
        <div class="row flex-lg-row-reverse align-items-center g-5 py-5 justify-content-center">
            <div class="col-10 col-sm-12 col-lg-6 ">
                <img src="image/meetinglogo.png" class="d-block mx-lg-auto  img-fluid" alt="Meeting Image" width="800"
                    height="500" loading="lazy">
            </div>
            <div class="col-lg-6">
                <h1 class="display-5 fw-bold lh-1 mb-3"><span class="text-primary"> Mechanism </span>to Help You
                    Schedule <span class="text-danger">Meetings </span>Faster</h1>
                <p class="lead text-success"><b>Copy and paste these templates for automated solutions to all of your
                        meeting scheduling woes. From interviews to internal comms, SMS provides a solution.</b></p>
                <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                        @if (Route::has('login') && Auth::check())
                      <button type="button" class="btn btn-success btn-lg shadow px-4 me-md-2"><a href="{{ url('/home') }}" class="text-light text-decoration-none">Create Meeting</a></button>
                        @elseif (Route::has('login') && !Auth::check())
                        <button type="button" class="btn btn-success shadow btn-lg px-4 me-md-2"><a href="{{ route('login') }}" class="text-light text-decoration-none">Create Meeting</a></button>  
                    @endif
                    @if (Route::has('login') && Auth::check())
                    <button type="button" class="btn btn-outline-success shadow btn-lg px-4 me-md-2"><a href="{{ url('/home') }}" class="text-success text-decoration-none">Profile</a></button>
                      @elseif (Route::has('login') && !Auth::check())
                      <button type="button" class="btn btn-outline-success  shadow btn-lg px-4 me-md-2"><a href="{{ route('login') }}" class="text-success text-decoration-none">Profile</a></button>  
                  @endif
                    
                </div>
            </div>
        </div>
    </div>
    <!----/hero---->