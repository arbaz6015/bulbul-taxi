<!----navbar------>
<nav class="navbar navbar-expand-lg text-success sticky-top shadow-sm bg-light navbar-light">
    <div class="container">
        <a class="navbar-brand" href="{{url('/')}}">
        <img src="{{asset('/image/logo.png')}}" alt=""class="img-fluid" width="200" height="50">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-2 mb-2 mb-lg-0">
                <li class="nav-item ">
                    <a class="nav-link text-success" aria-current="page" href="{{url('/')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-success" href="{{url('about')}}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-success" href="#">Features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-success" href="#">Pricing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-success" href="#">FAQs</a>
                </li>
               
                <li class="nav-item ">
                    <a class="nav-link text-success" href="{{url('contact')}}">Contact us</a>
                </li>
            </ul>
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item mb-2 px-2">
                            <a href="{{ route('login') }}"
                                    class="btn btn-success shadow px-4">Login</a>
                        </li>
                    @endif
                    {{-- @if (Route::has('register'))
                        <li class="nav-item px-2">
                            <button class="btn btn-success"><a href="{{ route('register') }}"
                                    class="text-light text-decoration-none">Signup</a></button>
                        </li>
                    @endif --}}
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link link-success text-success dropdown-toggle" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <img src="image/userlogo.png" class="img-fluid rounded-circle" alt="" width="30px"
                                height="30px">
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right text-center" aria-labelledby="navbarDropdown">
                            <a href="#" class="dropdown-item" >
                                <img src="image/userlogo.png" class="img-fluid rounded-circle" alt="" width="30px"
                                        height="30px">
                            {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{url('/profile')}}" class="dropdown-item" >
                                Profile
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                    <li class="nav-item px-2">
                        <a href="{{ route('home') }}"
                            class="nav-link text-decoration-none text-success">Dashboard</a>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
<!----/navbar---->