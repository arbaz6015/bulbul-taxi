<!----footer--->
<footer class=" container-fluid bg-light py-4 mt-5 text-center shadow">
    <strong>Copyright &copy; 2022 - <?= date('Y'); ?>
        <a href="#" class="text-success">Green Finance</a> | 
    </strong>
    All rights reserved
</footer>

<!----/footer--->