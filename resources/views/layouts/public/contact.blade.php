 <!----conatct----->
 <form method="POST" action="{{url('submit')}}">
 <div class="container">
    <div class="row mt-5">
        <h3 class="text-center text-success mt-5 mb-5">Contact Us</h3>
        <div class="col-lg-4">
            <div class="info text-success">
                <div class="address mt-4">
                    <h4>Location:</h4>
                    <p>location:"A108 Adam Street, Germany, NY 535022"</p>
                </div>
                <div class="email mt-4">
                    <h4>Email:</h4>
                    <p>email:"greenFinance@gmail.com"</p>
                </div>
                <div class="phone mt-4">
                    <h4>Call:</h4>
                    <p>call:"+12 0000 55488 55"</p>
                </div>
            </div>
        </div>
        @csrf
        <div class="col-lg-8 mt-5 mt-lg-0">
            <div class="row">
                <div class="col-md-6 form-group">
                    <input type="text" name="name" class="form-control bg-light" id="name" placeholder="Your Name"
                        required="" value="">
                </div>
                <div class="col-md-6 form-group mt-5 mt-md-0">
                    <input type="email" class="form-control bg-light" name="email" id="email"
                        placeholder="Your Email" required="" value="">
                </div>
            </div>
            <div class="form-group mt-5">
                <input type="text" class="form-control bg-light" name="subject" id="subject" placeholder="Subject"
                    required="" value="">
            </div>
            <div class="form-group mt-5">
                <textarea class="form-control bg-light" name="message" rows="5" placeholder="Message" required=""></textarea>
            </div>
            <div class="mt-5 mb-5"><button class="btn btn-success shadow" type="submit">Send Message</button></div>
        </div>
    </div>
</div>
</div>
</form>
<!----/conatct----->