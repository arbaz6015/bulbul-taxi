<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-success elevation-4" style="background-color: #bfe7c8;">
<div class="sidebar">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
        <img src="{{asset('/image/logo.png')}}" alt="bulbul-taxi-dashboard" class="img-fluid" width="70px" />
<i class="pl-3" style="color:green">Bulbul Taxi</i>
    </a>
    <!-- Sidebar -->
    
    <!-- <div class="sidebar"> -->
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('/image/userlogo.png')}}" class="img-circle elevation-2" alt="" width="30px" height="30px">

            </div>
            <div class="info">
                <a href="{{url('/profile')}}" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>


        
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
       with font-awesome or any other icon font library -->

       <!-- dashboard  -->
                <li class="nav-item">
                    <a href="{{url('home')}}" class="nav-link {{Route::is('home*') ? 'active ' : '' }}">
                     <div>   <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p class="pl-1">
                            Dashboard
                        </p>
                        </div>
                    </a>
                </li>
                <!-- end dashboard  -->
                <!-- Driver -->
                <li class="nav-item {{Route::is('account.*') ? 'menu-open' : ''}}">
                    <a href="javascript:void(0)" class="nav-link {{Route::is('account.*') ? 'active' : ''}}">
                       <div><i class="nav-icon fa fa-users"></i>
                        <p class="pl-1">
                        Drivers    </p>
                        </div>
                    <i class="fas fa-angle-left right"></i>  </a>
                   <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <!-- <a href="{{ route('user.addUser') }}" class="nav-link {{Route::is('user.addUser*') ? 'active ' : ''}}"> -->
                            <a href="{{route('adddriver.create')}}" class="nav-link {{(Route::is('create*'))? 'active sidebar-mini-active' : ''}}">
                                <!-- <i class="fa fa-plus nav-icon"></i> -->
                                <p>Add New Driver</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('adddriver.alldriver')}}" class="nav-link {{(Route::is('alldriver*'))? 'active sidebar-mini-active ' : ''}}">
                                <!-- <i class="fa fa-list-alt nav-icon"></i> -->
                                <p>All Drivers</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                        <a href="{{route('account.create')}}" class="nav-link {{(Route::is('create*'))? 'active sidebar-mini-active' : ''}}">
                                <!-- <i class="fa fa-list-alt nav-icon"></i> -->
                                <p>Add Drivers Account </p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                        <a href="{{route('account.viewaccount')}}" class="nav-link {{(Route::is('viewaccount*'))? 'active sidebar-mini-active' : ''}}">
                                <!-- <i class="fa fa-list-alt nav-icon"></i> -->
                                <p>View Drivers Account </p>
                            </a>
                        </li>
                    </ul>
               
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('/payment')}}"class="nav-link {{ request()->routeIs('payment*')? 'active sidebar-mini-active'  : ''}}">
                                <!-- <i class="fa fa-list-alt nav-icon"></i> -->
                                <p>Drivers Payment</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- End Driver -->
                <li class="nav-item {{Route::is('role.*') ? 'menu-open' : ''}}">
                    <a href="javascript:void(0)" class="nav-link {{Route::is('role.*') ? 'active sidebar-mini-active' : ''}}">
                           <!-- <i class="nav-icon fa fa-users"></i> -->
                      <div>  <i class="fas fa-regular fa-car"></i>
                        <p class="pl-3">Trip </p>
                    </div>
                        <i class="fas fa-angle-left right"></i> </a>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('role.create') }}" class="nav-link {{Route::is('role.create*') ? 'active sidebar-mini-active' : ''}}">
                            <p>Active Trips</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('role.all') }}" class="nav-link {{(Route::is('role.all*')||Route::is('role.update*'))? 'active sidebar-mini-active' : ''}}">
                            <p> Completed Trips</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <!-- <a href="{{ route('user.alluser') }}" class="nav-link {{(Route::is('user.alluser*')||Route::is('user.edit-user*'))? 'active ' : ''}}"> -->
                            <a href="{{url('/bookedtrip')}}"class="nav-link {{ request()->routeIs('bookedtrip*')? 'active sidebar-mini-active'  : ''}}">
                                 <!-- <i class="fa fa-list-alt nav-icon"></i> -->
                                <p>Booked Trips</p>
                            </a>
                        </li>
                    </ul>
                    
                </li>
                </li>
                <!-- END TRIPS  -->

                <!-- VEHICAL TYPES  -->
                <li class="nav-item {{Route::is('addvehicle.*') ?  : ''}}">
                    <a href="javascript:void(0)" class="nav-link {{Route::is('addvehicle.*') ?  'active sidebar-mini-active' : ''}}">
                        <!-- <i class="nav-icon fa fa-users"></i> -->
                       <div> <i class="fas fa-solid fa-car-side"></i>
                        <p class="pl-3">  Vehicle                      </p>
                        </div>
                        <i class="fas fa-angle-left right"></i>                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                        <!-- <a href="{{url('/addvehicle')}}"class="nav-link {{ Route::is('addvehicle*')? 'active sidebar-mini-active'  : ''}}"> -->

                        <a href="{{route('addvehicle.create')}}" class="nav-link {{(Route::is('create*'))? 'active sidebar-mini-active' : ''}}">
                                <!-- <i class="fa fa-plus nav-icon"></i> -->
                                <p>Add Vehicle Details</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('addvehicle.viewvehicle')}}" class="nav-link {{ request()->routeIs('viewvehicle*')? 'active sidebar-mini-active'  : ''}}">
                                <!-- <i class="fa fa-list-alt nav-icon"></i> -->
                                <p> View All Vehicle </p>
                            </a>
                        </li>
                    </ul>
                    <!-- <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('/editvehicle')}}" class="nav-link {{(Route::is('editvehicle*'))? 'active sidebar-mini-active' : ''}}">
                                <i class="fa fa-list-alt nav-icon"></i>
                                <p>Edit Vehicle Details</p>
                            </a>
                        </li>
                    </ul> -->
              </li>    
              <!-- end vehicle         -->

                <!-- Fare management TAG  -->
                <li class="nav-item  menu-is-opening menu-open">
                <li class="nav-item">
                    <a href="{{url('/mail')}}" class="nav-link {{ request()->routeIs('mail*') ? 'sidebar-mini-active' : '' }}">
                       <div>  <i class="fas fa-solid fa-dollar-sign"></i>
                       <p class="pl-3">
                            Fare Management
                        </p>
                       </div>
                       <i class="fas fa-angle-left right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('fare.create')}}" class="nav-link {{(Route::is('addfare*'))? 'active sidebar-mini-active' : ''}}">
                                <!-- <i class="fa fa-list-alt nav-icon"></i> -->
                                <p>Add fare</p>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('fare.all')}}" class="nav-link {{(Route::is('fare*'))? 'active sidebar-mini-active' : ''}}">
                                <!-- <i class="fa fa-list-alt nav-icon"></i> -->
                                <p>Fare List</p>
                            </a>
                        </li>
                    </ul>
                </li>
                </li>

                <!-- end fare management -->

                   <!-- All Passengers tag  -->
                   <li class="nav-item">
                    <a href="{{url('/passenger')}}" class="nav-link {{ request()->routeIs('passenger*') ? 'active sidebar-mini-active' : '' }}">
                        <div><i class="fas fa-solid fa-user"></i>

                        <p class="pl-3">
                             All Passengers
                        </p>
                        </div>
                    </a>
                </li>
              <!-- end  All Passengers tag  -->
                <!-- All Passengers tag  -->
                <li class="nav-item">
                    <a href="{{url('/revenue')}}" class="nav-link {{ request()->routeIs('revenue*') ? 'active sidebar-mini-active' : '' }}">
                        <div> <i class="fas fa-solid fa-dollar-sign"></i>

                        <p class="pl-3">
                             Revenue
                        </p>
                        </div>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{url('/paypal')}}" class="nav-link {{ request()->routeIs('paypal*') ? 'active sidebar-mini-active' : '' }}">
                        <div> <i class="fas fa-solid fa-dollar-sign"></i>

                        <p class="pl-3">
                        Payment Configuration
                        </p>
                        </div>
                    </a>
                </li>
                <br>
                <br>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>













 <!-- Role -->
                <!-- <li class="nav-item {{Route::is('role.*') ? 'menu-open' : ''}}">
                    <a href="javascript:void(0)" class="nav-link {{Route::is('role.*') ? 'sidebar-mini-active' : ''}}">
                        <i class="nav-icon fa fa-code-branch"></i>
                        <p>
                            Role
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('role.create') }}" class="nav-link {{Route::is('role.create*') ? 'active sidebar-mini-active' : ''}}">
                                <i class="fa fa-plus nav-icon"></i>
                                <p>Create</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('role.all') }}" class="nav-link {{(Route::is('role.all*')||Route::is('role.update*'))? 'active sidebar-mini-active' : ''}}">
                                <i class="fa fa-list-alt nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                    </ul>
                </li> -->

                <!-- End Role -->



  <!-- end  All Passengers tag  -->

                   <!-- <li class="nav-item">
                    <a href="{{url('/meetingNotification')}}" class="nav-link  {{ request()->routeIs('meetingNotification*') ? 'active ' : '' }} ">
                        <i class="nav-icon fas fa-bell"></i>
                        <p>
                            Meeting Notify
                            <span class="right badge badge-danger">New
                            </span>
                        </p>
                    </a>
                </li>  -->
                <!-- <li class="nav-item">
                    <a href="{{url('/allmeeting')}}" class="nav-link  {{ request()->routeIs('allmeeting*') ? 'active ' : '' }}">
                        <i class="nav-icon fas fa-handshake"></i>
                        <p>
                            All Meetings
                        </p>
                    </a>
                </li> -->
                <!-- <li class="nav-item">
                    <a href="{{url('/addmeeting')}}" class="nav-link  {{ request()->routeIs('addmeeting*') ? 'active ' : '' }}">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Schedule Meetings
                        </p>
                    </a>
                </li> -->



                <!-- <li class="nav-item">
                    <a href="{{url('report')}}" class="nav-link  {{ request()->routeIs('report*') ? 'active ' : '' }}">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                             Meeting Reports
                        </p>
                    </a>
                </li> -->
                 <!-- <li class="nav-item">
                    <a href="{{url('/calendar')}}" class="nav-link  {{ request()->routeIs('calendar*') ? 'active ' : '' }}">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>                           Calendar                       </p>
                    </a>              </li> -->

