@extends('layouts.section.dashboards')
@section('content')

<div class="content-wrapper mt-5 py-3">
    <h3><i class="fas fa-solid fa-sitemap p-4"></i> Site Statistics</h3>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-indigo">
              <div class="inner">
                <h3>
                     @if(isset($addmeeting) && !empty($addmeeting))
                    {{count($addmeeting)}}
                    @else
                    0
                    @endif
                </h3>
                <i class="fas fa-solid fa-user"></i>
                <p>Total Passenger</p>
              </div>
              <div class="icon">
                <i class="nav-icon fas fa-th"></i>
              </div>
              <a href="{{url('/passenger')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-cyan">
              <div class="inner">
                <h3>@if(isset($up_date) && !empty($up_date))
                    {{count($up_date)}}
                    @endif</h3>
                    <i class="nav-icon fa fa-users"></i>
                    <p>All Drivers</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ route('user.alluser') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-pink">
                <div class="inner">
                    <h3>
                        @if(isset($users) && !empty($users))
                        {{count($users)}}
                        @else
                        0
                        @endif</h3>
                        
                        <i class="fas fa-regular fa-car"></i>
                        <p>All Vehicle</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{url('/viewvehicle')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3><i class="fas fa-solid fa-dollar-sign"></i> @if(isset($add) && !empty($add))
                        {{count($add)}}
                        @endif</h3>
                    <i class="fas fa-solid fa-dollar-sign"></i>
                <p>Revenue</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="{{url('/revenue')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div>
      <!-- /.container-fluid -->
      
      <!-- Ride Statistics<  -->
      <h3><i class="fas fa-solid fa-calendar p-3"></i> Ride Statistics</h3>

<!-- Main content -->

  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-6 ">
        <!-- small box -->
        <div class="small-box bg-primary ">
          <div class="inner">

              <h3>
                 @if(isset($addmeeting) && !empty($addmeeting))
                {{count($addmeeting)}}
                @else
                0
                @endif
            </h3>
            <i class="fas fa-duotone fa-car"></i>
            <p>Booked Ride</p>
          </div>
          <div class="icon">
            <i class="nav-icon fas fa-th"></i>
          </div>
          <a href="{{ url('bookedtrip') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-secondary">
          <div class="inner">
            <h3>@if(isset($up_date) && !empty($up_date))
                {{count($up_date)}}
                @endif</h3>
                <i class="fas fa-light fa-calculator"></i>
                <p>Running Ride</p>
          </div>
          <div class="icon">
              <i class="ion ion-stats-bars"></i>
          </div>
          <a href="{{ route('role.create') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success bg-gradient">
          <div class="inner">
            <h3>@if(isset($add) && !empty($add))
                {{count($add)}}
                @endif</h3>
                <i class="fas fa-solid fa-check"></i>
            <p>Completed Ride</p>
        </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="{{ route('role.all') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->

    <!-- /.row (main row) -->
  </div>
  <!-- /.container-fluid -->
</section>
<!-- end Ride Statistics  -->



    
    <section class="content">
        <div class="container-fluid ">
            <div class="card user-card-full shadow">
                <div class="row m-l-0 m-r-0 mt-4">
                    <div class="col-sm-4 bg-c-lite-green user-profile">
                        <div class="card-block text-center">
                            <div class="m-b-25">
                                <img src="{{asset('/image/logo.png')}}" class="img-radius img-fluid user-profile-image" alt="User-Profile-Image" role="button" width="200px">
                            </div>
                            <h4 class="f-w-800 mt-3 text-success">{{ Auth::user()->name }}</h4>
                            <p>{{isset(Auth::user()->getRoleNames()[0])?Auth::user()->getRoleNames()[0]:0}}</p>
                        </div>
                    </div>
                   <div class="col-sm-8 py-4">
                        <div class="card-block">
                            <h5 class="m-b-20 p-b-5 b-b-default f-w-600 px-3">Admin Information</h5>
                            <hr class="bg-success">
                            <div class="row px-3">
                                <div class="col-sm-6 mb-2">
                                   <p class="mb-2 f-w-600">Name</p>
                                   <!-- <input type="text" name="name" id="name" placeholder="Enter User Name"> -->
                                  
                                   <h6 class="text-muted f-w-400">{{ Auth::user()->name }}</h6>
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">Email</p>
                                    <!-- <input type="email" name="email" id="email" placeholder="Enter User Email"> -->
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->email }}</h6>
                                    <!-- <h6 class="text-muted f-w-400">bulbultaxi@gmail.com</h6> -->
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">Phone Number</p>
                                    <!-- <input type="text" name="number" id="number" placeholder="Enter User Number"> -->
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->number }}</h6>
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <p class="mb-2 f-w-600">password</p>
                                    <!-- <input type="text" name="id" id="id" placeholder="Enter User I.D"> -->
                                    <h6 class="text-muted f-w-400">{{ Auth::user()->employee_id }}</h6>
                                </div>
                            </div>
                            <hr class="bg-success">
                            <div class="card text-center" style="width: 150px;">
                            <a href="#" class="btn btn-primary w-100">Reset password</a>
                            
                            <!-- <h5 class=" f-w-600 px-3 py-2 btn btn-info">Register</h5> -->
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.container-fluid -->

        <!-- end table -->

       




        





 <!-- Ride Statistics<  -->
 <!-- <h3><i class="fas fa-solid fa-calendar"></i> Ride Statistics</h3> -->

<!-- Main content -->

  <!-- <div class="container-fluid"> -->
    <!-- Small boxes (Stat box) -->
    <!-- <div class="row">
      <div class="col-lg-3 col-6 "> -->
        <!-- small box -->
        <!-- <div class="small-box bg-primary ">
          <div class="inner">

            <h3>
                 @if(isset($addmeeting) && !empty($addmeeting))
                {{count($addmeeting)}}
                @else
                0
                @endif
            </h3>
            <i class="fas fa-duotone fa-car"></i>
            <p>Total No Ride</p>
          </div>
          <div class="icon">
            <i class="nav-icon fas fa-th"></i>
          </div>
          <a href="{{url('allmeeting')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div> -->
      <!-- ./col -->
      <!-- <div class="col-lg-3 col-6"> -->
        <!-- small box -->
        <!-- <div class="small-box bg-secondary">
          <div class="inner">
            <h3>@if(isset($up_date) && !empty($up_date))
                {{count($up_date)}}
                @endif</h3>
                <i class="fas fa-light fa-calculator"></i>
            <p>Running Ride</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div> -->
      <!-- ./col -->
      <!-- <div class="col-lg-3 col-6"> -->
        <!-- small box -->
      
      <!-- ./col -->
      <!-- <div class="col-lg-3 col-6"> -->
        <!-- small box -->
        <!-- <div class="small-box bg-success bg-gradient">
          <div class="inner">
            <h3>@if(isset($add) && !empty($add))
                {{count($add)}}
                @endif</h3>
                <i class="fas fa-solid fa-check"></i>
            <p>Completed Ride</p>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div> -->
      <!-- ./col -->
    <!-- </div> -->
    <!-- /.row -->
    <!-- Main row -->

    <!-- /.row (main row) -->
  <!-- </div> -->
  <!-- /.container-fluid -->
  <!-- </section> -->
<!-- end Ride Statistics  -->



































































































        <!-- <div class="card card-default">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-exclamation-triangle"></i>
                @if(isset($today) && !empty($today))
                {{count($today)}}
                @endif
                 Notification
              </h3>
            </div> -->
            <!-- /.card-header -->
            <!-- @foreach($today as $tody)
            <div class="card-body">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-check"></i> <span>{{$tody->meeting_name}}</span></h5>
                At {{$tody->meeting_time}} O'Clock Meeting .
              </div>
            </div> -->
            <!-- /.card-body -->
            @endforeach
        <!-- </div>
        </div>
    </section> -->
    <!-- /.content -->

  <!-- </div> -->

<!-- 

<div class="container">
        <h2 class="fw-bold">Meetings</h2>
        <nav class="navbar navbar-expand-lg bg-dark ">
            <ul class="navbar-nav">
                <li class="nav-item ">
                    <a class="nav-link active "
                        aria-current="page" href="#">Today</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Previous</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Upcoming</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link " href="#">Personal Meeting</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ">Meeting Agenda</a>
                </li>
            </ul>
        </nav>
    </div>
        <hr class="bg-success">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="bg-light rounded p-2  fs-6 text-success text-center px-5 border border-success ">&#128197;
                        Start time &nbsp; <span class="fw-bold text-dark">To</span> &nbsp; End Time</div>
                </div>
                <div class="col-2">
                    <div class="ms-auto "><button class="btn btn-success  p-2 px-4">Schedule Meeting</button></div>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-outline-success p-2 px-4">Today Meeting</button>
                </div>
            </div>


            <div class="row mt-4 mb-4  text-danger text-center">
                <hr class="bg-danger">
                <div class="col-3  mt-4">
                    <h4>Expired Meeting</h4>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Date:<span>00/00/0000</span></p>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Date:<span>00/00/0000</span></p>
                </div>
            </div>
            <hr class="bg-success">
            <div class="row  mt-4 mb-4  text-success text-center ">
                <div class="col-3  mt-4">
                    <h4>Today Meeting</h4>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Time:<span>00:00</span></p>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Time:<span>00:00</span></p>
                </div>
            </div>
            <hr class="bg-success ">
            <div class="row  mt-4 mb-4 text-primary text-center ">
                <div class="col-3 mt-4">
                    <h4>Upcoming Meeting</h4>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Date:<span>00/00/0000</span></p>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Date:<span>00/00/0000</span></p>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Date:<span>00/00/0000</span></p>
                </div>
            </div>

        </div>
    </div>




<div class="create mt-5" style="margin-left: 300px;">
        <div class="container text-success">
            <div class="d-flex">
                <h3>Create Meeting</h3>
                <button class="btn btn-success ms-auto mb-2">Add User +</button>
            </div>
            <div class="row g-3 align-items-center">
                <div class="py-2 col-lg-4 ">
                    <label class="py-2 col-form-label">Topic(Agenda):</label>
                </div>
                <div class="py-2 col-lg-8">
                    <input class="form-control">
                </div>
                <div class="py-2 col-lg-4">
                    <label class="py-2 col-form-label">Description(optional):</label>
                </div>
                <div class="py-2 col-lg-8">
                    <input class="form-control">
                </div>
                <hr class="bg-success">
                <div class="py-2 col-lg-4">
                    <label class="py-2 col-form-label">When:</label>
                </div>
                <div class="py-2 col-lg-4">
                    <input type="date" class="form-control">
                </div>
                <div class="py-2 col-lg-2">
                    <input class="form-control" placeholder="00:00">
                </div>
                <div class="py-2 col-lg-2">
                    <select class="form-select" aria-label="Disabled select example">
                        <option selected value="1">AM</option>
                        <option value="2">PM</option>
                    </select>
                </div>
                <hr class="bg-success">
                <div class="py-2 col-lg-4">
                    <label class="py-2 col-form-label">Duration:</label>
                </div>
                <div class="py-2 col-lg-2">
                    <select class="form-select" aria-label="Disabled select example">
                        <option selected value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </div>
                <div class="py-2 col-lg-1">
                    <h6>Hours</h6>
                </div>
                <div class="py-2 col-lg-2">
                    <select class="form-select" aria-label="Disabled select example">
                        <option selected value="1">0</option>
                        <option value="2">2</option>
                    </select>
                </div>
                <div class="py-2 col-lg-1">
                    <h6>min</h6>
                </div>
                <div class="py-2 col-lg-4 ">
                    <label class="py-2 col-form-label">Time Zone:</label>
                </div>
                <div class="py-2 col-lg-8">
                    <input class="form-control" type="time">
                </div>
                <div class="py-2 col-lg-4 ">
                    <label class="py-2 col-form-label">Meeting Id:</label>
                </div>
                <div class="py-2 col-lg-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                        <label class="form-check-label" for="flexCheckIndeterminate">
                            Auto generated ID: 1GVFG959F6G9
                        </label>
                    </div>
                </div>
                <div class="py-2 col-lg-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                        <label class="form-check-label" for="flexCheckIndeterminate">
                            My meeting ID: 1238TFYHF
                        </label>
                    </div>
                </div>
                <div class="py-2 col-lg-4 ">
                    <label class="py-2 col-form-label">Security:</label>
                </div>
                <div class="py-2 col-lg-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                        <label class="form-check-label" for="flexCheckIndeterminate">
                            Passcode
                        </label>
                    </div>
                </div>
                <div class="py-2 col-lg-4">
                    <input class="form-control">
                </div>
                <hr class="bg-success">
                <div class="py-2 col-lg-4 ">
                    <label class="py-2 col-form-label">Host Userame:</label>
                </div>
                <div class="py-2 col-lg-8">
                    <input class="form-control" placeholder="Enter your user name">
                </div>
                <div class="py-2 col-lg-4 ">
                    <label class="py-2 col-form-label">Invite Link:</label>
                </div>
                <div class="py-2 col-lg-8">
                    <a href="" class="link-primary">http:guydcgwfyug52ewf46v82ax486</a>
                </div>
            </div>
            <div class="py-2 col-12 mt-3 text-center">
                <button class="btn btn-success px-5 ">Save</button>
                <button class="btn btn-outline-success px-5">Cancel</button>
            </div>
        </div>
    </div>





    <div class=" mt-5" style="margin-left: 300px; ">
        <div class="px-5 ">
            <h2 class="fw-bold text-success">Meetings</h2>
        </div>
        <nav class="navbar navigate navbar-expand-lg ">
            <ul class="navbar-nav px-5">
                <li class="nav-item px-5">
                    <a class="nav-link active fs-6 link-success  text-decoration-none" aria-current="page"
                        href="#">Today</a>
                </li>
                <li class="nav-item px-5">
                    <a class="nav-link fs-6 link-success text-decoration-none" href="#">Previous</a>
                </li>
                <li class="nav-item px-5">
                    <a class="nav-link fs-6 link-success text-decoration-none border-bottom border-3 border-success"
                        href="#">Upcoming</a>
                </li>
                <li class="nav-item px-5">
                    <a class="nav-link fs-6 link-success text-decoration-none " href="#">Personal Meeting</a>
                </li>
                <li class="nav-item px-5">
                    <a class="nav-link fs-6 link-success text-decoration-none ">Meeting Agenda</a>
                </li>
            </ul>
        </nav>
        <hr class="bg-success">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="bg-light rounded p-2  fs-6 text-success text-center px-5 border border-success ">&#128197;
                        Start time &nbsp; <span class="fw-bold text-dark">To</span> &nbsp; End Time</div>
                </div>
                <div class="col-2">
                    <div class="ms-auto "><button class="btn btn-success  p-2 px-4">Schedule Meeting</button></div>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-outline-success p-2 px-4">Today Meeting</button>
                </div>
            </div>


            <div class="row mt-4 mb-4  text-center">
                <hr class="bg-danger">
                <div class="col-3  mt-4">
                    <h4>My meetings</h4>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Date:<span>00/00/0000</span></p>
                    <button class="btn btn-success px-5 ">Edit</button>
                    <button class="btn btn-outline-success px-5">Start</button>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Date:<span>00/00/0000</span></p>
                    <button class="btn btn-success px-5 ">Edit</button>
                    <button class="btn btn-outline-success px-5">Start</button>
                </div>
            </div>
            <hr class="bg-success">
            <div class="row  mt-4 mb-4   text-center ">
                <div class="col-3  mt-4">
                    <h4>Upcoming</h4>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Time:<span>00:00</span></p>
                    <button class="btn btn-success px-5 ">Edit</button>
                    <button class="btn btn-outline-success px-5">Start</button>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Time:<span>00:00</span></p>
                    <button class="btn btn-success px-5 ">Edit</button>
                    <button class="btn btn-outline-success px-5">Start</button>
                </div>
                <div class="col-3">
                    <h4>Meeting Name</h4>
                    <h6>Meeting:Id</h6>
                    <p>Description Of Meeting</p>
                    <p>Time:<span>00:00</span></p>
                    <button class="btn btn-success px-5 ">Edit</button>
                    <button class="btn btn-outline-success px-5">Start</button>
                </div>
            </div>
        </div>
    </div> -->
@endsection









                    <!-- driver


                    <div class="col-lg-4 col-md-12 col-sm-12 col-12 mt-5 py-5">
							<div class="card card-box">
								<div class="card-head">
									<header>Drivers List</header>
								</div>
								<div class="card-body ">
									<div class="row">
										<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;"><ul class="empListWindow small-slimscroll-style" style="overflow: hidden; width: auto;">
											<li>
												<div class="prog-avatar">
													<img src="../../assets/img/user/user1.jpg" alt="" width="40" height="40">
												</div>
												<div class="details">
													<div class="title">
														<a href="#">Rajesh Pandya</a>
													</div>
													<div>
														<span class="mobileTxt">3435564456</span>
													</div>
												</div>
											</li>
											<li>
												<div class="prog-avatar">
													<img src="../../assets/img/user/user2.jpg" alt="" width="40" height="40">
												</div>
												<div class="details">
													<div class="title">
														<a href="#">Sarah Smith</a>
													</div>
													<div>
														<span class="mobileTxt">464564353456</span>
													</div>
												</div>
											</li>
											<li>
												<div class="prog-avatar">
													<img src="../../assets/img/user/user3.jpg" alt="" width="40" height="40">
												</div>
												<div class="details">
													<div class="title">
														<a href="#">John Deo</a>
													</div>
													<div>
														<span class="mobileTxt">45345246464</span>
													</div>
												</div>
											</li>
											<li>
												<div class="prog-avatar">
													<img src="../../assets/img/user/user4.jpg" alt="" width="40" height="40">
												</div>
												<div class="details">
													<div class="title">
														<a href="#">Jay Soni</a>
													</div>
													<div>
														<span class="mobileTxt">9787667675</span>
													</div>
												</div>
											</li>
											<li>
												<div class="prog-avatar">
													<img src="../../assets/img/user/user5.jpg" alt="" width="40" height="40">
												</div>
												<div class="details">
													<div class="title">
														<a href="#">Jacob Ryan</a>
													</div>
													<div>
														<span class="mobileTxt">79795767563</span>
													</div>
												</div>
											</li>
											<li>
												<div class="prog-avatar">
													<img src="../../assets/img/user/user6.jpg" alt="" width="40" height="40">
												</div>
												<div class="details">
													<div class="title">
														<a href="#">Megha Trivedi</a>
													</div>
													<div>
														<span class="mobileTxt">57454365346</span>
													</div>
												</div>
											</li>
											<li>
												<div class="prog-avatar">
													<img src="../../assets/img/user/user2.jpg" alt="" width="40" height="40">
												</div>
												<div class="details">
													<div class="title">
														<a href="#">Sarah Smith</a>
													</div>
													<div>
														<span class="mobileTxt">989678768546</span>
													</div>
												</div>
											</li>
											<li>
												<div class="prog-avatar">
													<img src="../../assets/img/user/user3.jpg" alt="" width="40" height="40">
												</div>
												<div class="details">
													<div class="title">
														<a href="#">John Deo</a>
													</div>
													<div>
														<span class="mobileTxt">53453464533</span>
													</div>
												</div>
											</li>
											<li>
												<div class="prog-avatar">
													<img src="../../assets/img/user/user4.jpg" alt="" width="40" height="40">
												</div>
												<div class="details">
													<div class="title">
														<a href="#">Jay Soni</a>
													</div>
													<div>
														<span class="mobileTxt">45646345734</span>
													</div>
												</div>
											</li>
										</ul><div class="slimScrollBar" style="background: rgb(158, 165, 171); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 180.267px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
										<div class="full-width text-center p-t-10">
											<a href="#" class="btn purple btn-outline btn-circle margin-0">View All</a>
										</div>
									</div>
								</div>
							</div>
						</div> -->
